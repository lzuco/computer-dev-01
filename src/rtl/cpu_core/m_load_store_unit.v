//-----------------------------------------------
//    module name: m_load_store_unit
//    author: Sun Zhe
//  
//    version: 1st version (2021-10-01)
//    description: LSU 模块实现对字、半字、字节的 load 和  store 操作，load 指令需要从 GRF 的最低位
//                 开始读相应的位数（位数根据字、半字还是字节来决定），存到存储器的偏移量中，存储器中
//                 的其他位保持不变；store 指令需要从存储器的偏移量开始读相应的位数，存到 GRF 的最低
//                 位，并根据指令是无符号扩展还是有符号扩展对读出的数据进行补 0 或者补符号位。对于非对
//                 齐的情况，将数据读出或者写入都要跨行，此时需要暂停流水线，花费多个周期完成。
//
//-----------------------------------------------
`timescale 1ns / 1ps

module m_load_store_unit (
    input  wire         clk,
    input  wire         rstn,
	   
	input  wire [102:0] i_ExeLSUBus_103, 

    input  wire         i_rsp_vaild,
	output wire         o_lsuEnable_1,				   
    output wire  [31:0] o_memAddr_32   ,
    input  wire  [31:0] i_memData_32   ,     //待Load的数据
    output wire  [31:0] o_memData_32   ,     //发往存储器的数据
    output wire  [ 3:0] o_memWen_4    ,      //存储器写使能信号
   
    output wire         o_lsuPipeHold_1,     //流水线控制信号

    output wire  [31:0] o_rdData_32,
    output wire         o_rdWen_1,           //GRF写使能

    output wire  [ 4:0] o_exception_5        //gray code                               --> to interrupt module

    );
	reg [31:0]  r_memAddr_32 ;  
	reg [31:0]  r_memData_32 ;
	reg [ 3:0]  r_memWen_4   ;
	assign o_memAddr_32 = r_memAddr_32;
	assign o_memData_32 = r_memData_32;
	assign o_memWen_4   = r_memWen_4  ;

	reg         r_grfWen_1;         //LSU grf写使能
	wire        w_grfWen_1;         //其他指令 grf写使能
    wire        w_isStore_1;        
    wire [31:0] w_exeResult_32; 
    wire [ 4:0] w_rdAddr_5;     
    wire        w_isLoad_1;       
    wire        w_isCsr_1;      
    wire [31:0] w_csrData_32;
	
	wire [31:0] i_memAddr_32  ;
	wire [31:0] i_storeData_32;
	wire [ 1:0] w_lsuType_2   ;
	wire        w_loadSign_1  ;
	wire [31:0] w_aluOperand1_32;
    wire 		w_misaligned_1;     //未对齐标志
	reg  [31:0] r_grfdata_32;
    reg  [ 2:0] r_state_3;
    reg  [ 2:0] r_next_state_3;
    reg  [31:0] r_tempstore_32;     //只有load会出现连续的两次读存储器，因此需要暂存
    reg         r_endsignal_1;
		

    localparam state_IDLE             = 3'd0;
    localparam state_LoadByteAndEnd   = 3'd1;     //读 字节  发送结束信号
    localparam state_LoadHwordAndEnd  = 3'd2;     //读 半字  发送结束信号
    localparam state_LoadwordAndEnd   = 3'd3;     //读   字  发送结束信号
    localparam state_LoadHwordmisa    = 3'd4;     //读 下一行的 半字  暂存上一次结果
    localparam state_Loadwordmisa     = 3'd5;     //读 下一行的   字  暂存上一次结果
    localparam state_StorewordAndEnd  = 3'd6;     //写   字  发送结束信号
    localparam state_StoreHwordAndEnd = 3'd7;     //写 半字  发送结束信号
	
	assign {
				i_storeData_32,                   //待存的值
				w_aluOperand1_32,                 // i_csrData_32
				w_grfWen_1,
				w_isStore_1,
				w_lsuType_2,                      //(字半字字节 11 10/01 00)
				w_loadSign_1,                     //(是否符号扩展 1 0)      
				w_exeResult_32,
				w_isLoad_1,
				w_isCsr_1  
						 } = i_ExeLSUBus_103;	  
							  
    assign i_memAddr_32    = w_exeResult_32;      //存储器地址	
    
    //未对齐标志
    assign w_misaligned_1  = ( w_lsuType_2 == 2'b11 & i_memAddr_32[1:0] != 2'b00) 
                           | ((w_lsuType_2 == 2'b01 | w_lsuType_2 == 2'b10 )& i_memAddr_32[1:0] == 2'b11);

    assign o_lsuEnable_1   = w_isLoad_1  | w_isStore_1 ;

	assign o_rdData_32     =  {{{32{~w_grfWen_1              }}  & r_grfdata_32       }
                             | {{32{~w_isCsr_1 & w_grfWen_1  }}  & w_exeResult_32     }
                             |  {32{ w_isCsr_1 & w_grfWen_1  }}  & w_aluOperand1_32   }; //i_csrData_32
    assign o_rdWen_1       = r_grfWen_1 | w_grfWen_1;
    
    assign o_exception_5   = 5'b0; //不考虑越界情况，此处异常留待补充
	assign o_lsuPipeHold_1 = o_lsuEnable_1 & (~r_endsignal_1);
    	
    always @(*) begin              //状态机
        if(w_isLoad_1) begin       //mem -> grf 
            case (r_state_3)
                state_IDLE: begin 
                                r_endsignal_1  = 1'b0;
                                r_memAddr_32   = i_memAddr_32;
								r_tempstore_32 = 32'b0;
                                r_memData_32   = 32'b0;
                                r_memWen_4     = 4'b0;
                                r_grfdata_32   = 32'b0;
                                r_grfWen_1     = 1'b0;
                                if(i_rsp_vaild) begin
                                    case (w_lsuType_2)
                                        2'b11:        begin r_next_state_3 = !w_misaligned_1?state_LoadwordAndEnd:state_Loadwordmisa;end
                                        2'b10, 2'b01: begin r_next_state_3 = !w_misaligned_1?state_LoadHwordAndEnd:state_LoadHwordmisa; end
                                        2'b00:        begin r_next_state_3 = state_LoadByteAndEnd; end
                                        //default: begin r_next_state_3=state_IDLE; end
                                    endcase    
                                end
								else begin r_next_state_3 = state_IDLE; end
                            end        
                state_LoadByteAndEnd :begin     r_next_state_3 = state_IDLE; 
                                                r_endsignal_1  = 1'b1;
                                                r_memAddr_32   = i_memAddr_32;
												r_tempstore_32 = 32'b0;
                                                r_memData_32   = 32'b0;
                                                r_memWen_4     = 4'b0;
                                                //根据偏移量和是否为符号扩展确定发往寄存器的数据
                                                r_grfdata_32   =  i_memAddr_32[1:0] == 2'b00 ? w_loadSign_1? {{24{i_memData_32[ 7]}},i_memData_32[ 7: 0]}: {24'b0,i_memData_32[ 7: 0]}:
                                                                  i_memAddr_32[1:0] == 2'b01 ? w_loadSign_1? {{24{i_memData_32[15]}},i_memData_32[15: 8]}: {24'b0,i_memData_32[15: 8]}:
                                                                  i_memAddr_32[1:0] == 2'b10 ? w_loadSign_1? {{24{i_memData_32[23]}},i_memData_32[23:16]}: {24'b0,i_memData_32[23:16]}:
                                                                  i_memAddr_32[1:0] == 2'b11 ? w_loadSign_1? {{24{i_memData_32[31]}},i_memData_32[31:24]}: {24'b0,i_memData_32[31:24]}:
                                                                  32'b0; //不存在未对齐情况
                                                r_grfWen_1     =  1'b1;
    
                end
                state_LoadHwordAndEnd:begin     r_next_state_3 = state_IDLE; r_endsignal_1 = 1'b1; 
                                                r_memAddr_32   = i_memAddr_32;
												r_tempstore_32 = 32'b0;
                                                r_memData_32   = 32'b0;
                                                r_memWen_4     = 4'b0;
                                                 //对于半字的Load存在未对齐的情况(offset=3)，根据是否对齐、偏移量和是否为符号扩展确定发往寄存器的数据
                                                r_grfdata_32   =  w_misaligned_1?
                                                                    i_memAddr_32[1:0] == 2'b11 ? w_loadSign_1? {{ 8{i_memData_32[23]}},i_memData_32[23:0],r_tempstore_32[31:24]}: {8'b0,i_memData_32[23:0],r_tempstore_32[31:24]}:32'b0
                                                                :(  i_memAddr_32[1:0] == 2'b00 ? w_loadSign_1? {{16{i_memData_32[15]}},i_memData_32[15: 0]}: {16'b0,i_memData_32[15: 0]}:
                                                                    i_memAddr_32[1:0] == 2'b01 ? w_loadSign_1? {{16{i_memData_32[23]}},i_memData_32[23: 8]}: {16'b0,i_memData_32[23: 8]}:
                                                                    i_memAddr_32[1:0] == 2'b10 ? w_loadSign_1? {{16{i_memData_32[31]}},i_memData_32[31:16]}: {16'b0,i_memData_32[31:16]}:32'b0)
                                                                ;
                                                r_grfWen_1     = 1'b1;
                                        end
                state_LoadwordAndEnd :begin     r_next_state_3 = state_IDLE; 
                                                r_endsignal_1  = 1'b1;  
                                                r_memAddr_32   = i_memAddr_32;
												r_tempstore_32 = 32'b0;
                                                r_memData_32   = 32'b0;
                                                r_memWen_4     = 4'b0;
                                                //对于字的Load与半字相似，但未对齐情况增加(offset=3,offset=2,offset=1)
                                                r_grfdata_32   =  w_misaligned_1?(
                                                                        i_memAddr_32[1:0] == 2'b01 ?   {i_memData_32[7:0],r_tempstore_32[31:8]}     :
                                                                        i_memAddr_32[1:0] == 2'b10 ?   {i_memData_32[15:0],r_tempstore_32[31:16]}   :
                                                                        i_memAddr_32[1:0] == 2'b11 ?   {i_memData_32[23:0],r_tempstore_32[31:24]}   :32'b0
                                                                    ):  i_memAddr_32[1:0] == 2'b00 ?  i_memData_32[31:0]:32'b0
                                                                ;
                                                r_grfWen_1     = 1'b1;                
                                        end
                state_LoadHwordmisa  :begin     r_next_state_3 = state_LoadHwordAndEnd; 
                                                r_endsignal_1  = 1'b0;                                                      
                                                r_memAddr_32   = i_memAddr_32 + 32'h4;
                                                r_tempstore_32 = i_memData_32;
                                                r_memData_32   = 32'b0;
                                                r_memWen_4     = 4'b0;
                                                r_grfdata_32   = 32'b0;
                                                r_grfWen_1     = 1'b0;
                                                end
                state_Loadwordmisa   :begin     r_next_state_3 = state_LoadwordAndEnd;  
                                                r_endsignal_1  = 1'b0;             
                                                //将当前数据暂存，并准备读取存储器下一单元的数据                                         
                                                r_memAddr_32   = i_memAddr_32 + 32'h4;
                                                r_tempstore_32 = i_memData_32;
                                                r_memData_32   = 32'b0;
                                                r_memWen_4     = 4'b0;
                                                r_grfdata_32   = 32'b0;
                                                r_grfWen_1     = 1'b0;
                                                end
                default :begin                 r_endsignal_1   = 1'b1;
												r_memAddr_32   = i_memAddr_32;
												r_tempstore_32 = 32'b0;
												r_memData_32   = 32'b0;
												r_memWen_4     = 4'b0;
												r_grfdata_32   = 32'b0;
												r_grfWen_1     = 1'b0;
												r_next_state_3 = state_IDLE; 
				end
            endcase
        end else if(w_isStore_1) begin //storeData -> mem 
            case (r_state_3)
                state_IDLE: begin 
                            if(!w_misaligned_1) begin //一次完成写入
                                        r_endsignal_1  = 1'b1;
                                        r_memAddr_32   = i_memAddr_32;
										r_tempstore_32 = 32'b0;
                                        r_grfdata_32   = 32'b0;
                                        r_grfWen_1     = 1'b0;
                                        r_next_state_3 = state_IDLE;
                                        //一次写入情况：
                                        //字 offset=0; 半字 offset=2,1,0; 字节 offset=3,2,1,0 
                                        //根据偏移量确定相应字节的写使能信号
                                        case (w_lsuType_2)
                                        2'b11:        begin 
                                                r_memData_32  = i_storeData_32[31:0];
                                                r_memWen_4    = 4'b1111;
                                        end
                                        2'b10, 2'b01: begin 
                                            case (i_memAddr_32[1:0])
                                                2'b10: begin  
                                                    r_memData_32  = {i_storeData_32[15:0],16'b0};
                                                    r_memWen_4    = 4'b1100;
                                                end
                                                2'b01: begin  
                                                    r_memData_32  = {8'b0,i_storeData_32[15:0],8'b0};
                                                    r_memWen_4    = 4'b0110;
                                                end
                                                2'b00: begin  
                                                    r_memData_32  = {16'b0,i_storeData_32[15:0]};
                                                    r_memWen_4    = 4'b0011;
                                                end
                                                default: begin r_memData_32  = 32'b0;  r_memWen_4    = 4'b0000; end
                                            endcase                                                        
                                        
                                        end
                                        2'b00:        begin 
                                            case (i_memAddr_32[1:0])
                                                2'b11: begin  
                                                    r_memData_32  = {i_storeData_32[7:0],24'b0};
                                                    r_memWen_4    = 4'b1000;
                                                end
                                                2'b10: begin  
                                                    r_memData_32  = {8'b0,i_storeData_32[7:0],16'b0};
                                                    r_memWen_4    = 4'b0100;
                                                end
                                                2'b01: begin  
                                                    r_memData_32  = {16'b0,i_storeData_32[7:0],8'b0};
                                                    r_memWen_4    = 4'b0010;
                                                end
                                                2'b00: begin  
                                                    r_memData_32  = {24'b0,i_storeData_32[7:0]};
                                                    r_memWen_4    = 4'b0001;
                                                end
                                            endcase
                                        end
                                    endcase
                                    
                            end else begin //两次完成写入
                                r_endsignal_1  = 1'b0;
                                r_memAddr_32   = i_memAddr_32;
                                r_grfdata_32   = 32'b0;
                                r_grfWen_1     = 1'b0;
                                r_tempstore_32 = 32'b0;
                                //两次次写入情况：
                                //字 offset=3,2,1,0; 半字 offset=3
                                //根据偏移量确定相应字节的写使能信号
                                case (w_lsuType_2)
                                    2'b11: begin 
                                        case (i_memAddr_32[1:0])
                                                2'b11: begin  
                                                    r_memData_32  = {i_storeData_32[7:0],24'b0};
                                                    r_memWen_4    = 4'b1000;
                                                end
                                                2'b10: begin  
                                                    r_memData_32  = {i_storeData_32[15:0],16'b0};
                                                    r_memWen_4    = 4'b1100;
                                                end
                                                2'b01: begin  
                                                    r_memData_32  = {i_storeData_32[23:0],8'b0};
                                                    r_memWen_4    = 4'b1110;
                                                end
                                                2'b00: begin  
                                                    r_memData_32  = 32'b0;
                                                    r_memWen_4    = 4'b0000;
                                                end
                                            endcase
    
                                        r_next_state_3 = state_StorewordAndEnd;
                                    end
                                    2'b10,2'b01: begin 
                                        r_memData_32   = {i_storeData_32[7:0],24'b0};
                                        r_memWen_4     = 4'b1000;
                                        r_next_state_3 = state_StoreHwordAndEnd;
                                    end
                                    default: begin  
									    r_memData_32   = {32'b0};
                                        r_memWen_4     = 4'b0000;
                                        r_next_state_3 = state_IDLE;end
                                endcase
    
                            end

                        end
                state_StorewordAndEnd    : begin 
                                    r_endsignal_1  = 1'b1;
                                    r_next_state_3 = state_IDLE;
									r_tempstore_32 = 32'b0;
                                    r_memAddr_32   = i_memAddr_32;
                                    r_grfdata_32   = 32'b0;
                                    r_grfWen_1     = 1'b0;
                                    //读取寄存器的最低位，存至存储器的偏移量部分，完成第二次写入，Store操作结束
                                    case (i_storeData_32[1:0])
                                                    2'b11: begin  
                                                        r_memData_32  = {8'b0,i_storeData_32[31:8]};
                                                        r_memWen_4    = 4'b0111;
                                                    end
                                                    2'b10: begin  
                                                        r_memData_32  = {16'b0,i_storeData_32[31:16]};
                                                        r_memWen_4    = 4'b0011;
                                                    end
                                                    2'b01: begin  
                                                        r_memData_32  = {24'b0,i_storeData_32[31:24]};
                                                        r_memWen_4    = 4'b0001;
                                                    end
                                                    2'b00: begin  
                                                        r_memData_32  = 32'b0;
                                                        r_memWen_4    = 4'b0000;
                                                    end
                                                endcase
                
                end
                state_StoreHwordAndEnd    : begin 
                                    r_endsignal_1  = 1'b1;
                                    r_memAddr_32   = i_memAddr_32;
									r_tempstore_32 = 32'b0;
                                    r_grfdata_32   = 32'b0;
                                    r_grfWen_1     = 1'b0;
                                    r_memData_32   = {24'b0,i_storeData_32[15:8]};
                                    r_memWen_4     = 4'b0001;
                                    r_next_state_3 = state_IDLE;
                                    end
                default :begin  r_endsignal_1  = 1'b1;
								r_memAddr_32   = i_memAddr_32;
								r_tempstore_32 = 32'b0;
								r_memData_32   = 32'b0;
								r_memWen_4     = 4'b0;
								r_grfdata_32   = 32'b0;
								r_grfWen_1     = 1'b0;
								r_next_state_3 = state_IDLE; 
				end
        
            endcase
        end else begin 
                r_endsignal_1  = 1'b1;
                r_memAddr_32   = i_memAddr_32;
				r_tempstore_32 = 32'b0;
                r_memData_32   = 32'b0;
                r_memWen_4     = 4'b0;
                r_grfdata_32   = 32'b0;
                r_grfWen_1     = 1'b0;
                r_next_state_3 = state_IDLE; 
        end    
    end
    
    //时序逻辑，描述状态转移
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin            
            r_state_3       <= state_IDLE;
        end else begin
            r_state_3       <= r_next_state_3;
        end
    end

endmodule
