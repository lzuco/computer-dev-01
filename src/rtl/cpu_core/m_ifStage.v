//-----------------------------------------------
//    module name: m_ifStage
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 第一级流水模块实例化了提前分支模块、指令寄存器模块以及nextPC模块，
//                 主要完成指令的提前分支跳转、选择出正确的下一条指令的PC以及转换压缩指令
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_ifStage(

    input  wire        rstn,             
    input  wire        clk,             

    input  wire        i_isIntPC_1,         //PC是否来自中断模块
    input  wire [31:0] i_intPC_32,          //来自中断的PC
    output wire [ 4:0] o_exception_5,       //发往中断模块的异常编码
	output wire [31:0] o_jumpInst_32,

    input  wire        i_clearFirstn_1,     //冲刷流水线
    input  wire        i_holdflag_1,

	output wire [ 9:0] o_ifToGrf_10,
	input  wire [63:0] i_grfToIf_64,
	
	output wire [63:0] o_instAndPcIf_64, 
	output wire [31:0] o_nextPC_32,
	input  wire [31:0] i_inst_32

);
    wire [31:0] w_inst_32;
    wire [31:0] w_PC_32;
    wire [31:0] w_branchPC_32;
    wire        w_16flag_1;
    wire        w_isBranch_1;

    wire [31:0] w_instout_32;
	wire [31:0] w_instaddrin_32;
	wire [31:0] w_instaddr_32;
	
//-----{m_nextPC模块}begin
    m_nextPC pcGen(
		.clk				(clk			),
        .rstn				(rstn			),
	
        .i_holdflag_1		(i_holdflag_1	),
        .i_16flag_1			(w_16flag_1		),
        .i_isIntPC_1		(i_isIntPC_1	),
        .i_intPC_32			(i_intPC_32		),
        .i_isBranch_1		(w_isBranch_1	),
        .i_branchPC_32		(w_branchPC_32	),
		
        .o_presentPC_32		(w_instaddrin_32), //当前取到的的指令地址 -> instReg
        .o_nextPC_32		(o_nextPC_32	)  //下一条取指的PC
    );
//-----{m_nextPC模块}end

//-----{m_instReg模块}begin
    m_instReg instReg(
        .clk				(clk			),
        .rstn				(rstn			),

        .i_inst_32			(i_inst_32		),
		.i_instaddr_32		(w_instaddrin_32),
        .o_inst_32			(w_inst_32		),
		.o_instaddr_32		(w_instaddr_32	),
		
		.o_16flag_1			(w_16flag_1		),
		.i_clear_1			(w_isBranch_1 |i_isIntPC_1| i_clearFirstn_1 | i_holdflag_1) // 来自内部和外部的信号

    );
//-----{m_instReg模块}end

//-----{m_prebranch模块}begin
    m_prebranch prebranch(
        .i_inst_32			(w_inst_32		),
        .i_addr_32			(w_instaddr_32	),
		
		.o_ifToGrf_10       (o_ifToGrf_10	),
		.i_grfToIf_64       (i_grfToIf_64	),
        
        .o_inst_32			(w_instout_32	),
        .o_exception_5		(o_exception_5	),

        .o_branchPC_32		(w_branchPC_32	),
        .o_isBranch_1		(w_isBranch_1	)
    );
//-----{m_prebranch模块}end

    assign o_jumpInst_32    = w_inst_32;
	assign o_instAndPcIf_64 = {w_instaddr_32,w_instout_32};

endmodule
