//-----------------------------------------------
//    module name: m_grf
//    author: LuYihua
//  
//    version: 1st version (2021-10-01)
//    description: 
//             ͨ�üĴ���ģ��    
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module m_grf(

    input wire         clk,
    input wire         rstn,
                      
	input  wire [47:0] i_idToGrf_48,
	output wire [63:0] o_grfToId_64,
	
	input  wire [ 9:0] i_ifToGrf_10,
	output wire [63:0] o_grfToIf_64
	

    );
	
//-----{�źŴ��}begin
	wire        w_writeEnable_1;  // д�Ĵ�����־
	wire [ 4:0] w_waddr_5;        // д�Ĵ�����ַ       
	wire [ 4:0] w_raddr1_5;       // ���Ĵ���1��ַ
	wire [ 4:0] w_raddr2_5;       // ���Ĵ���2��ַ
	wire [31:0] w_wdata_32;       // д�Ĵ������� 
	
	reg  [31:0] r_rdata1_32;      // ���Ĵ���1����
	reg  [31:0] r_rdata2_32;      // ���Ĵ���2����
	
	wire [ 4:0] w_raddr3_5;       // ���Ĵ���3��ַ
	wire [ 4:0] w_raddr4_5;       // ���Ĵ���4��ַ
	
	reg  [31:0] r_rdata3_32;      // ���Ĵ���3����
	reg  [31:0] r_rdata4_32;      // ���Ĵ���4����
 
	assign {	w_writeEnable_1,
				w_waddr_5,      
				w_raddr1_5,     
				w_raddr2_5,     
				w_wdata_32   } = i_idToGrf_48;
                
	assign {w_raddr3_5,	w_raddr4_5} = i_ifToGrf_10;	
	assign o_grfToId_64             = {r_rdata1_32,r_rdata2_32};
	assign o_grfToIf_64             = {r_rdata3_32,r_rdata4_32};


//-----{�źŴ��}end


    reg[31:0] regs [0:31];
    integer i;                                      //ѭ����
    // д�Ĵ���
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
          for(i=0;i<32;i=i+1)                       //�Ĵ����ϵ��0
             regs[i] <= 32'b0;
        end
        else begin 
          if (w_writeEnable_1 & |w_waddr_5) begin
                regs[w_waddr_5] <= w_wdata_32;
            end 
        end
    end

    // ���Ĵ���1
    always @ (*) begin
        if ( !(|w_raddr1_5)) begin                  // w_raddr1_5 == 5'b0
            r_rdata1_32 = 32'b0;
        end else begin
            r_rdata1_32 = regs[w_raddr1_5];
        end
    end

    // ���Ĵ���2
    always @ (*) begin
        if (!(|w_raddr2_5)) begin
            r_rdata2_32 = 32'b0;
        end else begin
            r_rdata2_32 = regs[w_raddr2_5];
        end
    end
    
        // ���Ĵ���3
    always @ (*) begin
        if (!(|w_raddr3_5)) begin
            r_rdata3_32 = 32'b0;
        // �������ַ����д��ַ����������д��������ֱ�ӷ���д����
        end else if (w_raddr3_5 == w_waddr_5 & w_writeEnable_1) begin
            r_rdata3_32 = w_wdata_32;
        end else begin
            r_rdata3_32 = regs[w_raddr3_5];
        end
    end

    // ���Ĵ���4
    always @ (*) begin
        if (!(|w_raddr4_5)) begin
            r_rdata4_32 = 32'b0;
        // �������ַ����д��ַ����������д��������ֱ�ӷ���д����
        end else if (w_raddr4_5 == w_waddr_5 & w_writeEnable_1) begin
            r_rdata4_32 = w_wdata_32;
        end else begin
            r_rdata4_32 = regs[w_raddr4_5];
        end
    end
endmodule
