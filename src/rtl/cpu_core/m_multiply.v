//-----------------------------------------------
//    module name: m_multiply
//    author: WeiRen
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`timescale 1ns / 1ps
module m_multiply(                         
    input  wire        clk,
    input  wire        rstn,
    input  wire        i_mulBegin_1,       // �˷���ʼ�ź�
    input  wire [31:0] i_mulOperand1_32,   // �˷�������1
    input  wire [31:0] i_mulOperand2_32,   // �˷�������2
    input  wire [ 1:0] i_mulDivSign_2,     // �˷�����
    output wire [63:0] o_product_64,       // �˻�
    output wire        o_mulWorking_1,     // �˷������ź�
    output wire        o_mulEnd_1          // �˷������ź�
);

    reg  [ 2:0] r_cnt_3;
    wire [ 2:0] w_cntNext_3;

    reg  [63:0] r_product_64;
    wire [63:0] w_product_64;
    wire [ 7:0] w_partOperand_8;
    wire [39:0] w_partProduct_40;

    wire [31:0] w_operand1_32;
    wire [31:0] w_operand2_32;

    wire w_productNeg_1;

    wire w_cntEqu4_1;
    
    assign w_productNeg_1   = i_mulDivSign_2[0] & i_mulOperand1_32[31] & ~(i_mulDivSign_2[1] & i_mulOperand2_32[31])
                            | i_mulDivSign_2[1] & i_mulOperand2_32[31] & ~(i_mulDivSign_2[0] & i_mulOperand1_32[31]);
    assign w_cntEqu4_1      = r_cnt_3[2]&~|r_cnt_3[1:0];

    assign w_operand1_32    = {32{  i_mulDivSign_2[0] & i_mulOperand1_32[31] }} & ~i_mulOperand1_32+1'b1
                            | {32{~(i_mulDivSign_2[0] & i_mulOperand1_32[31])}} &  i_mulOperand1_32;

    assign w_operand2_32    = {32{  i_mulDivSign_2[1] & i_mulOperand2_32[31] }} & ~i_mulOperand2_32+1'b1
                            | {32{~(i_mulDivSign_2[1] & i_mulOperand2_32[31])}} &  i_mulOperand2_32;

    assign w_cntNext_3 =  {3{w_cntEqu4_1                    }} & 3'b0
                        | {3{i_mulBegin_1 & ~w_cntEqu4_1    }} & r_cnt_3+1;

	assign w_partOperand_8  = {8{~r_cnt_3[1] &  r_cnt_3[0]  }} & w_operand2_32[31:24]
                            | {8{ r_cnt_3[1] & ~r_cnt_3[0]  }} & w_operand2_32[23:16]
                            | {8{&r_cnt_3[1:0]              }} & w_operand2_32[15: 8]
                            | {8{ r_cnt_3[2]&~|r_cnt_3[1:0] }} & w_operand2_32[ 7: 0];

    assign w_partProduct_40 = w_operand1_32 * w_partOperand_8;
    always @(posedge clk or negedge rstn) begin
        if (~rstn) begin
            r_cnt_3      <= 2'b0;
            r_product_64 <= 64'b0;
        end else begin
            r_cnt_3      <= w_cntNext_3;
            r_product_64 <= ({r_product_64[55:0],8'b0} + w_partProduct_40)&{64{~o_mulEnd_1}};
        end
    end
    assign w_product_64   = ({r_product_64[55:0],8'b0} + w_partProduct_40);
    assign o_product_64   = {64{ w_productNeg_1}}   & ~w_product_64+1
                          | {64{~w_productNeg_1}}   &  w_product_64;
    assign o_mulWorking_1 = (|r_cnt_3 | i_mulBegin_1)&rstn;
    assign o_mulEnd_1     = ~o_mulWorking_1|w_cntEqu4_1;
	
    endmodule




/*`timescale 1ns / 1ps
`include "riscv.h"
module m_multiply(                         
    input         clk,            // Ê±ÖÓ
    input         i_mulBegin_1,       
    input  [31:0] i_mulOperand1_32,   
    input  [31:0] i_mulOperand2_32,  // ID->EXE×ÜÏß
    input  [ 1:0] i_mulDivSign_2,    // EXE->MEM×ÜÏß
    output [63:0] o_product_64,      // ³Ë³ý·¨½á¹û
    output        o_mulWorking_1,     // EXEÒªÐ´»Ø¼Ä´æÆ÷¶ÑµÄÄ¿±êµØÖ··
    output        o_mulEnd_1
);
    assign o_mulEnd_1       = 1'b1;
    assign o_mulWorking_1   = 1'b0;
    reg   [63:0] temp;
    reg   [31:0] a;
    always @(*) begin
        if(i_mulDivSign_2 == 2'b11) begin
            temp = $signed(i_mulOperand1_32) * $signed(i_mulOperand2_32);
        end else if(i_mulDivSign_2 == 2'b00) begin
            temp = $unsigned(i_mulOperand1_32) * $unsigned(i_mulOperand2_32);
        end else begin
            if(i_mulOperand1_32[31] == 1'b1 )
                 begin
                      a = ~(i_mulOperand1_32-1'b1);
                      temp = ~(a*$unsigned(i_mulOperand2_32))+1'b1;
                 end else
                      temp = (($unsigned(i_mulOperand1_32) * $unsigned(i_mulOperand2_32))); 
        end
    end
    
    assign     o_product_64 = temp;
    
    endmodule
*/
