//-----------------------------------------------
//    module name: m_CtoI
//    author: Wei Ren
//  
//    version: 1st version (2021-10-01)
//    description: 将压缩指令（16位）转换为相同功能的非压缩指令（32位），
//                 以便后续统一处理
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_CtoI (
    input  wire [15:0]    i_inst_16,
    output wire [31:0]    o_inst_32
);
//------------------------------实现指令列表{begin}------------------------------//

    //----------Load and Store Instructions(读写指令)----------//

    // Stack-Pointer-Based Loads and Stores             (基于栈指针的读写)
    wire w_CLWSP_1  , w_CSWSP_1 ;

    // Register-Based Loads and Stores                  (基于寄存器的读写)
    wire w_CLW_1    , w_CSW_1   ;

    //----------Control Transfer Instuctions(控制权转让指令)----------//
    wire w_CJ_1     , w_CJAL_1  , w_CJR_1   , w_CJALR_1  
        ,w_CBEQZ_1  , w_CBNEZ_1  ;

    //----------Interger Computational Instructions(整数运算指令)----------//

    // Interger Constant-Generation Instructions        (整型常数生成指令)
    wire w_CLI_1    , w_CLUI_1  ;

    // Integer Register-Immediate Operations            (整型寄存器立即数间操作)
    wire w_CADDI_1  , w_CADDI16SP_1 , w_CADDI4SPN_1 ;
    wire w_CSLLI_1  , w_CSRLI_1     , w_CSRAI_1     ;
    wire w_CANDI_1  ;

    // Integer Register-Register Operations             (整型寄存器间操作)
    wire w_CMV_1    , w_CADD_1  ;
    wire w_CAND_1   , w_COR_1   , w_CXOR_1  , w_CSUB_1  ;

    // Defined Illegal Instruction                      (定义的非法指令)
    wire w_illegal_1;

    // NOP Instruction                                  (NOP指令)
    wire w_CNOP_1   ;

    // Breakpoint Instruction                           (断点指令)
    wire w_CEBREAK_1;

//------------------------------实现指令列表{end}------------------------------//


//------------------------------指令译码{begin}------------------------------//

    wire [2:0]  w_CFUNCT3_3;                // 功能码
    wire [3:0]  w_CFUNCT4_4;                // 功能码
    wire [5:0]  w_CFUNCT6_6;                // 功能码
    wire [1:0]  w_COP_2;                    // 操作码
    wire [4:0]  w_CIRrs1_5;                 // CI/CR型指令的rs1部分
    wire [4:0]  w_CIRSSrs2_5;               // CI/CR/CSS型指令的rs2部分
    wire [4:0]  w_CLSABrs1_5;               // CL/CS/CA/CB型指令的rs1部分
    wire [4:0]  w_CLSABrs2_5;               // CL/CS/CA/CB型指令的rs2部分
    wire [1:0]  w_CBFUNCT2_2;               // CB型指令的FUNCT2
    wire [1:0]  w_CAFUNCT2_2;               // CA型指令的FUNCT2
    wire [4:0]  w_CSSRs2_5;                 // CSS型指令的rs2部分

    wire        w_CIRrs1NotZero_1;          // CI/CR型指令的rs1部分非零
    wire        w_CIRrs1Zero_1;             // CI/CR型指令的rs1部分为零
    wire        w_CIRrs2NotZero_1;          // CI/CR型指令的rs2部分非零
    wire        w_CIRrs2Zero_1;             // CI/CR型指令的rs2部分为零

    assign w_COP_2              =  i_inst_16[ 1: 0]; 
    assign w_CFUNCT3_3          =  i_inst_16[15:13];
    assign w_CBFUNCT2_2         =  i_inst_16[11:10];
    assign w_CAFUNCT2_2         =  i_inst_16[ 6: 5];
    assign w_CFUNCT4_4          =  i_inst_16[15:12];
    assign w_CFUNCT6_6          =  i_inst_16[15:10];

    assign w_CIRrs1_5           =  i_inst_16[11: 7];
    assign w_CIRrs1NotZero_1    = |w_CIRrs1_5;
    assign w_CIRrs1Zero_1       = ~w_CIRrs1NotZero_1;

    //assign w_CRrs2              =  i_inst_16[ 6: 2];
    assign w_CIRrs2NotZero_1    = |w_CIRSSrs2_5;
    assign w_CIRrs2Zero_1       = ~w_CIRrs2NotZero_1;

    assign w_CLSABrs1_5         = {2'b01,i_inst_16[9:7]};
    assign w_CLSABrs2_5         = {2'b01,i_inst_16[4:2]};
    assign w_CIRSSrs2_5         = i_inst_16[6:2];



    //----------Load and Store Instructions(读写指令)----------//

    // Stack-Pointer-Based Loads and Stores             (基于栈指针的读写)
    assign w_CLWSP_1    = {w_CFUNCT3_3 == `C_FUNCT3_LWSP}   & {w_COP_2 == `C_OP_C2  }    & w_CIRrs1NotZero_1 ;
    assign w_CSWSP_1    = {w_CFUNCT3_3 == `C_FUNCT3_SWSP}   & {w_COP_2 == `C_OP_C2  };

    // Register-Based Loads and Stores                  (基于寄存器的读写)
    assign w_CLW_1      = {w_CFUNCT3_3 == `C_FUNCT3_LW}     & {w_COP_2 == `C_OP_C0  };
    assign w_CSW_1      = {w_CFUNCT3_3 == `C_FUNCT3_SW}     & {w_COP_2 == `C_OP_C0  };

    //----------Control Transfer Instuctions(控制权转让指令)----------//
    assign w_CJ_1       = {w_CFUNCT3_3 == `C_FUNCT3_J   }   & {w_COP_2 == `C_OP_C1  };
    assign w_CJAL_1     = {w_CFUNCT3_3 == `C_FUNCT3_JAL }   & {w_COP_2 == `C_OP_C1  };
    assign w_CJR_1      = {w_CFUNCT4_4 == `C_FUNCT4_JR  }   & {w_COP_2 == `C_OP_C2  }   & w_CIRrs2Zero_1    & w_CIRrs1NotZero_1;
    assign w_CJALR_1    = {w_CFUNCT4_4 == `C_FUNCT4_JALR}   & {w_COP_2 == `C_OP_C2  }   & w_CIRrs2Zero_1    & w_CIRrs1NotZero_1;
    assign w_CBEQZ_1    = {w_CFUNCT3_3 == `C_FUNCT3_BEQZ}   & {w_COP_2 == `C_OP_C1  };
    assign w_CBNEZ_1    = {w_CFUNCT3_3 == `C_FUNCT3_BNEZ}   & {w_COP_2 == `C_OP_C1  };

    //----------Interger Computational Instructions(整数运算指令)----------//

    // Interger Constant-Generation Instructions        (整型常数生成指令)
    assign w_CLI_1      = {w_CFUNCT3_3 == `C_FUNCT3_LI  }   & {w_COP_2 == `C_OP_C1 }    & w_CIRrs1NotZero_1 ;
    assign w_CLUI_1     = {w_CFUNCT3_3 == `C_FUNCT3_LUI }   & {w_COP_2 == `C_OP_C1 }    & w_CIRrs1NotZero_1     & ~{w_CIRrs1_5 == 5'd2};

    // Integer Register-Immediate Operations            (整型寄存器立即数间操作)
    assign w_CADDI_1    = {w_CFUNCT3_3 == `C_FUNCT3_ADDI        }& {w_COP_2 == `C_OP_C1 }   &  w_CIRrs1NotZero_1    ;
    assign w_CADDI16SP_1= {w_CFUNCT3_3 == `C_FUNCT3_ADDI16SP    }& {w_COP_2 == `C_OP_C1 }   & {w_CIRrs1_5 == 5'd2}  ;
    assign w_CADDI4SPN_1= {w_CFUNCT3_3 == `C_FUNCT3_ADDI4SPN    }& {w_COP_2 == `C_OP_C0 };
    assign w_CSLLI_1    = {w_CFUNCT3_3 == `C_FUNCT3_SLLI        }& {w_COP_2 == `C_OP_C2 }   &  w_CIRrs1NotZero_1;
    assign w_CSRLI_1    = {w_CFUNCT3_3 == `C_FUNCT3_MISC_ALU    }& {w_COP_2 == `C_OP_C1 }   & {w_CBFUNCT2_2 == `C_FUNCT2_SRLI};
    assign w_CSRAI_1    = {w_CFUNCT3_3 == `C_FUNCT3_MISC_ALU    }& {w_COP_2 == `C_OP_C1 }   & {w_CBFUNCT2_2 == `C_FUNCT2_SRAI};
    assign w_CANDI_1    = {w_CFUNCT3_3 == `C_FUNCT3_MISC_ALU    }& {w_COP_2 == `C_OP_C1 }   & {w_CBFUNCT2_2 == `C_FUNCT2_ANDI};

    // Integer Register-Register Operations             (整型寄存器间操作)
    assign w_CMV_1      = {w_CFUNCT4_4 == `C_FUNCT4_MV  }& {w_COP_2 == `C_OP_C2 }   & w_CIRrs1NotZero_1 & w_CIRrs2NotZero_1;
    assign w_CADD_1     = {w_CFUNCT4_4 == `C_FUNCT4_ADD }& {w_COP_2 == `C_OP_C2 }   & w_CIRrs1NotZero_1 & w_CIRrs2NotZero_1;
    assign w_CAND_1     = {w_CFUNCT6_6 == `C_FUNCT6_AND }& {w_COP_2 == `C_OP_C1 }   & {w_CAFUNCT2_2 == `C_FUNCT2_AND    };
    assign w_COR_1      = {w_CFUNCT6_6 == `C_FUNCT6_OR  }& {w_COP_2 == `C_OP_C1 }   & {w_CAFUNCT2_2 == `C_FUNCT2_OR     };
    assign w_CXOR_1     = {w_CFUNCT6_6 == `C_FUNCT6_XOR }& {w_COP_2 == `C_OP_C1 }   & {w_CAFUNCT2_2 == `C_FUNCT2_XOR    };
    assign w_CSUB_1     = {w_CFUNCT6_6 == `C_FUNCT6_SUB }& {w_COP_2 == `C_OP_C1 }   & {w_CAFUNCT2_2 == `C_FUNCT2_SUB    };

    // Defined Illegal Instruction                      (定义的非法指令)
    assign w_illegal_1  = {i_inst_16 == 16'b0};

    // NOP Instruction                                  (NOP指令)
    assign w_CNOP_1     = {w_CFUNCT3_3 == `C_FUNCT3_ADDI    }& {w_COP_2 == `C_OP_C1 }   & w_CIRrs1Zero_1;

    // Breakpoint Instruction                           (断点指令)
    assign w_CEBREAK_1  = {w_CFUNCT4_4 == `C_FUNCT4_EBREAK  }& {w_COP_2 == `C_OP_C2 }   & w_CIRrs1Zero_1    & w_CIRrs2Zero_1;

//------------------------------指令译码{end}------------------------------//

//------------------------------指令分类{begin}------------------------------//

// RV32C拓展后指令
    //C.LWSP    =   lw      rd      ,offset(x2).                // CI       rd
    //C.SWSP    =   sw      rs2     ,offset(x2).                // CSS      rs2
    //C.LW      =   lw      rd'     ,offset(rs1').              // CL       rs1 rd
    //C.SW      =   sw      rs2     ,offset(rs1')               // CS       rs1 rs2
    //C.LI      =   addi    rd      ,x0             ,imm.       // CI       rd
    //C.LUI     =   lui     rd      ,nzimm.                     // CI       rd
    //C.ADDI    =   addi    rd      ,rd             ,nzimm.     // CI       rd
    //C.ADDI16SP=   addi    x2      ,x2             ,nzimm.     // CI       rd/rs1
    //C.ADDI4SPN=   addi    rd'     ,x2             ,nzimm.     // CIW      rd
    //C.SLLI    =   slli    rd      ,rd             ,shamt.     // CI       rd/rs1
    //C.SRLI    =   srli    rd'     ,rd'            ,shamt.     // CB       rd/rs1
    //C.SRAI    =   srai    rd'     ,rd'            ,shamt.     // CB       rd/rs1
    //C.ANDI    =   andi    rd'     ,rd'            ,imm.       // CB       rd/rs1
    //C.MV      =   add     rd'     ,x0             ,rs2'.      // CR       rd/rs1  rs2
    //C.ADD     =   add     rd'     ,rd'            ,rs2'.      // CA       rd/rs1  rs2
    //C.AND     =   and     rd'     ,rd'            ,rs2'.      // CA       rd/rs1  rs2
    //C.OR      =   or      rd'     ,rd'            ,rs2'.      // CA       rd/rs1  rs2
    //C.XOR     =   xor     rd'     ,rd'            ,rs2'.      // CA       rd/rs1  rs2
    //C.SUB     =   sub     rd'     ,rd'            ,rs2'.      // CA       rd/rs1  rs2
    //C.NOP     =   addi    0       ,0.

// RV32C立即数与拓展后指令立即数映射关系
    //C.LWSP    12 6:2      ,5 4:2 7:6          // imm3
    //C.SWSP    12:7        ,5:2 7:6            // imm4
    //C.LW      12:10 6:5   ,5:3 2 6            // imm2
    //C.SW      12:10 6:5   ,5:3 2 6            // imm2
    //C.LI      12 6:2      ,5 4:0              // imm1
    //C.LUI     12 6:2      ,17 16:12           // imm1
    //C.ADDI    12 6:2      ,5 4:0              // imm1
    //C.ADDI16SP 12 6:2     ,9 4 6 8:7 5        // imm5
    //C.ADDI4SPN 12:5       ,5:4 9:6 2 3        // imm6
    //C.SLLI    12 6:2      ,5 4:0              // imm1
    //C.SRLI    12 6:2      ,5 4:0              // imm1
    //C.SRAI    12 6:2      ,5 4:0              // imm1
    //C.ANDI    12 6:2      ,5 4:0              // imm1


//分类类型定义

    // 按拓展后指令分类
    wire    w_sw_1      ,w_lw_1     ,w_addi_1   ,w_add_1    ;
    wire    w_lui_1     ,w_slli_1   ,w_srli_1   ,w_srai_1   ;
    wire    w_andi_1    ,w_and_1    ,w_or_1     ,w_xor_1    ;
    wire    w_sub_1     ,w_ebreak_1 ,w_jal_1    ,w_jalr_1   ;
    wire    w_beq_1     ,w_bne_1    ;

    // 按指令类型分类
    wire    w_CR_1  , w_CI_1    , w_CSS_1   , w_CIW_1   ,
            w_CL_1  , w_CS_1    , w_CA_1    , w_CB_1    ,
            w_CJType_1  ;

    // 按寄存器号位置分类
    wire    w_CIR_1 , w_CLSAB_1 , w_CIRSS_1 , w_CIWL_1  , w_CAB_1;

    // 按拓展前后立即数映射关系分类
    wire    w_imm1_1, w_imm2_1  , w_imm3_1  , w_imm4_1,
            w_imm5_1, w_imm6_1, w_imm7_1;

    // 特殊分类
    wire    w_SP_1;     // 对栈寄存器的操作
    wire    w_CisJ_1;   // 指令为跳转(无链接)
    wire    w_CisJAL_1; // 指令为跳转链接
    wire    w_CBI_1;    // 比较指令

//指令分类

    // 按拓展后指令分类
    assign w_lw_1       = w_CLWSP_1 | w_CLW_1   ;
    assign w_sw_1       = w_CSWSP_1 | w_CSW_1   ;
    assign w_addi_1     = w_CLI_1   | w_CADDI_1 | w_CADDI16SP_1 | w_CADDI4SPN_1 | w_CNOP_1;
    assign w_add_1      = w_CADD_1  | w_CMV_1   ;
    assign w_lui_1      = w_CLUI_1  ;
    assign w_slli_1     = w_CSLLI_1 ;
    assign w_srli_1     = w_CSRLI_1 ;
    assign w_srai_1     = w_CSRAI_1 ;
    assign w_andi_1     = w_CANDI_1 ;
    assign w_and_1      = w_CAND_1  ;
    assign w_or_1       = w_COR_1   ;
    assign w_xor_1      = w_CXOR_1  ;
    assign w_sub_1      = w_CSUB_1  ;
    assign w_ebreak_1   = w_CEBREAK_1;
    assign w_jal_1      = w_CJ_1    | w_CJAL_1  ;
    assign w_jalr_1     = w_CJR_1   |w_CJALR_1  ;
    assign w_beq_1      = w_CBEQZ_1 ;
    assign w_bne_1      = w_CBNEZ_1 ;

    // 按指令类型分类
    assign w_CR_1       = w_CMV_1       | w_CADD_1      | w_CJALR_1 | w_CJR_1   ;
    assign w_CI_1       = w_CLI_1       | w_CLUI_1      | w_CADDI_1
                        | w_CLWSP_1     | w_CADDI16SP_1 | w_CSLLI_1 | w_CNOP_1  ;
    assign w_CSS_1      = w_CSWSP_1 ;
    assign w_CIW_1      = w_CADDI4SPN_1;
    assign w_CL_1       = w_CLW_1   ;
    assign w_CS_1       = w_CSW_1   ;
    assign w_CA_1       = w_CAND_1      | w_COR_1       | w_CXOR_1      | w_CSUB_1  ;
    assign w_CB_1       = w_CSRLI_1     | w_CSRAI_1     | w_CANDI_1     | w_CBEQZ_1 | w_CBNEZ_1;
    assign w_CJType_1   = w_CJAL_1  ;
    assign w_CIR_1      = w_CI_1    | w_CR_1;
    assign w_CIRSS_1    = w_CIR_1   | w_CSS_1;
    assign w_CLSAB_1    = w_CL_1    | w_CS_1        | w_CAB_1;
    assign w_CIWL_1     = w_CIW_1   | w_CL_1;
    assign w_CAB_1      = w_CA_1    | w_CB_1;

    // 按拓展前后立即数映射关系分类
    assign w_imm1_1 = w_CLI_1   | w_CLUI_1  | w_CADDI_1 | w_CANDI_1;
    assign w_imm2_1 = w_CLW_1   | w_CSW_1;
    assign w_imm3_1 = w_CLWSP_1;
    assign w_imm4_1 = w_CSWSP_1;
    assign w_imm5_1 = w_CADDI16SP_1;
    assign w_imm6_1 = w_CADDI4SPN_1;
    assign w_imm7_1 = w_CBI_1;

    // 特殊分类
    assign w_SP_1       = w_CLWSP_1 | w_CSWSP_1 | w_CADDI4SPN_1 | w_CADDI16SP_1;
    assign w_CisJ_1     = w_CJ_1    | w_CJR_1   ;
    assign w_CisJAL_1   = w_CJAL_1  | w_CJALR_1 ;
    assign w_CBI_1      = w_CBNEZ_1 | w_CBEQZ_1 ;

//------------------------------指令分类{begin}------------------------------//

//------------------------------数据整理{begin}------------------------------//

    wire [ 4:0] w_rs1_5;    // rs1线
    wire [ 4:0] w_rs2_5;    // rs2线
    wire [ 4:0] w_rd_5;     // rd线
    wire [ 4:0] w_shamt_5;  // shamt线
    wire [11:0] w_imm_12;   // imm线
    wire [19:0] w_imm_20;   // imm线
    wire [6:0]  w_imm_7;    // imm线
    wire [4:0]  w_imm_5;    // imm线

    // rs1线数据生成
    assign w_rs1_5      = {{5{w_CIR_1 & ~w_SP_1  & ~w_CLI_1 }}  & w_CIRrs1_5    }
                        | {{5{w_CLSAB_1                     }}  & w_CLSABrs1_5  }
                        | {{5{w_SP_1                        }}  & 5'b00010      }
                        | {{5{w_CLI_1                       }}  & 5'b0          };

    // rs2线数据生成
    assign w_rs2_5      = {{5{w_CIRSS_1             }}   & w_CIRSSrs2_5  }
                        | {{5{w_CLSAB_1 & ~w_CBI_1  }}   & w_CLSABrs2_5  }
                        | {{5{w_CBI_1               }}   & {5'b0        }};

    // rd线数据生成
    assign w_rd_5       = {{5{w_CIR_1 & ~w_CisJAL_1 & ~w_CisJ_1 }}   & w_CIRrs1_5    }
                        | {{5{w_CIWL_1                          }}   & w_CLSABrs2_5  }
                        | {{5{w_CAB_1                           }}   & w_CLSABrs1_5  }
                        | {{5{w_CisJ_1                          }}   & {5'b0        }}
                        | {{5{w_CisJAL_1                        }}   & {5'b1        }};

    // shamt线数据生成
    assign w_shamt_5    = { i_inst_16[12]   ,i_inst_16[6:2]};

    // imm线数据生成
    assign w_imm_12     = {{12{w_imm1_1}}   & {6'b0,i_inst_16[  12] ,i_inst_16[ 6: 2]                                                           }}
                        | {{12{w_imm2_1}}   & {5'b0,i_inst_16[   5] ,i_inst_16[12:10]   ,i_inst_16[6]   ,2'b0                                   }}
                        | {{12{w_imm3_1}}   & {4'b0,i_inst_16[ 3:2] ,i_inst_16[   12]   ,i_inst_16[6:4] ,2'b0                                   }}
                        | {{12{w_imm4_1}}   & {4'b0,i_inst_16[ 8:7] ,i_inst_16[12: 9]   ,2'b0                                                   }}
                        | {{12{w_imm5_1}}   & {2'b0,i_inst_16[  12] ,i_inst_16[ 4: 3]   ,i_inst_16[5]   ,i_inst_16[2]   ,i_inst_16[6]   ,4'b0   }}
                        | {{12{w_imm6_1}}   & {2'b0,i_inst_16[10:7] ,i_inst_16[12:11]   ,i_inst_16[5]   ,i_inst_16[6]                           }};

    assign w_imm_20     = {{20{w_jal_1 }}   & {1'b0,i_inst_16[   8] ,i_inst_16[10: 9]   ,i_inst_16[6]   ,i_inst_16[7]   ,i_inst_16[2]
                                            , i_inst_16[11]         ,i_inst_16[ 5: 3]   ,i_inst_16[12]  ,8'b0                           }};

    assign w_imm_7      = {3'b0,i_inst_16[12],i_inst_16[6:5],i_inst_16[2]};

    assign w_imm_5      ={i_inst_16[11:10],i_inst_16[4:3],1'b0};

//------------------------------数据整理{end}------------------------------//

//------------------------------指令输出{begin}------------------------------//

    assign o_inst_32    = {{32{w_lw_1   }}  & {w_imm_12         ,w_rs1_5    ,`FUNCT3_LW     ,w_rd_5         ,`OP_LOAD                   }}      // 输出lw指令
                        | {{32{w_sw_1   }}  & {w_imm_12[11:5]   ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_SW     ,w_imm_12[4:0]  ,`OP_STORE  }}      // 输出sw指令
                        | {{32{w_addi_1 }}  & {w_imm_12         ,w_rs1_5    ,`FUNCT3_ADDI   ,w_rd_5         ,`OP_OP_IMM                 }}      // 输出addi指令
                        | {{32{w_add_1  }}  & {7'b0             ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_ADD    ,w_rd_5         ,`OP_OP     }}      // 输出add指令
                        | {{32{w_lui_1  }}  & {20'b0            ,w_imm_12   ,w_rd_5         ,`OP_LUI                                    }}      // 输出lui指令
                        | {{32{w_slli_1 }}  & {7'b0             ,w_shamt_5  ,w_rs1_5        ,`FUNCT3_SLLI   ,w_rd_5         ,`OP_OP_IMM }}      // 输出slli指令
                        | {{32{w_srli_1 }}  & {7'b0             ,w_shamt_5  ,w_rs1_5        ,`FUNCT3_SRLI   ,w_rd_5         ,`OP_OP_IMM }}      // 输出srli指令
                        | {{32{w_srai_1 }}  & {7'b0100000       ,w_shamt_5  ,w_rs1_5        ,`FUNCT3_SRAI   ,w_rd_5         ,`OP_OP_IMM }}      // 输出srai指令
                        | {{32{w_andi_1 }}  & {w_imm_12         ,w_rs1_5    ,`FUNCT3_ANDI   ,w_rd_5         ,`OP_OP_IMM                 }}      // 输出andi指令
                        | {{32{w_and_1  }}  & {7'b0             ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_AND    ,w_rd_5         ,`OP_OP     }}      // 输出and指令
                        | {{32{w_or_1   }}  & {7'b0             ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_OR     ,w_rd_5         ,`OP_OP     }}      // 输出or指令
                        | {{32{w_xor_1  }}  & {7'b0             ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_XOR    ,w_rd_5         ,`OP_OP     }}      // 输出xor指令
                        | {{32{w_sub_1  }}  & {7'b0100000       ,w_rs2_5    ,w_rs1_5        ,`FUNCT3_SUB    ,w_rd_5         ,`OP_OP     }}      // 输出sub指令
                        | {{32{w_ebreak_1}} & {12'b1            ,5'b0       ,3'b0           ,5'b0           ,`OP_SYSTEM                 }}      // 输出ebreak指令
                        | {{32{w_jal_1  }}  & {w_imm_20         ,w_rd_5     ,`OP_JAL                                                    }}      // 输出JAL指令
                        | {{32{w_jalr_1 }}  & {12'b0            ,w_rs1_5    ,3'b0           ,w_rd_5         ,`OP_JALR                   }}      // 输出JALR指令
                        | {{32{w_beq_1  }}  & {w_imm_7          ,w_rs2_5    ,w_rs1_5    ,3'b0           ,w_imm_5        ,`OP_BRANCH     }}      // 输出BEQ指令
                        | {{32{w_bne_1  }}  & {w_imm_7          ,w_rs2_5    ,w_rs1_5    ,3'b1           ,w_imm_5        ,`OP_BRANCH     }};     // 输出BNE指令

//------------------------------指令输出{end}------------------------------//

endmodule
