//-----------------------------------------------
//    module name: m_if_id
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 该模块为一二级流水线之间的级见寄存器模块，当需要暂停流水线时，向第二级流水传递空指令，
//                 否则正常传递指令
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

    module m_if_id(
    input  wire        rstn,            
    input  wire        clk,      
			    
    input  wire [63:0] i_instAndPc_64,      //指令和指令PC
    input  wire        i_holdflag_1,        //暂停流水线信号
			    
    output wire [63:0] o_instAndPc_64
);
	 
    localparam nop64 = 64'h0000_0000_0000_0013;
	reg  [63:0] r_instAndPc_64 ;

    //是否阻塞流水线
    always @ (posedge clk or negedge rstn) begin
        if(!rstn) begin
            r_instAndPc_64 <= nop64;
        end 
        else begin
            //若暂停流水线则传递空指令，否则正常传递指令
			if(i_holdflag_1) begin
				r_instAndPc_64 <= r_instAndPc_64;
			end
			else begin
				r_instAndPc_64 <= i_instAndPc_64;
			end
		end
    end
    
    assign o_instAndPc_64 = r_instAndPc_64;
endmodule
