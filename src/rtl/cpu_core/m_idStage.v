//-----------------------------------------------
//    module name: m_idStage
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 第二级流水模块中实例化了译码模块，执行模块和lsu模块，
//                 主要完成指令译码，alu运算，乘除法，以及访存和写回的执行
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module m_idStage(
    input wire         clk,
    input wire         rstn,
					  
	input  wire [63:0] i_PcAndinstr_64,
    //流水线控制      
    output wire        o_idPipeHold_1,
    output wire [ 4:0] o_exception_5,            //异常编码，使用格雷码，发往中断处理模块
    //GRF 数据
	output wire [47:0] o_idToGrf_48,
	input  wire [63:0] i_grfToId_64,
	//CSR 数据
	input  wire [31:0] i_csrData_32,             //CSR寄存器数据，写入GRF
	output wire [44:0] o_idToCsr_45,
	//Dcache 数据
	input  wire [31:0] i_dcacheData_32,          //从Dcache读取的数据，用于load操作
	output wire [31:0] o_storeAddr_32,           //Dcache的写地址，用于store操作				  
    output wire [ 3:0] o_storeWen_4,             //Dcache写使能信号，用于store操作
    output wire [31:0] o_wdata_32,               //写入Dcache的数据，用于store操作
    output wire        dbus_req_valid_o,
    input  wire        dbus_req_ready_i,
    input  wire        dbus_rsp_valid_i,
    output wire        dbus_rsp_ready_o
    
);
//-----{信号打包}begin
	wire [11:0] w_csrAddr_12;                    //CSR寄存器的写地址
	wire [31:0] w_csrWdata_32;                   //CSR寄存器的写数据
	wire        w_csrWen_1;                      //CSR寄存器的写使能
	assign o_idToCsr_45 = {	w_csrWen_1,
							w_csrWdata_32,
							w_csrAddr_12        } ;
	
	wire        w_loadWen_1;
    wire [ 4:0] w_rdAddr_5 ;  
    wire [ 4:0] w_rs1Addr_5;
    wire [ 4:0] w_rs2Addr_5;
    wire [31:0] w_lsuRdata_32;
	assign o_idToGrf_48 = {	w_loadWen_1,  
                            w_rdAddr_5,   
                            w_rs1Addr_5,
                            w_rs2Addr_5,
                            w_lsuRdata_32 } ;    

//-----{信号打包}end


//-----{译码模块}begin
    wire [125:0] w_DecoderExeBus_126;

    m_decode decode(
		.i_PcAndinstr_64		(i_PcAndinstr_64	),
		.i_grfToId_64			(i_grfToId_64		),
        .i_csrValue_32          (i_csrData_32       ),
        .o_rs1Addr_5            (w_rs1Addr_5        ),
        .o_rs2Addr_5            (w_rs2Addr_5        ),
        .o_csrAddr_12           (w_csrAddr_12       ),          
        .o_csrWen_1				(w_csrWen_1			),   
        .o_DecoderExeBus_126    (w_DecoderExeBus_126)
    );
//-----{译码模块}end

//-----{执行模块调用}begin
    wire [102:0] w_ExeLsuBus_103;
    wire 		 w_muldivHold_1;
    m_exe exe(
        .clk                    (clk                ),
        .rstn                   (rstn               ),
        .i_DecoderExeBus_126    (w_DecoderExeBus_126),
        .o_ExeLsuBus_103        (w_ExeLsuBus_103    ),
		.o_rdAddr_5				(w_rdAddr_5			),          
        .o_csrWdata_32			(w_csrWdata_32		),
        .o_muldivHold_1         (w_muldivHold_1     )
    );

//-----{执行模块调用}end

//-----{访存(写回)模块调用}begin 
    wire         w_lsuPipeHold_1;
    wire         w_lsuEnable_1;
    m_load_store_unit lsu (
        .clk			        (clk				),
        .rstn			        (rstn				),
						        
        .i_ExeLSUBus_103        (w_ExeLsuBus_103	),  
						        
        .o_memAddr_32	        (o_storeAddr_32		),
        .i_memData_32	        (i_dcacheData_32	),
        .o_memData_32	        (o_wdata_32			),
        .o_memWen_4		        (o_storeWen_4		),
						        
        .i_rsp_vaild	        (dbus_rsp_valid_i	),
        .o_lsuPipeHold_1        (w_lsuPipeHold_1	),
		.o_lsuEnable_1	        (w_lsuEnable_1		),
						        
        .o_rdData_32	        (w_lsuRdata_32		), 
        .o_rdWen_1		        (w_loadWen_1		),
						        
        .o_exception_5	        (o_exception_5		)
		
    );
//-----{访存(写回)模块调用}end

//流水线控制信号

	assign dbus_req_valid_o = w_lsuEnable_1;
    assign dbus_rsp_ready_o = 1'b1;
	
    assign o_idPipeHold_1   = w_lsuPipeHold_1 | w_muldivHold_1;

endmodule
