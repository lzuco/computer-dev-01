//-----------------------------------------------
//    module name: m_alu
//    author: Wei Ren
//  
//    version: 1st version (2021-10-01)
//    description: 算术逻辑单元，实现的功能有加法操作、减法操作、小于置位操作、无符号小于置位操作、
//                 与操作、或操作、异或操作、逻辑左移操作、逻辑右移操作、算术右移操作、高位加载操作
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_alu (
    input  wire [10:0] i_aluControl_11,
    input  wire [31:0] i_aluOperand1_32,
    input  wire [31:0] i_aluOperand2_32,
    output wire [31:0] o_aluResult_32
);

//-----{aluControl解码}begin

    wire w_add_1;                               // 加法操作
    wire w_sub_1;                               // 减法操作
    wire w_slt_1;                               // 小于置位操作
    wire w_sltu_1;                              // 无符号小于置位操作
    wire w_and_1;                               // 与操作
    wire w_or_1;                                // 或操作
    wire w_xor_1;                               // 异或操作
    wire w_sll_1;                               // 逻辑左移操作
    wire w_srl_1;                               // 逻辑右移操作
    wire w_sra_1;                               // 算术右移操作
    wire w_lui_1;                               // 高位加载操作

    assign w_add_1  = i_aluControl_11[10];
    assign w_sub_1  = i_aluControl_11[ 9];
    assign w_slt_1  = i_aluControl_11[ 8];
    assign w_sltu_1 = i_aluControl_11[ 7];
    assign w_and_1  = i_aluControl_11[ 6];
    assign w_or_1   = i_aluControl_11[ 5];
    assign w_xor_1  = i_aluControl_11[ 4];
    assign w_sll_1  = i_aluControl_11[ 3];
    assign w_srl_1  = i_aluControl_11[ 2];
    assign w_sra_1  = i_aluControl_11[ 1];
    assign w_lui_1  = i_aluControl_11[ 0];

//-----{aluControl解码}end

    wire [31:0] w_addSubResult_32; //这部分放到前面，先声明后使用~
    wire [31:0] w_sltResult_32;
    wire [31:0] w_sltuResult_32;
    wire [31:0] w_andResult_32;
    wire [31:0] w_orResult_32;
    wire [31:0] w_xorResult_32;
    wire [31:0] w_sllResult_32;
    wire [31:0] w_srlResult_32;
    wire [31:0] w_sraResult_32;
    wire [31:0] w_luiResult_32;

    assign w_andResult_32   = i_aluOperand1_32 & i_aluOperand2_32;          // 与运算
    assign w_orResult_32    = i_aluOperand1_32 | i_aluOperand2_32;          // 或运算
    assign w_xorResult_32   = i_aluOperand1_32 ^ i_aluOperand2_32;          // 异或运算
    assign w_luiResult_32   = i_aluOperand2_32;                             // lui执行


//-----{加法器}begin
//add,sub,slt,sltu均使用该模块
    wire [31:0] w_adderOperand1_32;                                         // 加法操作数1
    wire [31:0] w_adderOperand2_32;                                         // 加法操作数2
    wire        w_adderCin_1;                                               // 加法进位输入
    wire [32:0] w_adderResult_33;                                           // 加法结果
    wire        w_adderCout_1;                                              // 加法进位输出
    assign w_adderOperand1_32   = i_aluOperand1_32;
    assign w_adderOperand2_32   = w_add_1   ?  i_aluOperand2_32
                                            : ~i_aluOperand2_32;
    assign w_adderCin_1         = ~w_add_1;
    assign w_adderResult_33     = w_adderOperand1_32 + w_adderOperand2_32 + w_adderCin_1;
    assign w_adderCout_1        = w_adderResult_33[32];

    //加减结果
    assign w_addSubResult_32    = w_adderResult_33[31:0];

    //slt结果
    //adderOperand1[31] adderOperand2[31] adderResult[31]
    //       0             1                  X(0或1)       "正-负"，显然小于不成立
    //       0             0                    1           相减为负，说明小于
    //       0             0                    0           相减为正，说明不小于
    //       1             1                    1           相减为负，说明小于
    //       1             1                    0           相减为正，说明不小于
    //       1             0                  X(0或1)       "负-正"，显然小于成立
    assign w_sltResult_32[31:1] = 31'd0;
    assign w_sltResult_32[0]    = (  i_aluOperand1_32[31] & ~i_aluOperand2_32[31])
                                | (~(i_aluOperand1_32[31] ^  i_aluOperand2_32[31])
                                &   w_addSubResult_32[31]                        );

    //sltu结果
    //对于32位无符号数比较，相当于33位有符号数（{1'b0,operand1}和{1'b0,operand2}）的比较，最高位0为符号位
    //故，可以用33位加法器来比较大小，需要对{1'b0,operand2}取反,即需要{1'b0,operand1}+{1'b1,~operand2}+cin
    //但此处用的为32位加法器，只做了运算:   operand1   +    ~operand2   +cin
    //32位加法的结果为{adderCout,adderSubResult},则33位加法结果应该为{adderCout+1'b1,adder_result}
    //对比slt结果注释，知道，此时判断大小属于第二三种情况，即源操作数1符号位为0，源操作数2符号位为0
    //结果的符号位为1，说明小于，即adderCout+1'b1为2'b01，即adderCout为0
    assign w_sltuResult_32  = {31'd0,~w_adderCout_1};

//-----{加法器}end

//-----{移位器}begin

    // 逻辑左移
    assign w_sllResult_32   =($unsigned(i_aluOperand1_32)) << i_aluOperand2_32[4:0]; //fix bug,两个操作数位置反了，srai等指令特殊要求未完成

    // 逻辑右移
    assign w_srlResult_32   =($unsigned(i_aluOperand1_32)) >> i_aluOperand2_32[4:0];

    // 算数右移
    assign w_sraResult_32   =(($signed(i_aluOperand1_32)) >>> i_aluOperand2_32[4:0]);


//-----{移位器}end

//-----{alu输出结果生成}begin

    assign o_aluResult_32   = {{32{w_add_1 | w_sub_1}}  & w_addSubResult_32 }   // 输出加减法结果
                            | {{32{w_slt_1          }}  & w_sltResult_32    }   // 输出小于置位结果
                            | {{32{w_sltu_1         }}  & w_sltuResult_32   }   // 输出无符号小于置位结果
                            | {{32{w_and_1          }}  & w_andResult_32    }   // 输出与结果
                            | {{32{w_or_1           }}  & w_orResult_32     }   // 输出或结果
                            | {{32{w_xor_1          }}  & w_xorResult_32    }   // 输出异或结果
                            | {{32{w_sll_1          }}  & w_sllResult_32    }   // 输出逻辑左移结果
                            | {{32{w_srl_1          }}  & w_srlResult_32    }   // 输出逻辑右移结果
                            | {{32{w_sra_1          }}  & w_sraResult_32    }   // 输出算数右移结果
                            | {{32{w_lui_1          }}  & w_luiResult_32    };  //添加输出 lui结果 ，12改为32，by.lzc

//-----{alu输出结果生成}begin
endmodule
