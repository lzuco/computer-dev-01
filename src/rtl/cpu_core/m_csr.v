//-----------------------------------------------
//    module name: m_csr
//    author: LuYihua
//  
//    version: 1st version (2021-10-01)
//    description: CSR寄存器组，控制和状态寄存器，用于配置或记录一些运行的状态，
//                 CSR寄存器是处理器内核内部的寄存器，使用专有的12位地址编码空
//                 间。第二级流水读写指令优先于第一级流水。当第二级和第一级流水
//                 同时产生异常时优先对第二级进行响应。在机器模式下可以通过特权
//                 指令直接对CSR寄存器进行读写。特权指令可以直接对CSR寄存器进行修改。
//        
//-----------------------------------------------
`timescale 1ns / 1ps

module m_csr(

    input  wire         clk,
    input  wire 	    rstn,
    
	input  wire [128:0] i_intFToCsr_129,
	input  wire [128:0] i_intSToCsr_129,
	input  wire [ 44:0] i_idToCsr_45,
	
	output wire [ 31:0] o_csrValue_32,              //使用特权指令输出的CSR寄存器的值
    output wire [ 31:0] o_mtvecValue_32,            //输出MTVEC寄存器的值
    output wire [ 31:0] o_mispValue_32,             //输出MSIP寄存器的值
    output wire [ 31:0] o_mepcValue_32,             //输出MEPC寄存器的值
    output wire [ 31:0] o_mstatusValue_32           //输出MSTATUS寄存器的值
	

    );

	wire        w_csrEn1_1;                         //第一级流水输入CSR写入值的使能信号。判断是否将当前输入写入CSR寄存器
	wire [31:0] w_mcauseValue1_32;                  //第一级流水写入的MCAUSE寄存器值
	wire [31:0] w_mepcValue1_32;                    //第一级流水写入的MEPC寄存器值
	wire [31:0] w_mtvalValue1_32;                   //第一级流水写入的MTVAL寄存器值
	wire [31:0] w_mstatusValue1_32;                 //第一级流水写入的MSTATUS寄存器值
	wire        w_csrEn2_1;                         //第二级流水输入CSR写入值的使能信号。判断是否将当前输入写入CSR寄存器
	wire [31:0] w_mcauseValue2_32;                  //第二级流水写入的MCAUSE寄存器值
	wire [31:0] w_mepcValue2_32;                    //第二级流水写入的MEPC寄存器值
	wire [31:0] w_mtvalValue2_32;                   //第二级流水写入的MTVAL寄存器值
	wire [31:0] w_mstatusValue2_32;                 //第二级流水写入的MSTATUS寄存器值
	wire [31:0] w_csrWriteValue_32;                 //特权指令写入的CSR寄存器值
	wire [11:0] w_csrAddr_12;                       //特权指令写入CSR寄存器的地址
	wire        w_csrWriteEn_1;                     //特权指令读写的CSR使能信号
	
	assign {	w_csrEn1_1,
				w_mcauseValue1_32, 
				w_mepcValue1_32,   
				w_mtvalValue1_32,  
				w_mstatusValue1_32  } = i_intFToCsr_129;
	assign {	w_csrEn2_1,
				w_mcauseValue2_32, 
				w_mepcValue2_32,   
				w_mtvalValue2_32,  
				w_mstatusValue2_32  } = i_intSToCsr_129;
	assign {	w_csrWriteEn_1,
				w_csrWriteValue_32,
				w_csrAddr_12        } = i_idToCsr_45;
				

    //CSR register
    reg [31:0] r_mtvec_32;
    reg [31:0] r_mcause_32;
    reg [31:0] r_mepc_32;
    reg [31:0] r_mstatus_32;
    reg [31:0] r_mtval_32;
    reg [31:0] r_misp_32;
    reg [31:0] r_mscratch_32;
    reg [63:0] r_cycle_64;
	
	reg [31:0] r_csrValue_32;
	assign o_csrValue_32        = r_csrValue_32;
    
    assign o_mtvecValue_32      = r_mtvec_32;
    assign o_mepcValue_32       = r_mepc_32;
    assign o_mstatusValue_32    = r_mstatus_32;
    assign o_mispValue_32       = r_misp_32;
    
    // cycle counter
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            r_cycle_64 <= 64'b0;
        end else begin
            r_cycle_64 <= r_cycle_64 + 1'b1;
        end
    end
    
    // CSR寄存器地址
    localparam CSR_MTVEC    = 12'h305;
    localparam CSR_MCAUSE   = 12'h342;
    localparam CSR_MEPC     = 12'h341;
    localparam CSR_MIE      = 12'h304;
    localparam CSR_MSTATUS  = 12'h300;
    localparam CSR_MSCRATCH = 12'h340;
    localparam CSR_MTVAL    = 12'h343;
    localparam CSR_CYCLE    = 12'hc00;
    localparam CSR_CYCLEH   = 12'hc80;

    always @ (posedge clk or negedge rstn) begin
       if(!rstn) begin
            r_mtvec_32   <= 32'b0;  
            r_mcause_32  <= 32'b0; 
            r_mepc_32    <= 32'b0;   
            r_mstatus_32 <= 32'b0;
            r_mtval_32   <= 32'b0;  
            r_misp_32    <= 32'b0;   
            end else begin
            //根据CSR寄存器地址向CSR寄存器中写数据
            if (w_csrWriteEn_1) begin
                case (w_csrAddr_12)
                    CSR_MTVEC:    begin    r_mtvec_32    <= w_csrWriteValue_32;    end
                    CSR_MCAUSE:   begin    r_mcause_32   <= w_csrWriteValue_32;    end
                    CSR_MEPC:     begin    r_mepc_32     <= w_csrWriteValue_32;    end
                    CSR_MTVAL:    begin    r_mtval_32    <= w_csrWriteValue_32;    end
                    CSR_MSTATUS:  begin    r_mstatus_32  <= w_csrWriteValue_32;    end
                    CSR_MSCRATCH: begin    r_mscratch_32 <= w_csrWriteValue_32;    end
                    default: begin    end
                endcase
            end 
            if(w_csrEn1_1 | w_csrEn2_1)
                begin
                      r_mcause_32  <=   w_csrEn2_1 ? w_mcauseValue2_32  :
                                        w_csrEn1_1 ? w_mcauseValue1_32  :
                                        r_mcause_32;
                      r_mepc_32    <=   w_csrEn2_1 ? w_mepcValue2_32    : 
                                        w_csrEn1_1 ? w_mepcValue1_32    : 
                                         r_mepc_32;
                      r_mtval_32   <=   w_csrEn2_1 ? w_mtvalValue2_32   : 
                                        w_csrEn1_1 ? w_mtvalValue1_32   : 
                                        r_mtval_32;
                      r_mstatus_32 <=   w_csrEn2_1 ? w_mstatusValue2_32 : 
                                        w_csrEn1_1 ? w_mstatusValue1_32 : 
                                        r_mstatus_32;
                end
            end
    end

    always @ (*) begin
            //根据CSR地址读取数据
                case (w_csrAddr_12)
                    CSR_CYCLE:    begin  r_csrValue_32 = r_cycle_64[31:0];  end
                    CSR_CYCLEH:   begin  r_csrValue_32 = r_cycle_64[63:32]; end
                    CSR_MTVEC:    begin  r_csrValue_32 = r_mtvec_32;        end
                    CSR_MCAUSE:   begin  r_csrValue_32 = r_mcause_32;       end
                    CSR_MEPC:     begin  r_csrValue_32 = r_mepc_32;         end
                    CSR_MTVAL:    begin  r_csrValue_32 = r_mtval_32;        end
                    CSR_MSTATUS:  begin  r_csrValue_32 = r_mstatus_32;      end
                    CSR_MSCRATCH: begin  r_csrValue_32 = r_mscratch_32;     end
                    default:      begin  r_csrValue_32 = 32'b0;             end
                endcase
            end 
endmodule
