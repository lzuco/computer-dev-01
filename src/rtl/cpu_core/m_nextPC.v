//-----------------------------------------------
//    module name: m_nextPC
//    author: Sun Zhe
//  
//    version: 1st version (2021-10-01)
//    description: 该模块用于控制下一条指令的PC，在以下几种情况中选择出正确的下一条指令的PC：
//                 1.暂停流水线
//                 2.发生中断
//                 3.指令为跳转类型 
//                 4.压缩（16位）指令
//                 5.正常的下一条指令
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_nextPC(
	input  wire        clk,
    input  wire        rstn,
		   
    input  wire        i_holdflag_1,   //是否暂停信号
    input  wire        i_16flag_1,     //是否为16位指令
	input  wire        i_isIntPC_1,    //是否为中断
    input  wire [31:0] i_intPC_32,     //来自中断的PC
    input  wire [31:0] i_branchPC_32,  //来自跳转的PC
    input  wire        i_isBranch_1,   //是否跳转

	output wire [31:0] o_presentPC_32, //输出当前PC
	output wire [31:0] o_nextPC_32     //输出下一条PC

    );

    reg [31:0] r_nextPC_32;
	reg [31:0] r_presentPC_32;
	reg [31:0] r_presentPC_t_32;  //暂存上一次的presentPC

   always @(posedge clk or negedge rstn)begin
        if(!rstn) begin
			r_presentPC_32   <= `RESETADDR;
			r_presentPC_t_32 <= `RESETADDR;
		end else begin
			r_presentPC_t_32 <= i_holdflag_1 ? r_presentPC_t_32:r_presentPC_32;  //每次都将当前PC暂存至r_presentPC_t_32中，以作暂停流水线用
			r_presentPC_32   <= i_holdflag_1 ? r_presentPC_t_32:r_nextPC_32;
		end
	end	

    always @(*) begin
	    if(!rstn) begin
			r_nextPC_32       = `RESETADDR;
		end else begin
			if(~i_holdflag_1) begin
				if (i_isIntPC_1) begin
					r_nextPC_32         = i_intPC_32;              //中断PC
				end 
				else if (i_isBranch_1) begin
					r_nextPC_32         = i_branchPC_32;           //跳转PC
				end
				else if (i_16flag_1) begin
					r_nextPC_32         = r_presentPC_32;          //16位指令，PC不变
				end
				else begin
					r_nextPC_32         = r_presentPC_32 + 32'h4;  //正常取下一条指令    
				end
			end
			else begin
					r_nextPC_32         = r_presentPC_t_32;        //暂停流水线，nextPC不变
			end
		end	
    end

    assign o_nextPC_32    = r_nextPC_32;
	assign o_presentPC_32 = r_presentPC_32;
endmodule
