//-----------------------------------------------
//    module name: cpu_core
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: CPU核模块，三级流水，包含中断异常处理
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module cpu_core(
    input  wire        clk,
    input  wire        rstn,   
	
	//Peripheral interrupt
    input  wire [ 4:0] i_intFlag_5,
    
    //icache         
    output wire [31:0] o_instAddr_32,
    input  wire [31:0] i_instData_32,
    
    //dcache
    output wire [ 3:0] o_dataWen_4,
    output wire [31:0] o_dataAddr_32,
    output wire [31:0] o_writeData_32,
    input  wire [31:0] i_readData_32,
    output wire        dbus_req_valid_o,
    input  wire        dbus_req_ready_i,
    input  wire        dbus_rsp_valid_i,
    output wire        dbus_rsp_ready_o
    
    );
    
    wire         w_secondFlowErr_1;
	wire  [ 4:0] w_exception_5;
    wire  [ 4:0] w_exception2_5;
    wire         w_idPipeHold_1;
	
    wire  [63:0] w_instAndPcIf_64;
    wire  [63:0] w_instAndPcId_64;
 
    wire  [31:0] w_newPC1_32;
    wire         w_EnNewPC1_1;
    wire  [31:0] w_newPC2_32;
    wire         w_EnNewPC2_1;
    wire  [31:0] w_instJump_32;
	
    wire  [31:0] w_CsrData_32;
    wire  [31:0] w_mtvecValue_32;    
    wire  [31:0] w_mepcValue_32;     
    wire  [31:0] w_mispValue_32;   
    wire  [31:0] w_mstatusValue_32; 
    
	wire [128:0] w_intFToCsr_129;
    wire [128:0] w_intSToCsr_129;
    wire  [44:0] w_idToCsr_45  ;
	
	wire  [47:0] w_idToGrf_48;
	wire  [63:0] w_grfToId_64;
			    
	wire  [ 9:0] w_ifToGrf_10;
	wire  [63:0] w_grfToIf_64;
	
    m_ifStage m_ifStage( 
        .rstn               (rstn					),
        .clk                (clk					),
        
		.o_exception_5      (w_exception_5			),    
		.o_jumpInst_32      (w_instJump_32			),
		.i_isIntPC_1        (w_EnNewPC1_1 | w_EnNewPC2_1), 
        .i_intPC_32         ((w_EnNewPC2_1) ? w_newPC2_32 : (w_EnNewPC1_1) ? w_newPC1_32 : 32'b0),
		
		.o_ifToGrf_10       (w_ifToGrf_10			),
		.i_grfToIf_64       (w_grfToIf_64			),
		
        .i_clearFirstn_1    (w_secondFlowErr_1		), //冲刷流水线，来自第二级，为跨流水信号
        .i_holdflag_1       (w_idPipeHold_1			),
        
        .o_instAndPcIf_64   (w_instAndPcIf_64		),           
                    
		.o_nextPC_32        (o_instAddr_32			),
        .i_inst_32          (i_instData_32			)
            
    );

    m_InterruptFirst m_InterruptFirst(
        .i_errFlag_5        (w_exception_5			),              
        .i_intFlag_5        (i_intFlag_5			),              

        .i_curPC_32         (w_instAndPcIf_64[63:32]),              
        .i_curInst_32       (w_instJump_32			),            

        .i_mispRegValue_32  (w_mispValue_32			),       
        .i_mtvecValue_32    (w_mtvecValue_32		),         
        .i_mstatusValue_32  (w_mstatusValue_32		),       
        .i_mepcValue_32     (w_mepcValue_32			),          

        .o_newPC_32         (w_newPC1_32			), //跨流水信号，需做仲裁      
        .o_EnNewPC_1        (w_EnNewPC1_1			),

        .o_intFToCsr_129	(w_intFToCsr_129	    )

    );
        
     m_if_id m_if_id(
        .rstn               (rstn					),            
        .clk                (clk					),      
    
        .i_instAndPc_64     (w_instAndPcIf_64		),   
        .i_holdflag_1       (w_idPipeHold_1			),
        .o_instAndPc_64     (w_instAndPcId_64		)
     );

    m_idStage m_idStage(
        .clk                (clk					),
        .rstn               (rstn					),
        
        .i_PcAndinstr_64    (w_instAndPcId_64		),          

        .o_idPipeHold_1     (w_idPipeHold_1			), //流水线控制信号 跨流水信号
        .o_exception_5      (w_exception2_5			),       

        .o_idToCsr_45		(w_idToCsr_45			), //寄存器组控制信号
		.i_csrData_32       (w_CsrData_32			),         
        .o_idToGrf_48       (w_idToGrf_48			),
		.i_grfToId_64       (w_grfToId_64			),

        .o_storeAddr_32     (o_dataAddr_32			), //Dcache控制信号 
        .o_storeWen_4       (o_dataWen_4			),         
        .o_wdata_32         (o_writeData_32			),           
		.i_dcacheData_32    (i_readData_32			),                         
        .dbus_req_valid_o   (dbus_req_valid_o		),
        .dbus_req_ready_i   (dbus_req_ready_i		), 
        .dbus_rsp_valid_i   (dbus_rsp_valid_i		), 
        .dbus_rsp_ready_o   (dbus_rsp_ready_o		) 
    );

    m_grf m_grf(
        .clk                (clk					),
        .rstn               (rstn					),
		.i_idToGrf_48       (w_idToGrf_48			),
		.o_grfToId_64       (w_grfToId_64			),
		.i_ifToGrf_10       (w_ifToGrf_10			),
		.o_grfToIf_64       (w_grfToIf_64			)
    );

    m_InterruptSecond m_InterruptSecond(
        .i_errFlag_5        (w_exception2_5		 	),              
							
        .i_instAndPcId_64   (w_instAndPcId_64  		),              

        .i_mstatusValue_32  (w_mstatusValue_32		),       
        .i_mtvecValue_32    (w_mtvecValue_32  		),         
							
        .o_newPC_32         (w_newPC2_32      		),
        .o_EnNewPC_1        (w_EnNewPC2_1     		),				
        .o_intSToCsr_129	(w_intSToCsr_129		 	),			
        .o_secondFlowErr_1  (w_secondFlowErr_1		)  //第二级异常与流水线信号冲刷合并
    ); 	                    
							
    m_csr m_csr(            
        .clk                (clk					),
        .rstn               (rstn					),
							
		.i_intFToCsr_129	(w_intFToCsr_129	    ),
        .i_intSToCsr_129	(w_intSToCsr_129		),
		.i_idToCsr_45		(w_idToCsr_45			),
        .o_csrValue_32      (w_CsrData_32			),
							
        .o_mtvecValue_32    (w_mtvecValue_32		),          
        .o_mispValue_32     (w_mispValue_32			),         
        .o_mepcValue_32     (w_mepcValue_32			),              
        .o_mstatusValue_32  (w_mstatusValue_32		)                  
    );
    
endmodule