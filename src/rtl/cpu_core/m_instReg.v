//-----------------------------------------------
//    module name: m_instReg
//    author: YuYang SunZhe WeiRen
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_instReg(
    //input interface
	input  wire        clk,     
	input  wire        rstn,            
    input  wire [31:0] i_inst_32,           
    input  wire [31:0] i_instaddr_32,             
    input              i_clear_1,
    //output interface
    output wire        o_16flag_1,          
    output wire [31:0] o_inst_32,
    output wire [31:0] o_instaddr_32
    );

    localparam Len32          = 3'b000;
    localparam Double16       = 3'b001;
    localparam Fst16SecH32    = 3'b010;
    localparam FstH32Sec16    = 3'b011;
    localparam Combine32      = 3'b100;
    localparam SixteenFlagOn  = 1'b1;
    localparam SixteenFlagOff = 1'b0;
	
    reg  [ 2:0] r_state_3;             
    reg  [31:0] r_instToStore_32;      
    wire [31:0] w_inst16_32;
	reg  [31:0] r_inst_32;
	reg         r_isCompress_1;
	reg  [31:0] r_instaddr_32;
	reg         r_16flag_1;
	
    m_CtoI CtoIL(
        .i_inst_16  (r_inst_32[15:0]),
        .o_inst_32  (w_inst16_32)
    );
	
	assign o_inst_32 = r_isCompress_1?w_inst16_32:r_inst_32;
	
    always @(posedge clk or negedge rstn)begin
        if(!rstn) begin
            r_inst_32           <=  `nop;   
			r_instaddr_32 		<= `RESETADDR;
            r_instToStore_32    <= i_inst_32;
            r_16flag_1          <= SixteenFlagOff;
            r_state_3           <= Len32;
            r_isCompress_1      <= 1'b0;
        end 
        else begin
            if(i_clear_1) begin
                r_inst_32           <= `nop;   
                r_instToStore_32    <= i_inst_32;
                r_instaddr_32 		<= o_instaddr_32;
                r_16flag_1    		<= SixteenFlagOff;
                r_state_3           <= Len32;
                r_isCompress_1      <= 1'b0;
            end else if (r_state_3 == Len32) begin  
                if (i_inst_32[1:0] == 2'b11) begin
                    r_inst_32           <= i_inst_32;   
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1     		<= SixteenFlagOff;
                    r_state_3           <= Len32;
                    r_isCompress_1      <= 1'b0;
                end 
                else if (i_inst_32[17:16] != 2'b11) begin
                    r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1     		<= SixteenFlagOn;
                    r_state_3           <= Double16;
                    r_isCompress_1      <= 1'b1;        
                end
                else begin
                    r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1     		<= SixteenFlagOff;
                    r_state_3           <= Fst16SecH32;
                    r_isCompress_1      <= 1'b1;
                end
            end else if (r_state_3 == Double16) begin
                if (r_16flag_1 == SixteenFlagOff) begin
                    if (i_inst_32[1:0] == 2'b11) begin
                        r_inst_32           <= i_inst_32;   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1     		<= SixteenFlagOff;
                        r_state_3           <= Len32;
                        r_isCompress_1      <= 1'b0;
                    end 
                    else if (i_inst_32[17:16] != 2'b11) begin
                        r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1     		<= SixteenFlagOn;
                        r_state_3           <= Double16;
                        r_isCompress_1      <= 1'b1;
                    end
                    else begin
                        r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1     		<= SixteenFlagOff;
                        r_state_3           <= Fst16SecH32;
                        r_isCompress_1      <= 1'b1;
                    end    
                end
                else if (r_16flag_1 == SixteenFlagOn) begin
                    r_inst_32           <= {{16{1'b0}},i_inst_32[31:16]};   
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1    	    <= SixteenFlagOff;
                    r_state_3           <= Double16; 
                    r_isCompress_1      <= 1'b1; 
                end   
            end
            else if (r_state_3 == Fst16SecH32) begin
                if (i_inst_32[17:16] != 2'b11) begin
                    r_inst_32           <= {i_inst_32[15:0],r_instToStore_32[31:16]};
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1          <= SixteenFlagOn;
                    r_state_3           <= FstH32Sec16;
                    r_isCompress_1      <= 1'b0;
                end
                else if (i_inst_32[17:16] == 2'b11) begin
                    r_inst_32           <= {i_inst_32[15:0],r_instToStore_32[31:16]};
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1          <= SixteenFlagOff;
                    r_state_3           <= Combine32;
                    r_isCompress_1      <= 1'b0;
                end
            end
            else if (r_state_3 == FstH32Sec16) begin
                if (r_16flag_1 == SixteenFlagOff) begin
                    if (i_inst_32[1:0] == 2'b11) begin
                        r_inst_32           <= i_inst_32;   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1          <= SixteenFlagOff;
                        r_state_3           <= Len32;
                        r_isCompress_1      <= 1'b0;
                    end 
                    else if (i_inst_32[17:16] != 2'b11) begin
                        r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1          <= SixteenFlagOn;
                        r_state_3           <= Double16;
                        r_isCompress_1      <=  1'b1;
                    end
                    else begin
                        r_inst_32           <= {{16{1'b0}},i_inst_32[15:0]};   
                        r_instToStore_32    <= i_inst_32;
                        r_instaddr_32 		<= i_instaddr_32;
                        r_16flag_1          <= SixteenFlagOff;
                        r_state_3           <= Fst16SecH32;
                        r_isCompress_1      <= 1'b1;
                    end    
                end
                else if (r_16flag_1 == SixteenFlagOn) begin
                    r_inst_32           <= {{16{1'b0}},i_inst_32[31:16]};   
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1          <= SixteenFlagOff;
                    r_state_3           <= FstH32Sec16; 
                    r_isCompress_1      <= 1'b1; 
                end    
            end
            else if (r_state_3 == Combine32) begin
                if (i_inst_32[17:16] != 2'b11) begin
                    r_inst_32           <= {i_inst_32[15:0],r_instToStore_32[31:16]};
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1          <= SixteenFlagOn;
                    r_state_3           <= FstH32Sec16;
                    r_isCompress_1      <= 1'b0;
                end
                else if (i_inst_32[17:16] == 2'b11) begin
                    r_inst_32           <= {i_inst_32[15:0],r_instToStore_32[31:16]};
                    r_instToStore_32    <= i_inst_32;
                    r_instaddr_32 		<= i_instaddr_32;
                    r_16flag_1          <= SixteenFlagOff;
                    r_state_3           <= Combine32;
                    r_isCompress_1      <= 1'b0;
                end  
            end
        end
    end
    assign o_instaddr_32 = r_instaddr_32;
    assign o_16flag_1    = r_16flag_1;
endmodule
