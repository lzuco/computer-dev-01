//-----------------------------------------------
//    module name: m_InterruptSecond
//    author: LuYihua
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module m_InterruptSecond(
    input wire   [ 4:0] i_errFlag_5,              //来自id_stage，异常编码
    input wire   [63:0] i_instAndPcId_64,  
    
    input wire   [31:0] i_mstatusValue_32,        //mstatus register value（MSTATUS寄存器为机器模式状态寄存器，该寄存器中的MIE域和MPIE域用于反应全局中断使能）
    input wire   [31:0] i_mtvecValue_32,          //mtevc register value（MTVEC寄存器为机器模式异常入口基地址寄存器，定义进入异常的程序 PC 地址）

    output wire  [31:0] o_newPC_32,               //进入中断/异常后，下一条需要处理的程序所在的地址
    output wire         o_EnNewPC_1,              //当前新PC的使能信号。用于控制当前输出的新PC是否有效

	output wire [128:0] o_intSToCsr_129,

    output wire         o_secondFlowErr_1         //err from the second flow（异常标志）
    );
    
	wire        w_EnNewCsr_1;                     //新CSR寄存器值的使能信号。用于控制当前输出的新CSR是否有效
	wire [31:0] w_mcauseValue_32;
	wire [31:0] w_mepcValue_32;
	wire [31:0] w_mtvalValue_32;
	wire [31:0] w_mstatusValue_32;
	wire [31:0] w_curPC_32;                       //当前正在执行的指令所在地址
	wire [31:0] w_curInst_32;                     //当前正在执行的指令
	assign {w_curPC_32,w_curInst_32} = i_instAndPcId_64;
	
	assign o_intSToCsr_129 = {	w_EnNewCsr_1,     
								w_mcauseValue_32,  
								w_mepcValue_32,    
								w_mtvalValue_32,   
								w_mstatusValue_32  };	

    wire        w_globalDisInterrupt_1;
    wire [4:0 ] w_errFlag_5;
    wire [31:0] w_mcauseErr_32;
    
    //全局中断使能位    
    assign w_globalDisInterrupt_1 = i_mstatusValue_32[3];  
    
    //INT_ERR_NONE     5'b00000
    assign o_secondFlowErr_1      = |i_errFlag_5;
    assign o_EnNewPC_1            = o_secondFlowErr_1 & ~w_globalDisInterrupt_1;
    assign w_EnNewCsr_1           = o_secondFlowErr_1 & ~w_globalDisInterrupt_1;

    
//-----{mcauseValue}begin 
    assign w_errFlag_5    = w_globalDisInterrupt_1 ? i_errFlag_5 : 5'b11111;
    assign w_mcauseErr_32 = (w_errFlag_5 == `ID_ILOGICAL_INSTR             ) ? 32'h0002 : //Value access error（数据访问错误）
                            (w_errFlag_5 == `WB_READ_MEM_MISALIGINMENT     ) ? 32'h0004 : //Environment call（环境调用）
                            (w_errFlag_5 == `WB_WRITE_MEM_MISALIGINMENT    ) ? 32'h0006 : //Read memory not aligned（写内存未对齐）
                            (w_errFlag_5 == `WB_READ_MEM_ADDR_NOT_FOUND    ) ? 32'h0005 : //Write memory not aligned（读内存未对齐）
                            (w_errFlag_5 == `WB_WRITE_MEM_ADDR_NOT_FOUND   ) ? 32'h0007 : //Read memory access address does not exist（读内存地址不存在）
                                                                               32'hFFFF ; //Write memory access address does not exist（写内存地址不存在）
    assign w_mcauseValue_32 = w_mcauseErr_32;
        
//-----{mcauseValue}end

//-----{newPc}begin 
    wire [1 :0] w_mode_2;
    wire [31:0] w_NewPcMode0_32;
    wire [31:0] w_NewPcMode1_32;
    wire [3:0]  w_cause_4;
    
    //mtevc寄存器的低2bit是MODE域
    assign w_mode_2         =   i_mtvecValue_32[1:0];

    assign w_NewPcMode0_32  =  {i_mtvecValue_32[31:2],2'b0};
    assign w_NewPcMode1_32  =  {i_mtvecValue_32[31:2],2'b0};      //第二级不存在中断这里可以删除              
    
    assign o_newPC_32       =    (w_mode_2 == `MODE_0)  ? w_NewPcMode0_32 :
                                 (w_mode_2 == `MODE_1)  ? w_NewPcMode1_32 :
                                 32'b0;
       
//-----{newPc}end
//-----{newCsr}begin 
    assign w_mepcValue_32  = w_curPC_32;
    assign w_mtvalValue_32 = w_curInst_32;
        
    assign w_mstatusValue_32[7] = w_mstatusValue_32[3]; //MPIE
    assign w_mstatusValue_32[3] = 1'b0;                    //shut down MIE
//-----{newCsr}end
    

endmodule
