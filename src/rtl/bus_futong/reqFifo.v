//-----------------------------------------------
//    module name: reqFifo 
//    author: Anping HE (heap@lzu.edu.cn)
//    modify author: 
//        Tong FU (fut21@lzu.edu.cn)
//        Xiabao WAN (wanbx21@lzu.edu.cn)
//        Mingshu CHEN (chenmsh18@lzu.edu.cn)
//          Kang Li Zhao    
//    version: 2st version (2021-11-21)
//    Last Modified: 2021-11-21
//    description: 
//        request fifo for local, west, east, north, south
//-----------------------------------------------

`timescale 1ns / 1ps

module reqFifo(
    inR, pmt, fire_2,inA ,idle, rst);

input inR, pmt, rst, idle;
output fire_2;
output inA;

 wire InANon;
 wire w_delayR;
 wire w_delayA;

pmtPosClick fifo0(
    .inR(inR), 
    .inA(InANon), 
    .outR(w_delayR),
    .outA(w_delayA), 
    .permit(idle),
    .fire(fire_2), 
    .rst(rst) );
/*posClick fifo1(
    .inR(w_delayR_3[0]), 
    .inA(w_delayA_3[0]), 
    .outR(w_delayR_3[1]), 
    .outA(w_delayA_3[1]), 
    .fire(fire_2[1]), 
    .rst(rst) );*/

/*bidirDelay2U delay0(
    .inR(w_delayR_3[0]), 
    .inA(w_delayA_3[0]), 
    .outR(w_delayR_3[1]), 
    .outA(w_delayA_3[1]) );*/
//click fifo2(
//    .inR(w_delayR_4[2]), 
//    .inA(w_delayA_4[2]), 
//    .outR(w_delayR_4[3]), 
//    .outA(w_delayA_4[3]),
//    .fire(fire_3[2]), 
//    .rst(rst) );
pmtSink trunkPmt(
    .trig(pmt),
    .inR(w_delayR), 
    .inA(w_delayA), 
    .rst(rst) );

assign inA = w_delayA;

endmodule


