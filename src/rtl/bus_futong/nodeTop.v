//-----------------------------------------------
//    module name: nodeTop 
//    author: Anping HE (heap@lzu.edu.cn)
//    version: 1st version (2021-11-13)
//    description: 
//        top module for mesh node
//-----------------------------------------------
`timescale 1ns / 1ps

module nodeTop(
localInR, localInA, i_localInMsg_51,
localOutR, localOutA, o_localMsg_51, 
westInR, westInA, i_westInMsg_51,
westOutR, westOutA, o_westMsg_51, 
eastInR, eastInA, i_eastInMsg_51,
eastOutR, eastOutA, o_eastMsg_51, 
northInR, northInA, i_northInMsg_51,
northOutR, northOutA, o_northMsg_51, 
southInR, southInA, i_southInMsg_51,
southOutR, southOutA, o_southMsg_51, 
rst
);

input rst;

//local ports
input localInR, localOutA;
output localInA, localOutR;
input [50:0] i_localInMsg_51;
output [50:0] o_localMsg_51;
//west ports
input westInR, westOutA;
output westInA, westOutR;
input [50:0] i_westInMsg_51;
output [50:0] o_westMsg_51;
//east ports
input eastInR, eastOutA;
output eastInA, eastOutR;
input [50:0] i_eastInMsg_51;
output [50:0] o_eastMsg_51;
//north ports
input northInR, northOutA;
output northInA, northOutR;
input [50:0] i_northInMsg_51;
output [50:0] o_northMsg_51;
//south ports
input southInR, southOutA;
output southInA, southOutR;
input [50:0] i_southInMsg_51;
output [50:0] o_southMsg_51;

//-----------------------------------------------
//inner wires and regs
 wire w_local2localR, w_local2westR, w_local2eastR, w_local2northR, w_local2southR;
 wire w_local2localA, w_local2westA, w_local2eastA, w_local2northA, w_local2southA;
 wire [50:0] w_local2localMsg_51, w_local2westMsg_51, w_local2eastMsg_51, w_local2northMsg_51, w_local2southMsg_51;


 wire w_west2localR, w_west2westR, w_west2eastR, w_west2northR, w_west2southR;
 wire w_west2localA, w_west2westA, w_west2eastA, w_west2northA, w_west2southA;
 wire [50:0] w_west2localMsg_51, w_west2westMsg_51, w_west2eastMsg_51, w_west2northMsg_51, w_west2southMsg_51;


 wire w_east2localR, w_east2westR, w_east2eastR, w_east2northR, w_east2southR;
 wire w_east2localA, w_east2westA, w_east2eastA, w_east2northA, w_east2southA;
 wire [50:0] w_east2localMsg_51, w_east2westMsg_51, w_east2eastMsg_51, w_east2northMsg_51, w_east2southMsg_51;


 wire w_north2localR, w_north2westR, w_north2eastR, w_north2northR, w_north2southR;
 wire w_north2localA, w_north2westA, w_north2eastA, w_north2northA, w_north2southA;
 wire [50:0] w_north2localMsg_51, w_north2westMsg_51, w_north2eastMsg_51, w_north2northMsg_51, w_north2southMsg_51;


 wire w_south2localR, w_south2westR, w_south2eastR, w_south2northR, w_south2southR;
 wire w_south2localA, w_south2westA, w_south2eastA, w_south2northA, w_south2southA;
 wire [50:0] w_south2localMsg_51, w_south2westMsg_51, w_south2eastMsg_51, w_south2northMsg_51, w_south2southMsg_51;
 wire localOutAtemp;




//-----------------------------------------------
//local part
routeMsg localRouteMsg(
    .inR(localInR),
    .inA(localInA),
    .i_msg_51(i_localInMsg_51),

    .localOutR(w_local2localR),
    .localOutA(w_local2localA),
    .o_localMsg_51(w_local2localMsg_51),

    .westOutR(w_local2westR),
    .westOutA(w_local2westA),
    .o_westMsg_51(w_local2westMsg_51),

    .eastOutR(w_local2eastR),
    .eastOutA(w_local2eastA),
    .o_eastMsg_51(w_local2eastMsg_51),

    .northOutR(w_local2northR),
    .northOutA(w_local2northA),
    .o_northMsg_51(w_local2northMsg_51),

    .southOutR(w_local2southR),
    .southOutA(w_local2southA),
    .o_southMsg_51(w_local2southMsg_51),

    .rst(rst)
);

assign localOutAtemp = localOutR;
arbMsg localArbMsg(
    .outA(localOutAtemp),
    .outR(localOutR),
    .o_msg_51(o_localMsg_51),

    .localInR(w_local2localR),
    .localInA(w_local2localA),
    .i_localMsg_51(w_local2localMsg_51),

    .westInR(w_west2localR),
    .westInA(w_west2localA),
    .i_westMsg_51(w_west2localMsg_51),

    .eastInR(w_east2localR),
    .eastInA(w_east2localA),
    .i_eastMsg_51(w_east2localMsg_51),

    .northInR(w_north2localR),
    .northInA(w_north2localA),
    .i_northMsg_51(w_north2localMsg_51),

    .southInR(w_south2localR),
    .southInA(w_south2localA),
    .i_southMsg_51(w_south2localMsg_51),

    .rst(rst)
);



//-----------------------------------------------
//west part
routeMsg westRouteMsg(
    .inR(westInR),
    .inA(westInA),
    .i_msg_51(i_westInMsg_51),

    .localOutR(w_west2localR),
    .localOutA(w_west2localA),
    .o_localMsg_51(w_west2localMsg_51),

    .westOutR(w_west2westR),
    .westOutA(w_west2westA),
    .o_westMsg_51(w_west2westMsg_51),

    .eastOutR(w_west2eastR),
    .eastOutA(w_west2eastA),
    .o_eastMsg_51(w_west2eastMsg_51),

    .northOutR(w_west2northR),
    .northOutA(w_west2northA),
    .o_northMsg_51(w_west2northMsg_51),

    .southOutR(w_west2southR),
    .southOutA(w_west2southA),
    .o_southMsg_51(w_west2southMsg_51),

    .rst(rst)
);

arbMsg westArbMsg(
    .outA(westOutA),
    .outR(westOutR),
    .o_msg_51(o_westMsg_51),

    .localInR(w_local2westR),
    .localInA(w_local2westA),
    .i_localMsg_51(w_local2westMsg_51),

    .westInR(w_west2westR),
    .westInA(w_west2westA),
    .i_westMsg_51(w_west2westMsg_51),

    .eastInR(w_east2westR),
    .eastInA(w_east2westA),
    .i_eastMsg_51(w_east2westMsg_51),

    .northInR(w_north2westR),
    .northInA(w_north2westA),
    .i_northMsg_51(w_north2westMsg_51),

    .southInR(w_south2westR),
    .southInA(w_south2westA),
    .i_southMsg_51(w_south2westMsg_51),

    .rst(rst)
);


//-----------------------------------------------
//east part
routeMsg eastRouteMsg(
    .inR(eastInR),
    .inA(eastInA),
    .i_msg_51(i_eastInMsg_51),

    .localOutR(w_east2localR),
    .localOutA(w_east2localA),
    .o_localMsg_51(w_east2localMsg_51),

    .westOutR(w_east2westR),
    .westOutA(w_east2westA),
    .o_westMsg_51(w_east2westMsg_51),

    .eastOutR(w_east2eastR),
    .eastOutA(w_east2eastA),
    .o_eastMsg_51(w_east2eastMsg_51),

    .northOutR(w_east2northR),
    .northOutA(w_east2northA),
    .o_northMsg_51(w_east2northMsg_51),

    .southOutR(w_east2southR),
    .southOutA(w_east2southA),
    .o_southMsg_51(w_east2southMsg_51),

    .rst(rst)
);

arbMsg eastArbMsg(
    .outA(eastOutA),
    .outR(eastOutR),
    .o_msg_51(o_eastMsg_51),

    .localInR(w_local2eastR),
    .localInA(w_local2eastA),
    .i_localMsg_51(w_local2eastMsg_51),

    .westInR(w_west2eastR),
    .westInA(w_west2eastA),
    .i_westMsg_51(w_west2eastMsg_51),

    .eastInR(w_east2eastR),
    .eastInA(w_east2eastA),
    .i_eastMsg_51(w_east2eastMsg_51),

    .northInR(w_north2eastR),
    .northInA(w_north2eastA),
    .i_northMsg_51(w_north2eastMsg_51),

    .southInR(w_south2eastR),
    .southInA(w_south2eastA),
    .i_southMsg_51(w_south2eastMsg_51),

    .rst(rst)
);



//-----------------------------------------------
//north part
routeMsg northRouteMsg(
    .inR(northInR),
    .inA(northInA),
    .i_msg_51(i_northInMsg_51),

    .localOutR(w_north2localR),
    .localOutA(w_north2localA),
    .o_localMsg_51(w_north2localMsg_51),

    .westOutR(w_north2westR),
    .westOutA(w_north2westA),
    .o_westMsg_51(w_north2westMsg_51),

    .eastOutR(w_north2eastR),
    .eastOutA(w_north2eastA),
    .o_eastMsg_51(w_north2eastMsg_51),

    .northOutR(w_north2northR),
    .northOutA(w_north2northA),
    .o_northMsg_51(w_north2northMsg_51),

    .southOutR(w_north2southR),
    .southOutA(w_north2southA),
    .o_southMsg_51(w_north2southMsg_51),

    .rst(rst)
);

arbMsg northArbMsg(
    .outA(northOutA),
    .outR(northOutR),
    .o_msg_51(o_northMsg_51),

    .localInR(w_local2northR),
    .localInA(w_local2northA),
    .i_localMsg_51(w_local2northMsg_51),

    .westInR(w_west2northR),
    .westInA(w_west2northA),
    .i_westMsg_51(w_west2northMsg_51),

    .eastInR(w_east2northR),
    .eastInA(w_east2northA),
    .i_eastMsg_51(w_east2northMsg_51),

    .northInR(w_north2northR),
    .northInA(w_north2northA),
    .i_northMsg_51(w_north2northMsg_51),

    .southInR(w_south2northR),
    .southInA(w_south2northA),
    .i_southMsg_51(w_south2northMsg_51),

    .rst(rst)
);



//-----------------------------------------------
//south part
routeMsg southRouteMsg(
    .inR(southInR),
    .inA(southInA),
    .i_msg_51(i_southInMsg_51),

    .localOutR(w_south2localR),
    .localOutA(w_south2localA),
    .o_localMsg_51(w_south2localMsg_51),

    .westOutR(w_south2westR),
    .westOutA(w_south2westA),
    .o_westMsg_51(w_south2westMsg_51),

    .eastOutR(w_south2eastR),
    .eastOutA(w_south2eastA),
    .o_eastMsg_51(w_south2eastMsg_51),

    .northOutR(w_south2northR),
    .northOutA(w_south2northA),
    .o_northMsg_51(w_south2northMsg_51),

    .southOutR(w_south2southR),
    .southOutA(w_south2southA),
    .o_southMsg_51(w_south2southMsg_51),

    .rst(rst)
);

arbMsg southArbMsg(
    .outA(southOutA),
    .outR(southOutR),
    .o_msg_51(o_southMsg_51),

    .localInR(w_local2southR),
    .localInA(w_local2southA),
    .i_localMsg_51(w_local2southMsg_51),

    .westInR(w_west2southR),
    .westInA(w_west2southA),
    .i_westMsg_51(w_west2southMsg_51),

    .eastInR(w_east2southR), 
    .eastInA(w_east2southA),
    .i_eastMsg_51(w_east2southMsg_51),

    .northInR(w_north2southR),
    .northInA(w_north2southA),
    .i_northMsg_51(w_north2southMsg_51),

    .southInR(w_south2southR),
    .southInA(w_south2southA),
    .i_southMsg_51(w_south2southMsg_51),

    .rst(rst)
);


endmodule
