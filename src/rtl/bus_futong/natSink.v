//-----------------------------------------------
//    module name: pmtSink_umc40
//    author: Anping HE (heap@lzu.edu.cn)
//    modify author: Kang Li Zhao
//    version: 1st version (2021-11-12)
//    description: 
//        permit sink  
//        tech: umc40nm 
//-----------------------------------------------

`timescale 1ns / 1ps

module natSink(inR, inA);

input inR;

output inA;

assign inA=inR;

endmodule

