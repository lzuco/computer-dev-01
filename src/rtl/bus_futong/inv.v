//-----------------------------------------------
//	module name: pmtSink_umc40
//	author: Anping HE (heap@lzu.edu.cn)
//	modify author: Kang Li Zhao
//	version: 1st version (2021-11-12)
//	description: 
//		permit sink  
//		tech: umc40nm 
//-----------------------------------------------

`timescale 1ns / 1ps

module inv(inR, outR);

input inR;

output outR;

wire inR0;
//INUHDV1 inv0 ( .I(inR), .ZN(inR0) );
//INUHDV1 inv1 ( .I(inR0), .ZN(outR) );
INVM0HM inv0 ( .A(inR), .Z(inR0) );
INVM0HM inv1 ( .A(inR0), .Z(outR) );

endmodule

