//-----------------------------------------------
//    module name: routeMsgEW
//    author: Anping HE (heap@lzu.edu.cn)
//    version: 1st version (2021-11-07)
//    description: 
//        corrdination = corrdination -1;
//        trim a message that will pass to next dir
//-----------------------------------------------

`timescale 1ns / 1ps

module routeMsgEW(i_msg_51, i_coord_4, o_msg_51, o_msgVld_1);

input [50:0] i_msg_51;
input [3:0] i_coord_4;

output [50:0] o_msg_51;
output o_msgVld_1;

 wire [3:0] distance;

//calculate the distance
 subtr4b sub(
    .a(i_coord_4),
    .b(4'b0001),
    .differ(distance));

assign o_msgVld_1 = i_coord_4[3]|i_coord_4[2]|i_coord_4[1]|i_coord_4[0];

assign    o_msg_51 = (o_msgVld_1==1) ? 
        {i_msg_51[31:9], distance, i_msg_51[4:0]} : 51'b0;


endmodule
