//-----------------------------------------------
//	module name: click_umc40
//	author: Anping HE (heap@lzu.edu.cn)
//      modify author: Kang Li Zhao
//	version: 1st version (2021-11-05)
//	description: 
//		standard click  
//		tech: umc40nm
//-----------------------------------------------

`timescale 1ns / 1ps

module negClick( inR, inA, outR, outA, fire, rst );

input inR, outA, rst;

output inA, outR, fire;

wire inAR, outAR, notR, rstNeg,inRNeg;
wire Rtemp0, Rtemp1, Rtemp2, Rtemp3, Rtemp4;
INVM0HM invR ( .A(inR), .Z(inRNeg) );

XOR2M0HM neqIn ( .A(inRNeg), .B(inA), .Z(inAR) );

XNR2M0HM eqOut ( .A(outA), .B(inA), .Z(outAR) );

AN2M0HM andFire ( .A(inAR), .B(outAR), .Z(fire) );
//AND2_X1M_A9TRULP_C40_W3 Fire ( .A(fire0), .B(permit), .Y(fire) );
INVM0HM invTmp ( .A(inA), .Z(notR) );

//INV_X1M_A9TRULP_C40_W3 inv6 ( .A(rst), .Y(rstNeg) );
//DRNQUHDV1 ffState ( .D(notR), .CK(fire), .RDN(rst), .Q(Rtemp0) );
DFQRM2HM ffState ( .D(notR), .CK(fire), .RB(rst), .Q(Rtemp0) );

DEL1M4HM delay7 ( .A(Rtemp0), .Z(Rtemp1) );
DEL1M4HM delay8 ( .A(Rtemp1), .Z(Rtemp2) );
DEL1M4HM delay9 ( .A(Rtemp2), .Z(Rtemp3) );
DEL1M4HM delay10 ( .A(Rtemp3), .Z(Rtemp4) );
DEL1M4HM delay41 ( .A(Rtemp4), .Z(inA) );

BUFM2HM U1 ( .A(inA), .Z(outR) );
endmodule

