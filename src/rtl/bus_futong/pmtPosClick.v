//-----------------------------------------------
//	module name: pmtClick
//	author: Fu Tong, Baoxia WAN 
//      modify author: Kang Li Zhao
//	version: 1st version (2021-11-20)
//	description: 
//		standard click with permit 
//		while permit is 1 , pmtClick is the same with click,
//      while permit is 0 , pmtClick is paused
//-----------------------------------------------
`timescale 1ns / 1ps

module pmtPosClick(inR, inA, outR, outA, permit, fire, rst);

input inR, outA, rst,permit;

output inA, outR, fire;

wire inAR, outAR, notR, rstNeg;
wire Rtemp0, Rtemp1, Rtemp2, Rtemp3, Rtemp4;
wire fire0;
XOR2M0HM neqIn ( .A(inR), .B(inA), .Z(inAR) );

XNR2M0HM eqOut ( .A(outA), .B(inA), .Z(outAR) );

AN2M0HM andFire ( .A(inAR), .B(outAR), .Z(fire0) );
AN2M0HM fire_pmt ( .A(fire0), .B(permit), .Z(fire) );
//AND2_X1M_A9TRULP_C40_W3 Fire ( .A(fire0), .B(permit), .Y(fire) );
INVM0HM invTmp ( .A(inA), .Z(notR) );

//INV_X1M_A9TRULP_C40_W3 inv6 ( .A(rst), .Y(rstNeg) );
//DRNQUHDV1 ffState ( .D(notR), .CK(fire), .RDN(rst), .Q(Rtemp0) );
DFQRM2HM ffState ( .D(notR), .CK(fire), .RB(rst), .Q(Rtemp0) );

DEL1M4HM delay7 ( .A(Rtemp0), .Z(Rtemp1) );
DEL1M4HM delay8 ( .A(Rtemp1), .Z(Rtemp2) );
DEL1M4HM delay9 ( .A(Rtemp2), .Z(Rtemp3) );
DEL1M4HM delay10 ( .A(Rtemp3), .Z(Rtemp4) );
DEL1M4HM delay41 ( .A(Rtemp4), .Z(inA) );

BUFM2HM U1 ( .A(inA), .Z(outR) );
endmodule


