//-----------------------------------------------
//	module name: pmtSink_umc40
//	author: Anping HE (heap@lzu.edu.cn)
//      modify author: Kang Li Zhao
//	version: 1st version (2021-11-12)
//	description: 
//		permit sink  
//		tech: umc40nm 
//-----------------------------------------------

`timescale 1ns / 1ps

module pmtSink(inR, inA, trig, rst);

input inR, trig, rst;
wire rstNeg;
output inA;
//INUHDV1 inv6 ( .I(rst), .ZN(rstNeg) );
//DRNQUHDV1 ffState ( .D(inR), .CK(trig), .RDN(rst), .Q(inA) );
DFQRM2HM ffState ( .D(inR), .CK(trig), .RB(rst), .Q(inA) );
endmodule

