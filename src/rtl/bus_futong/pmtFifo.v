//-----------------------------------------------
//    module name: reqFifo 
//    author: Anping HE (heap@lzu.edu.cn)
//    modify author: 
//        Tong FU (fut21@lzu.edu.cn)
//        Xiabao WAN (wanbx21@lzu.edu.cn)
//        Mingshu CHEN (chenmsh18@lzu.edu.cn)
//          Kang Li Zhao    
//    version: 2st version (2021-11-21)
//    Last Modified: 2021-11-21
//    description: 
//        request fifo for local, west, east, north, south
//-----------------------------------------------

`timescale 1ns / 1ps

module pmtFifo(
    inR, pmt, rst);

input inR,rst;
output pmt;
wire outR,outA,fire,inA;
negClick fifo0(
    .inR(inR), 
    .inA(inA), 
    .outR(outR), 
    .outA(outA), 
    .fire(fire), 
    .rst(rst) 
    ); 
natSink nat(
    .inR(outR),
    .inA(outA)
);
assign pmt=inR&fire;

endmodule


