//-----------------------------------------------
//    module name: bidirDelay8U
//    author: Anping HE (heap@lzu.edu.cn)
//    version: 1st version (2021-11-05)
//    description:
//        1. bidirected delay, e.g., equal delays for both req from left to right and ack from right to left
//        2. 8U for 8 bidirected delay units
//------------------------------------
`timescale 1ns / 1ps

module bidirDelay8U(inR, inA, outR, outA);

input   inR, outA;

output  outR, inA; 

wire w_req_0, w_ack_0;
wire w_req_1, w_ack_1, w_req_2, w_ack_2;
wire w_req_3, w_ack_3, w_req_4, w_ack_4, w_req_5, w_ack_5, w_req_6, w_ack_6;

bidirDelay1U delay0(inR, inA, w_req_0, w_ack_0);
bidirDelay1U delay1(w_req_0, w_ack_0, w_req_1, w_ack_1);
bidirDelay1U delay2(w_req_1, w_ack_1, w_req_2, w_ack_2);
bidirDelay1U delay3(w_req_2, w_ack_2, w_req_3, w_ack_3);
bidirDelay1U delay4(w_req_3, w_ack_3, w_req_4, w_ack_4);
bidirDelay1U delay5(w_req_4, w_ack_4, w_req_5, w_ack_5);
bidirDelay1U delay6(w_req_5, w_ack_5, w_req_6, w_ack_6);
bidirDelay1U delay7(w_req_6, w_ack_6, outR, outA);

endmodule
