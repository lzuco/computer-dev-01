//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module soc_top(
`ifdef POST_SYNTHSIS_SIMULATION
    input  wire        clk_in_p    ,
    input  wire        clk_in_n    ,
`else
    input  wire        clk_pad  ,
`endif 
    input  wire        rstn_pad        ,
    input  wire        init_enable_pad ,    
    input  wire        rx_pin_pad      ,    
    output wire        tx_pin_pad      ,
    
    input  wire        spi_miso_pad,    // SPI MISO引脚
    output wire        spi_mosi_pad,    // SPI MOSI引脚
    output wire        spi_ss_pad,      // SPI SS引脚
    output wire        spi_clk_pad,     // SPI CLK引脚
    
    inout  wire [`GPIO_NUM-1:0]  io_pin_pad             //GPIO 
    );

    //以下部分为复用 rx 和 tx 端口 , 节约引脚资源
    wire        init_rx ;       wire        uart_rx ;   
    wire        init_tx ;       wire        uart_tx ;   

	wire [31:0] gpio_ctrl_o;
	wire [31:0] gpio_data_o;
	wire [31:0] io_pin;
	wire        clk;
	wire        rstn;
	wire        init_enable;
	wire        rx_pin;
	wire        tx_pin;
    wire        spi_miso;    
    wire        spi_mosi;    
    wire        spi_ss;      
    wire        spi_clk;   
    
    localparam input_DO   = 1'b0; localparam output_DI   = 1'bz;   
    localparam input_OE   = 1'b0; localparam output_OE   = 1'b1; 
    localparam input_IDDQ = 1'b0; localparam output_IDDQ = 1'b0; localparam inout_IDDQ = 1'b0;
    localparam input_PD   = 1'b1; localparam output_PD   = 1'b0; localparam inout_PD   = 1'b1;
    localparam input_PU   = 1'b1; localparam output_PU   = 1'b0; localparam inout_PU   = 1'b1;
    localparam input_SMT  = 1'b1; localparam output_SMT  = 1'b0; localparam inout_SMT  = 1'b1;
    localparam input_SR   = 1'b0; localparam output_SR   = 1'b0; localparam inout_SR   = 1'b0;
    localparam input_PIN2 = 1'b0; localparam output_PIN2 = 1'b1; localparam inout_PIN2 = 1'b1;
    localparam input_PIN1 = 1'b0; localparam output_PIN1 = 1'b1; localparam inout_PIN1 = 1'b1;

`ifdef DC_ENV 
//-----INPUT IO PAD
	IUMA input_clk(
				.PAD   (clk_pad),.DI (clk),
				.OE    (input_OE  ),.IDDQ  (input_IDDQ),.PD    (input_PD  ),
				.PU    (input_PU  ),.SMT   (input_SMT ),.DO    (input_DO  ),
				.SR    (input_SR  ),.PIN2  (input_PIN2),.PIN1  (input_PIN1),.VSS(1'b0),.VDD(1'b0)
		);
	IUMA input_rstn(
				.PAD   (rstn_pad),.DI    (rstn),
				.OE    (input_OE  ),.IDDQ  (input_IDDQ),.PD    (input_PD  ),
				.PU    (input_PU  ),.SMT   (input_SMT ),.DO    (input_DO  ),
				.SR    (input_SR  ),.PIN2  (input_PIN2),.PIN1  (input_PIN1),.VSS(1'b0),.VDD(1'b0)
		);
	IUMA input_init_enable(
				.PAD   (init_enable_pad),.DI (init_enable),
				.OE    (input_OE  ),.IDDQ  (input_IDDQ),.PD    (input_PD  ),
				.PU    (input_PU  ),.SMT   (input_SMT ),.DO    (input_DO  ),
				.SR    (input_SR  ),.PIN2  (input_PIN2),.PIN1  (input_PIN1),.VSS(1'b0),.VDD(1'b0)
		);
	IUMA input_rx_pin(
				.PAD   (rx_pin_pad),.DI    (rx_pin),
				.OE    (input_OE  ),.IDDQ  (input_IDDQ),.PD    (input_PD  ),
				.PU    (input_PU  ),.SMT   (input_SMT ),.DO    (input_DO  ),
				.SR    (input_SR  ),.PIN2  (input_PIN2),.PIN1  (input_PIN1),.VSS(1'b0),.VDD(1'b0)
		);	
	IUMA input_spi_miso(
				.PAD   (spi_miso_pad),.DI  (spi_miso),
				.OE    (input_OE  ),.IDDQ  (input_IDDQ),.PD    (input_PD  ),
				.PU    (input_PU  ),.SMT   (input_SMT ),.DO    (input_DO  ),
				.SR    (input_SR  ),.PIN2  (input_PIN2),.PIN1  (input_PIN1),.VSS(1'b0),.VDD(1'b0)
		);		
//-----OUTPUT IO PAD
	IUMA output_tx_pin(
				.PAD   (tx_pin_pad), .DO    (tx_pin),
				.OE    (output_OE  ),.IDDQ  (output_IDDQ),.PD    (output_PD  ),
				.PU    (output_PU  ),.SMT   (output_SMT ),.DI    (output_DI  ),
				.SR    (output_SR  ),.PIN2  (output_PIN2),.PIN1  (output_PIN1),.VSS(1'b0),.VDD(1'b0)
		);
	IUMA output_spi_mosi(
				.PAD   (spi_mosi_pad), .DO    (spi_mosi),
				.OE    (output_OE  ),.IDDQ  (output_IDDQ),.PD    (output_PD  ),
				.PU    (output_PU  ),.SMT   (output_SMT ),.DI    (output_DI  ),
				.SR    (output_SR  ),.PIN2  (output_PIN2),.PIN1  (output_PIN1),.VSS(1'b0),.VDD(1'b0)
		);		
	IUMA output_spi_ss(
				.PAD   (spi_ss_pad), .DO    (spi_ss),
				.OE    (output_OE  ),.IDDQ  (output_IDDQ),.PD    (output_PD  ),
				.PU    (output_PU  ),.SMT   (output_SMT ),.DI    (output_DI  ),
				.SR    (output_SR  ),.PIN2  (output_PIN2),.PIN1  (output_PIN1),.VSS(1'b0),.VDD(1'b0)
		);		
	IUMA output_spi_clk(
				.PAD   (spi_clk_pad), .DO    (spi_clk),
				.OE    (output_OE  ),.IDDQ  (output_IDDQ),.PD    (output_PD  ),
				.PU    (output_PU  ),.SMT   (output_SMT ),.DI    (output_DI  ),
				.SR    (output_SR  ),.PIN2  (output_PIN2),.PIN1  (output_PIN1),.VSS(1'b0),.VDD(1'b0)
		);

    genvar i; 
    generate
       for(i=0;i<`GPIO_NUM;i=i+1)
       begin: gpio
	       IUMA inout_gpio_i(
				.PAD   (io_pin_pad[i]),  .OE    (gpio_ctrl_o[i]),
				.DO    (gpio_data_o[i]), .DI    (io_pin[i]), 

				.IDDQ  (inout_IDDQ),
				.PD    (inout_PD  ),.PU    (inout_PU  ),.SMT   (inout_SMT ),
				.SR    (inout_SR  ),.PIN2  (inout_PIN2),.PIN1  (inout_PIN1),.VSS(1'b0),.VDD(1'b0)
		  );
       end
    endgenerate

`else 
    //input
    //`ifdef POST_SYNTHSIS_SIMULATION
	assign clk		    = clk_pad;
    //`endif
	assign rstn		    = rstn_pad;
	assign init_enable	= init_enable_pad;
	assign rx_pin		= rx_pin_pad;
	assign spi_miso     = spi_miso_pad   ;
	//output
	assign tx_pin_pad	  = tx_pin;
    assign spi_mosi_pad   = spi_mosi  ;
    assign spi_ss_pad     = spi_ss    ;
    assign spi_clk_pad    = spi_clk   ;
    
   genvar i; 
    generate
       for(i=0;i<`GPIO_NUM;i=i+1)
       begin: io_ctrlpin
            assign io_pin_pad[i] = gpio_ctrl_o[i] ? gpio_data_o[i] : 1'bz;
       end
    endgenerate
    assign io_pin  = io_pin_pad;
`endif

`ifdef POST_SYNTHSIS_SIMULATION
        clk_wiz_0 clkgen(
        
            .clk_in1_n  (clk_in_n   ),
            .clk_in1_p  (clk_in_p   ),
            .resetn     (rstn       ),
            .clk_out1   (clk        )
            
        );
`endif


    assign tx_pin  = init_enable ? init_tx : uart_tx;
    assign init_rx = init_enable ? rx_pin  : 1'b0   ;
    assign uart_rx = init_enable ? 1'b0    : rx_pin ;

    wire        Local_in_R;
    wire        Local_out_R;
    wire [50:0] Local_in; 
    wire [50:0] Local_out;
    wire        Local_click_out_A; 
    wire        Local_click_in_A;
    
    wire        West_in_R;                  wire        East_in_R;
    wire        West_out_R;                 wire        East_out_R;
    wire [50:0] West_in;                    wire [50:0] East_in;
    wire [50:0] West_out;                   wire [50:0] East_out;
    wire        West_click_out_A;           wire        East_click_out_A;
    wire        West_click_in_A;            wire        East_click_in_A;
    
    wire        North_in_R;                 wire        South_in_R;
    wire        North_out_R;                wire        South_out_R;
    wire [50:0] North_in;                   wire [50:0] South_in;
    wire [50:0] North_out;                  wire [50:0] South_out;
    wire        North_click_out_A;          wire        South_click_out_A;
    wire        North_click_in_A;           wire        South_click_in_A;

`ifdef BUS_BY_FUTONG
    nodeTop bus(
        .localInR(Local_in_R),.localOutR(Local_out_R),.i_localInMsg_51(Local_in),.o_localMsg_51(Local_out),.localInA(Local_click_in_A),.localOutA(Local_click_out_A),
        .westInR(West_in_R),.westOutR(West_out_R),.i_westInMsg_51(West_in),.o_westMsg_51(West_out),.westInA(West_click_in_A),.westOutA(West_click_out_A),
        .eastInR(East_in_R),.eastOutR(East_out_R),.i_eastInMsg_51(East_in),.o_eastMsg_51(East_out),.eastInA(East_click_in_A),.eastOutA(East_click_out_A),
        .northInR(North_in_R),.northOutR(North_out_R),.i_northInMsg_51(North_in),.o_northMsg_51(North_out),.northInA(North_click_out_A),.northOutA(North_click_in_A),
        .southInR(South_in_R),.southOutR(South_out_R),.i_southInMsg_51(South_in),.o_southMsg_51(South_out),.southInA(South_click_out_A),.southOutA(South_click_in_A),
        .rst(rstn)
    );
`endif

`ifdef BUS_BY_WEIJIE
    center_node_top_code bus(
        .Local_in_R(Local_in_R),.Local_out_R(Local_out_R),    .Local_in(Local_in),    .Local_out(Local_out),    .Local_click_in_A(Local_click_in_A),    .Local_click_out_A(Local_click_out_A),
        .West_in_R(West_in_R),  .West_out_R(West_out_R),.West_in(West_in),.West_out(West_out),.West_click_in_A(West_click_in_A),.West_click_out_A(West_click_out_A),
        .East_in_R(East_in_R),  .East_out_R(East_out_R),.East_in(East_in),.East_out(East_out),.East_click_in_A(East_click_in_A),.East_click_out_A(East_click_out_A),
        .South_in_R(South_in_R),.South_out_R(South_out_R),.South_in(South_in),.South_out(South_out),.South_click_out_A(South_click_out_A),.South_click_in_A(South_click_in_A),
        .North_in_R(North_in_R),.North_out_R(North_out_R),.North_in(North_in),.North_out(North_out),.North_click_out_A(North_click_out_A),.North_click_in_A(North_click_in_A),
        .rstn(rstn)
    );
`endif

    wire int_timer;
    
    cpu_slot cpu_slot   (
        .clk           (clk              ),
        .rstn          (rstn             ),
        
        .init_enable   (init_enable      ),
        .init_rx       (init_rx          ),
        .init_tx       (init_tx          ),
        .int_sig_cpu   ({4'b0,int_timer} ),
        
        .in_R_to       (Local_in_R        ),
        .out_R_from    (Local_out_R       ),
        .data_to       (Local_in          ),
        .data_from     (Local_out         ),
        .click_out_to  (Local_click_out_A ),
        .click_in_from (Local_click_in_A  )
    );                                    
                                          
    uart_slot uart_slot (                 
        .clk           (clk               ),
        .rstn          (rstn              ),
        .tx_pin        (uart_tx           ),
        .rx_pin        (uart_rx           ),

        .in_R_to       (West_in_R         ),
        .out_R_from    (West_out_R        ),
        .data_to       (West_in           ),
        .data_from     (West_out          ),
        .click_out_to  (West_click_out_A  ),
        .click_in_from (West_click_in_A   )
    );                                      
                                          
    gpio_slot gpio_slot (                  
        .clk            (clk               ),
        .rstn           (rstn              ),
        .io_pin_i       (io_pin            ),
	    .gpio_ctrl_o	(gpio_ctrl_o	  ),
	    .gpio_data_o	(gpio_data_o	  ),

        .in_R_to       (South_in_R        ),
        .out_R_from    (South_out_R       ),
        .data_to       (South_in          ),
        .data_from     (South_out         ),
        .click_out_to  (South_click_out_A ),
        .click_in_from (South_click_in_A  )
    );                                      
                                          
   timer_slot timer_slot(                  
        .clk           (clk               ),
        .rstn          (rstn              ),
        .int_sig_o     (int_timer         ),
                                         
        .in_R_to       (North_in_R        ),
        .out_R_from    (North_out_R       ),
        .data_to       (North_in          ),
        .data_from     (North_out         ),
        .click_out_to  (North_click_out_A ),
        .click_in_from (North_click_in_A  )
    );
                                              
   spi_slot spi_slot(                  
        .clk           (clk               ),
        .rstn          (rstn              ),
        .spi_mosi      (spi_mosi          ),
        .spi_miso      (spi_miso          ),
        .spi_ss        (spi_ss            ),
        .spi_clk       (spi_clk           ),
                                         
        .in_R_to       (East_in_R        ),
        .out_R_from    (East_out_R       ),
        .data_to       (East_in          ),
        .data_from     (East_out         ),
        .click_out_to  (East_click_out_A ),
        .click_in_from (East_click_in_A  )
    );

endmodule