//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
`include "riscv.h"
module cpu_core_top(
    input [4:0]  i_intFlag_5,
    input           rstn,            
    input           clk   
    );
    
    wire [31:0] w_instAddr_32;
    wire [31:0] w_icacheInst_32;
    wire [3:0]  w_storeWen_4;
    wire [31:0] w_wDcacheData_32;
    wire [31:0] w_rDcacheData_32;
    wire [31:0] w_storeAddr_32;
    
    icache icache(
        .rstn                 (rstn),
        .clk                  (clk),
        .i_address_32              (w_instAddr_32), 
        .o_dataR_32                (w_icacheInst_32)
    );
    
    
    cpu_core cpu_core(
        .clk                (clk),
        .rstn               (rstn),   
        .i_intFlag_5        (i_intFlag_5),

        .o_instAddr_32      (w_instAddr_32),
        .i_instData_32      (w_icacheInst_32),

        .o_dataWen_4        (w_storeWen_4),
        .o_dataAddr_32      (w_storeAddr_32),
        .o_writeData_32     (w_wDcacheData_32),
        .i_readData_32      (w_rDcacheData_32),
        
        .dbus_req_ready_i    (1'b1),
        .dbus_rsp_valid_i    (1'b1)
    );
    
    dcache dcache(
        .rstn                   (rstn),
        .clk                    (clk),
        .i_wen_4                (w_storeWen_4),          
        .i_address_32           (w_storeAddr_32), 
        .i_dataW_32             (w_wDcacheData_32),  
        .o_dataR_32             (w_rDcacheData_32)
    );
    
    
endmodule
