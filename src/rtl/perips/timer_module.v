//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module timer_module(

    input         clk,
    input         rstn,
    input         we_i,
    input  [31:0] addr_i,
    input  [31:0] data_i,
    output [31:0] data_o,

    output        int_sig_o

    );

    // 寄存器(偏移)地址
    localparam REG_CTRL  = 4'h0;
    localparam REG_COUNT = 4'h4;
    localparam REG_VALUE = 4'h8;

    // 定时器控制寄存器，可读可写
    // bit[0]: 定时器使能
    // bit[1]: 定时器中断使能
    // bit[2]: 定时器中断pending标志，写1清零
    reg[31:0] timer_ctrl;

    // 定时器当前计数值寄存器, 只读
    reg[31:0] timer_count;

    // 定时器溢出值寄存器，当定时器计数值达到该值时产生pending，可读可写
    reg[31:0] timer_value;

    wire wen = we_i ;
    wire ren = (~we_i);
    wire timer_en = (timer_ctrl[0] == 1'b1);
    wire timer_int_en = (timer_ctrl[1] == 1'b1);
    wire timer_expired = (timer_count >= timer_value);
    wire write_reg_ctrl_en = wen & (addr_i[3:0] == REG_CTRL);
    wire write_reg_value_en = wen & (addr_i[3:0] == REG_VALUE);

    // 计数
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            timer_count <= 32'h0;
        end else begin
            if (timer_en) begin
                if (timer_expired) begin
                    timer_count <= 32'h0;
                end else begin
                    timer_count <= timer_count + 1'b1;
                end
            end else begin
                timer_count <= 32'h0;
            end
        end
    end

    reg int_sig_r;
    // 产生中断信号
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            int_sig_r <= 1'b0;
        end else begin
            if (write_reg_ctrl_en & (data_i[2] == 1'b1)) begin
                int_sig_r <= 1'b0;
            end else if (timer_int_en & timer_en & timer_expired) begin
                int_sig_r <= 1'b1;
            end
        end
    end

    assign int_sig_o = int_sig_r;

    // 写timer_ctrl
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            timer_ctrl <= 32'h0;
        end else begin
            if (write_reg_ctrl_en) begin
                timer_ctrl <= {24'b0,data_i[7:3], timer_ctrl[2] & (~data_i[2]), data_i[1:0]};
            end else begin
                if (timer_expired) begin
                    timer_ctrl[0] <= 1'b0;
                end
            end
        end
    end

    // 写timer_value
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            timer_value <= 32'h0;
        end else begin
            if (write_reg_value_en) begin
                 timer_value <= data_i;
            end
        end
    end

    assign data_o = (addr_i[3:0] == REG_VALUE) ? timer_value:
                     (addr_i[3:0] == REG_CTRL) ? timer_ctrl:
                    (addr_i[3:0] == REG_COUNT) ? timer_count:32'b0;
endmodule