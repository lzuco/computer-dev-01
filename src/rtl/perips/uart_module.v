//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module uart_module(

    input         clk,
    input         rstn,
           
    input         we_i,
    input  [31:0] addr_i,
    input  [31:0] data_i,
    output [31:0] data_o,
    
    output        tx_pin,
    input         rx_pin

    );

    wire [7:0] rx_data;
    reg rx_over;

    // 寄存器(偏移)地址
    localparam UART_CTRL    = 8'h0;
    localparam UART_STATUS  = 8'h4;
    localparam UART_TXDATA  = 8'h8;
    localparam UART_RXDATA  = 8'hc;

    // UART控制寄存器，可读可写
    // bit[0]: UART TX使能, 1: enable, 0: disable
    // bit[1]: UART RX使能, 1: enable, 0: disable
    reg[31:0] uart_ctrl;

    // UART状态寄存器
    // 只读，bit[0]: TX空闲状态标志, 1: busy, 0: idle
    // 可读可写，bit[1]: RX接收完成标志, 1: over, 0: receiving
    reg[31:0] uart_status;

    // UART发送数据寄存器，可读可写
    reg[31:0] uart_tx;

    // UART接收数据寄存器，只读
    reg[31:0] uart_rx;
	
    wire tx_data_ready;
    wire rx_data_valid;
    wire wen = we_i ;
    wire write_reg_ctrl_en = wen & (addr_i[7:0] == UART_CTRL);
    wire write_reg_status_en = wen & (addr_i[7:0] == UART_STATUS);
    wire write_reg_txdata_en = wen & (addr_i[7:0] == UART_TXDATA);
    wire tx_start = write_reg_txdata_en  & uart_ctrl[0] & (~uart_status[0]) & tx_data_ready;
    wire rx_recv_over = uart_ctrl[1] & rx_data_valid;


    // 写uart_rxdata
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            uart_rx <= 32'h0;
        end else begin
            // 接收完成时，保存接收到的数据
            if (rx_recv_over) begin
                uart_rx[7:0] <= rx_data;
            end
        end
    end

    // 写uart_txdata
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            uart_tx <= 32'h0;
        end else begin
            // 开始发送时，保存要发送的数据
            if (tx_start) begin
                uart_tx[7:0] <= data_i[7:0];
            end
        end
    end

    // 写uart_status
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            uart_status <= 32'h0;
        end else begin
            if (write_reg_status_en) begin
                // 写RX完成标志
                uart_status[1] <= data_i[1];
                
            end else begin
                // 开始发送数据时，置位TX忙标志
                if (tx_start) begin
                    uart_status[0] <= 1'b1;
                // 发送完成时，清TX忙标志
                end else if (tx_data_ready == 1'b1) begin
                    uart_status[0] <= 1'b0;
                // 接收完成，置位接收完成标志
                end
                if (rx_recv_over) begin
                    uart_status[1] <= 1'b1;
                end
            end
        end
    end

    // 写uart_ctrl
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            uart_ctrl <= 32'h0;
        end else begin
            if (write_reg_ctrl_en) begin
                uart_ctrl <= {24'b0,data_i[7:0]};
            end
        end
    end

assign data_o = (addr_i[7:0] == UART_CTRL) ? uart_ctrl:
                (addr_i[7:0] == UART_STATUS) ? uart_status:
                (addr_i[7:0] == UART_RXDATA) ? uart_rx:32'b0;
/***************************************************************************
calling uart_tx module and uart_rx module
****************************************************************************/
    uart_rx#
    (
        .CLK_FRE(50),
        .BAUD_RATE(115200)
    ) uart_rx_inst
    (
        .clk                        (clk                      ),
        .rst_n                      (rstn                    ),
        .rx_data                    (rx_data                  ),
        .rx_data_valid              (rx_data_valid            ),
        .rx_data_ready              (uart_ctrl[1]             ),
        .rx_pin                     (rx_pin                   )
    );
    
    uart_tx#
    (
        .CLK_FRE(50),
        .BAUD_RATE(115200)
    ) uart_tx_inst
    (
        .clk                        (clk                     ),
        .rst_n                      (rstn                   ),
        .tx_data                    (uart_tx[7:0]            ),
        .tx_data_valid              (tx_start                ),
        .tx_data_ready              (tx_data_ready           ), 
        .tx_pin                     (tx_pin                  )
    );

endmodule
