//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module gpio_module(

    input         clk,
    input         rstn,
           
    input         we_i,
    input  [31:0] addr_i,
    input  [31:0] data_i,
    output [31:0] data_o,

    output [31:0] gpio_ctrl_o,
    output [31:0] gpio_data_o,
               
    input  [`GPIO_NUM-1:0] io_pin_i
    );


    // GPIO控制寄存器
    localparam GPIO_CTRL = 4'h0;
    // GPIO数据寄存器
    localparam GPIO_DATA = 4'h4;

    // 1：输出,0：输入
    reg[31:0] gpio_ctrl;
    // 输入输出数据
    reg[31:0] gpio_data;
    //gpio_data处理
    
    wire write_reg_ctrl_en = we_i & (addr_i[3:0] == GPIO_CTRL);
    wire write_reg_data_en = we_i & (addr_i[3:0] == GPIO_DATA);

    assign gpio_ctrl_o = gpio_ctrl;
    assign gpio_data_o = gpio_data;

    genvar i; 
/*    generate
       for(i=0;i<32;i=i+1)
       begin: io_ctrlpin
            assign io_pin_i[i] = gpio_ctrl[i] ? gpio_data[i] : 1'bz;
       end
    endgenerate
*/
    //gpio_data处理
    generate
       for(i=0;i<`GPIO_NUM;i=i+1)
       begin: io_ctrl
            always@(posedge clk or negedge rstn) begin
                if(!rstn) begin
                    gpio_data[i] <= 1'b0;
                end else begin
                    if(write_reg_data_en & gpio_ctrl[i]) begin
                        gpio_data[i] <= data_i[i];
                    end
                    if(~gpio_ctrl[i] ) begin 
                        gpio_data[i] <= io_pin_i[i];
                        /*if(io_pin_i[i])
                            gpio_data[i] <= 1'b1;
                        else
                            gpio_data[i] <= 1'b0;*/
                    end 
 
                end    
             end
       end
    endgenerate

    // gpio_ctrl 处理
    always @ (posedge clk or negedge rstn) begin
        if (!rstn) begin
            gpio_ctrl <= 32'h0;
        end else begin
            if (write_reg_ctrl_en) begin
                  gpio_ctrl <= data_i;
            end 
            else begin 
                  gpio_ctrl <= gpio_ctrl;
            end
        end
    end

    assign data_o = (addr_i[3:0] == GPIO_CTRL) ? gpio_ctrl:
                    (addr_i[3:0] == GPIO_DATA) ? gpio_data : 32'b0 ;

endmodule

