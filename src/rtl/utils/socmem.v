//-----------------------------------------------
//    module name: socmem
//    author: 
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module socmem(
    input  wire        rstn,
    input  wire        clk,
    
    input  wire [ 3:0] i_iwen_4,          
    input  wire [31:0] i_iaddress_32, 
    input  wire [31:0] i_idataW_32,  
    output wire [31:0] o_idataR_32,
    
    input  wire [ 3:0]  i_dwen_4,          
    input  wire [31:0] i_daddress_32, 
    input  wire [31:0] i_ddataW_32,  
    output wire [31:0] o_ddataR_32,
    
    input  wire        req_valid_i,
    output wire        req_ready_o,
    output wire        rsp_valid_o,
    input  wire        rsp_ready_i
    );
     integer i;    
 
`ifdef VIVADO_ENV
    `ifdef BEHAVIOR_SIMULATION                           //仅用于行为仿真，不可被综合
        ///
       reg  [31:0] i_mem[0:8191];
       reg  [31:0] r_idata_32;
       integer i;                                        //循环用
       
       always @ (posedge clk or negedge rstn) begin      //写入存储器
            if(!rstn) begin
               /*for(i=0;i<8192;i=i+1)                   //寄存器上电归0
                 i_mem[i] <= 32'b0;*/
                end
            else if(|i_iwen_4) begin
                if(i_iwen_4[0]) begin i_mem[i_iaddress_32[14:2]][7:0]   <=  i_idataW_32[7:0]; 
                end else        begin i_mem[i_iaddress_32[14:2]][7:0]   <=  i_mem[i_iaddress_32[14:2]][7:0];  end
                if(i_iwen_4[1]) begin i_mem[i_iaddress_32[14:2]][15:8]  <=  i_idataW_32[15:8]; 
                end else        begin i_mem[i_iaddress_32[14:2]][15:8]  <=  i_mem[i_iaddress_32[14:2]][15:8]; end
                if(i_iwen_4[2]) begin i_mem[i_iaddress_32[14:2]][23:16] <=  i_idataW_32[23:16]; 
                end else        begin i_mem[i_iaddress_32[14:2]][23:16] <=  i_mem[i_iaddress_32[14:2]][23:16];end
                if(i_iwen_4[3]) begin i_mem[i_iaddress_32[14:2]][31:24] <=  i_idataW_32[31:24]; 
                end else        begin i_mem[i_iaddress_32[14:2]][31:24] <=  i_mem[i_iaddress_32[14:2]][31:24];end
            end
        end
       
        always @ (posedge clk or negedge rstn) begin      //存储器读出数据
            if(!rstn) begin
                r_idata_32 <= `nop;      
            end else
                r_idata_32 <= i_mem[i_iaddress_32[14:2]];
        end
        assign o_idataR_32 = r_idata_32;
        
        reg [31:0] mem[0:8191];
        reg  [31:0] r_ddata_32; 
        
        always @ (posedge clk or negedge rstn) begin      //写入存储器
            if(!rstn) begin
               for(i=0;i<8192;i=i+1)                      //寄存器上电归0
                 mem[i] <= 32'b0;
                end
            else if(i_dwen_4) begin
                if(i_dwen_4[0]) begin mem[i_daddress_32[14:2]][7:0]   <=  i_ddataW_32[7:0]; 
                end else        begin mem[i_daddress_32[14:2]][7:0]   <=  mem[i_daddress_32[14:2]][7:0]; end
                if(i_dwen_4[1]) begin mem[i_daddress_32[14:2]][15:8]  <=  i_ddataW_32[15:8]; 
                end else        begin mem[i_daddress_32[14:2]][15:8]  <=  mem[i_daddress_32[14:2]][15:8]; end
                if(i_dwen_4[2]) begin mem[i_daddress_32[14:2]][23:16] <=  i_ddataW_32[23:16]; 
                end else        begin mem[i_daddress_32[14:2]][23:16] <=  mem[i_daddress_32[14:2]][23:16]; end
                if(i_dwen_4[3]) begin mem[i_daddress_32[14:2]][31:24] <=  i_ddataW_32[31:24]; 
                end else        begin mem[i_daddress_32[14:2]][31:24] <=  mem[i_daddress_32[14:2]][31:24]; end
            end
        end
        
        always @ (posedge clk or negedge rstn) begin       //存储器读出数据
            if(!rstn) begin
                 r_ddata_32 <= 32'b0;
            end else
                 r_ddata_32 <= mem[i_daddress_32[14:2]];
        end
         assign o_ddataR_32 = r_ddata_32;
         
         
     `endif
     `ifdef POST_SYNTHSIS_SIMULATION //FPGA 后仿使用BRAM
            // VIVADO BLOCK i_memORY IP （SINGLE PORT RAM）
            // 位宽32bit 深度 8192 无任何输入输出寄存器
         I_Cache_32KB8192WORDS13BIT icache(
            .addra(i_iaddress_32[14:2]),
            .clka(clk),
            .dina(i_idataW_32),
            .douta(o_idataR_32),
            .wea(i_iwen_4)
         );
         
            // VIVADO BLOCK MEMORY IP （SINGLE PORT RAM）
            // 位宽32bit 深度 8192 无任何输入输出寄存器
             D_Cache_32KB8192WORDS13BIT dcache(
                .addra(i_daddress_32[14:2]),
                .clka(clk),
                .dina(i_ddataW_32),
                .douta(o_ddataR_32),
                .wea(i_dwen_4)
            );
     `endif
 `else 
     `ifdef DC_ENV
  // UMC .11um  SRAM ip
SHTB110_8192X8X4CM16 icache (
.A0(i_iaddress_32[2]),.A1(i_iaddress_32[3]),.A2(i_iaddress_32[4]),.A3(i_iaddress_32[5]),
.A4(i_iaddress_32[6]),.A5(i_iaddress_32[7]),.A6(i_iaddress_32[8]),.A7(i_iaddress_32[9]),
.A8(i_iaddress_32[10]),.A9(i_iaddress_32[11]),.A10(i_iaddress_32[12]),.A11(i_iaddress_32[13]),.A12(i_iaddress_32[14]),
.DO0(o_idataR_32[0]),.DO1(o_idataR_32[1]),.DO2(o_idataR_32[2]),.DO3(o_idataR_32[3]),.DO4(o_idataR_32[4]),
.DO5(o_idataR_32[5]),.DO6(o_idataR_32[6]),.DO7(o_idataR_32[7]),.DO8(o_idataR_32[8]),.DO9(o_idataR_32[9]),
.DO10(o_idataR_32[10]),.DO11(o_idataR_32[11]),.DO12(o_idataR_32[12]),.DO13(o_idataR_32[13]),.DO14(o_idataR_32[14]),
.DO15(o_idataR_32[15]),.DO16(o_idataR_32[16]),.DO17(o_idataR_32[17]),.DO18(o_idataR_32[18]),.DO19(o_idataR_32[19]),
.DO20(o_idataR_32[20]),.DO21(o_idataR_32[21]),.DO22(o_idataR_32[22]),.DO23(o_idataR_32[23]),.DO24(o_idataR_32[24]),
.DO25(o_idataR_32[25]),.DO26(o_idataR_32[26]),.DO27(o_idataR_32[27]),.DO28(o_idataR_32[28]),.DO29(o_idataR_32[29]),
.DO30(o_idataR_32[30]),.DO31(o_idataR_32[31]),
.DI0(i_idataW_32[0]),.DI1(i_idataW_32[1]),.DI2(i_idataW_32[2]),.DI3(i_idataW_32[3]),.DI4(i_idataW_32[4]),.DI5(i_idataW_32[5]),
.DI6(i_idataW_32[6]),.DI7(i_idataW_32[7]),.DI8(i_idataW_32[8]),.DI9(i_idataW_32[9]),.DI10(i_idataW_32[10]),.DI11(i_idataW_32[11]),
.DI12(i_idataW_32[12]),.DI13(i_idataW_32[13]),.DI14(i_idataW_32[14]),.DI15(i_idataW_32[15]),.DI16(i_idataW_32[16]),
.DI17(i_idataW_32[17]),.DI18(i_idataW_32[18]),.DI19(i_idataW_32[19]),.DI20(i_idataW_32[20]),.DI21(i_idataW_32[21]),
.DI22(i_idataW_32[22]),.DI23(i_idataW_32[23]),.DI24(i_idataW_32[24]),.DI25(i_idataW_32[25]),.DI26(i_idataW_32[26]),
.DI27(i_idataW_32[27]),.DI28(i_idataW_32[28]),.DI29(i_idataW_32[29]),.DI30(i_idataW_32[30]),.DI31(i_idataW_32[31]),
.WEB0(i_iwen_4[0]),.WEB1(i_iwen_4[1]),.WEB2(i_iwen_4[2]),.WEB3(i_iwen_4[3]),
.DVSE(1'b0),
.DVS0(1'b0),.DVS1(1'b0),.DVS2(1'b0),.DVS3(1'b0),
.CK(clk),
.CSB(1'b0),
.OE(1'b1));
             
               // UMC .11um  SRAM ip
SHTB110_8192X8X4CM16 dcache (
.A0(i_daddress_32[2]),.A1(i_daddress_32[3]),.A2(i_daddress_32[4]),.A3(i_daddress_32[5]),
.A4(i_daddress_32[6]),.A5(i_daddress_32[7]),.A6(i_daddress_32[8]),.A7(i_daddress_32[9]),
.A8(i_daddress_32[10]),.A9(i_daddress_32[11]),.A10(i_daddress_32[12]),.A11(i_daddress_32[13]),.A12(i_daddress_32[14]),
.DO0(o_ddataR_32[0]),.DO1(o_ddataR_32[1]),.DO2(o_ddataR_32[2]),.DO3(o_ddataR_32[3]),.DO4(o_ddataR_32[4]),
.DO5(o_ddataR_32[5]),.DO6(o_ddataR_32[6]),.DO7(o_ddataR_32[7]),.DO8(o_ddataR_32[8]),.DO9(o_ddataR_32[9]),
.DO10(o_ddataR_32[10]),.DO11(o_ddataR_32[11]),.DO12(o_ddataR_32[12]),.DO13(o_ddataR_32[13]),.DO14(o_ddataR_32[14]),
.DO15(o_ddataR_32[15]),.DO16(o_ddataR_32[16]),.DO17(o_ddataR_32[17]),.DO18(o_ddataR_32[18]),.DO19(o_ddataR_32[19]),
.DO20(o_ddataR_32[20]),.DO21(o_ddataR_32[21]),.DO22(o_ddataR_32[22]),.DO23(o_ddataR_32[23]),.DO24(o_ddataR_32[24]),
.DO25(o_ddataR_32[25]),.DO26(o_ddataR_32[26]),.DO27(o_ddataR_32[27]),.DO28(o_ddataR_32[28]),.DO29(o_ddataR_32[29]),
.DO30(o_ddataR_32[30]),.DO31(o_ddataR_32[31]),
.DI0(i_ddataW_32[0]),.DI1(i_ddataW_32[1]),.DI2(i_ddataW_32[2]),.DI3(i_ddataW_32[3]),.DI4(i_ddataW_32[4]),.DI5(i_ddataW_32[5]),
.DI6(i_ddataW_32[6]),.DI7(i_ddataW_32[7]),.DI8(i_ddataW_32[8]),.DI9(i_ddataW_32[9]),.DI10(i_ddataW_32[10]),.DI11(i_ddataW_32[11]),
.DI12(i_ddataW_32[12]),.DI13(i_ddataW_32[13]),.DI14(i_ddataW_32[14]),.DI15(i_ddataW_32[15]),.DI16(i_ddataW_32[16]),
.DI17(i_ddataW_32[17]),.DI18(i_ddataW_32[18]),.DI19(i_ddataW_32[19]),.DI20(i_ddataW_32[20]),.DI21(i_ddataW_32[21]),
.DI22(i_ddataW_32[22]),.DI23(i_ddataW_32[23]),.DI24(i_ddataW_32[24]),.DI25(i_ddataW_32[25]),.DI26(i_ddataW_32[26]),
.DI27(i_ddataW_32[27]),.DI28(i_ddataW_32[28]),.DI29(i_ddataW_32[29]),.DI30(i_ddataW_32[30]),.DI31(i_ddataW_32[31]),
.WEB0(i_dwen_4[0]),.WEB1(i_dwen_4[1]),.WEB2(i_dwen_4[2]),.WEB3(i_dwen_4[3]),
.DVSE(1'b0),
.DVS0(1'b0),.DVS1(1'b0),.DVS2(1'b0),.DVS3(1'b0),
.CK(clk),
.CSB(1'b0),
.OE(1'b1));

    `endif
`endif
              
    vld_rdy #(
        .CUT_READY(0)
    ) u_vld_rdy(
        .clk(clk),
        .rst_n(rstn),
        .vld_i(req_valid_i),
        .rdy_o(req_ready_o),
        .rdy_i(rsp_ready_i),
        .vld_o(rsp_valid_o)
    );

endmodule
