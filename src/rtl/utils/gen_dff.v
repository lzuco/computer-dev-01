//-----------------------------------------------
//    module name: 
//    author: Wei Ren
//  
//    version: 1st version (2021-10-01)
//    description: 
//    带使能端、复位后输出为0的触发器  
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module gen_en_dff #(
    parameter DW = 32)(

    input wire clk,
    input wire rst_n,

    input wire en,
    input wire[DW-1:0] din,
    output wire[DW-1:0] qout

    );

    reg[DW-1:0] qout_r;

    always @ (posedge clk or negedge rst_n) begin
        if (!rst_n) begin
            qout_r <= {DW{1'b0}};
        end else if (en == 1'b1) begin
            qout_r <= din;
        end
    end

    assign qout = qout_r;
 endmodule   