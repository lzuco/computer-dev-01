//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module cpu_slot(

    input  wire        clk,
    input  wire        rstn,
    
    input  wire        init_enable,
    input  wire        init_rx,
    output wire        init_tx,
    input  wire [4:0]  int_sig_cpu,
           
    output wire        in_R_to      ,
    input  wire        out_R_from   ,
    output wire [50:0] data_to      ,
    input  wire [50:0] data_from    ,
    output wire        click_out_to ,
    input  wire        click_in_from
    
    );
    wire [31:0] w_instAddr_32 ;
    wire [31:0] w_instData_32 ;

    wire [3:0]  w_dataWen_1   ;
    wire [31:0] w_dataAddr_32 ;
    wire [31:0] w_writeData_32;
    wire [31:0] w_readData_32 ;
    
    wire  [3:0]  w_dbus_we     ;
    wire [31:0] w_dbus_addr  ;
    wire [31:0] w_dbus_data_o;
    wire [31:0] w_dbus_data_i;
    
    wire        w_ibus_req_valid;
    wire        w_ibus_req_ready;
    wire        w_ibus_rsp_valid;
    wire        w_ibus_rsp_ready;
    
    wire        w_dbus_req_valid;
    wire        w_dbus_req_ready;
    wire        w_dbus_rsp_valid;
    wire        w_dbus_rsp_ready;
    
    wire        w_dcache_req_valid;
    wire        w_dcache_req_ready;
    wire        w_dcache_rsp_valid;    
    wire        w_dcache_rsp_ready;
    
    memory_slot memory(

        .clk                (clk            ),
        .rstn               (rstn            ),
        
        .init_enable        (init_enable),
        .init_rx            (init_rx    ),
        .init_tx            (init_tx    ),
        
        .ibus_we            (4'b0           ),
        .ibus_addr_i        (w_instAddr_32  ),
        .ibus_data_i        (32'b0          ),
        .ibus_data_o        (w_instData_32  ),

        .dbus_we            (w_dbus_we    ),
        .dbus_addr_i        (w_dbus_addr),
        .dbus_data_i        (w_dbus_data_o),
        .dbus_data_o        (w_dbus_data_i),
    
        .dbus_req_valid_i   (w_dcache_req_valid),
        .dbus_req_ready_o   (w_dcache_req_ready),
        .dbus_rsp_valid_o   (w_dcache_rsp_valid),
        .dbus_rsp_ready_i   (w_dcache_rsp_ready)
    
    );    
    
    


    cpu_core cpu_core(
        .clk                (clk            ),
        .rstn               (rstn            ),   
        .i_intFlag_5        (int_sig_cpu     ),

        .o_instAddr_32      (w_instAddr_32    ),
        .i_instData_32      (w_instData_32    ),

        .o_dataWen_4        (w_dataWen_1   ),
        .o_dataAddr_32      (w_dataAddr_32 ),
        .o_writeData_32     (w_writeData_32),
        .i_readData_32      (w_readData_32 ),

        .dbus_req_valid_o    (w_dbus_req_valid),
        .dbus_req_ready_i    (w_dbus_req_ready),
        .dbus_rsp_valid_i    (w_dbus_rsp_valid),
        .dbus_rsp_ready_o    (w_dbus_rsp_ready)

    );    

    slot_data data_mux(

        .clk                (clk              ),
        .rstn                (rstn              ),
    
        .data_bus_we        (w_dataWen_1        ),
        .data_bus_addr_i    (w_dataAddr_32      ),
        .data_bus_data_i    (w_writeData_32     ),
        .data_bus_data_o    (w_readData_32      ),
    
        .data_req_valid_i    (w_dbus_req_valid),
        .data_req_ready_o    (w_dbus_req_ready),
        .data_rsp_valid_o    (w_dbus_rsp_valid),
        .data_rsp_ready_i    (w_dbus_rsp_ready),
    
        .dcache_we            (w_dbus_we     ),
        .dcache_addr_o        (w_dbus_addr  ),
        .dcache_data_o        (w_dbus_data_o),
        .dcache_data_i        (w_dbus_data_i),

        .dcache_req_valid_o    (w_dcache_req_valid),
        .dcache_req_ready_i    (w_dcache_req_ready),
        .dcache_rsp_valid_i    (w_dcache_rsp_valid),
        .dcache_rsp_ready_o    (w_dcache_rsp_ready),
    
        .in_R_to            (in_R_to       ),
        .out_R_from         (out_R_from    ),
        .data_to            (data_to       ),
        .data_from          (data_from     ),
        .click_out_to       (click_out_to  ),
        .click_in_from      (click_in_from )
    );

endmodule    
