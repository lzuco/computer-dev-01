//-----------------------------------------------
//    module name: memory_slot
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 该模块实例化了socmem模块和data_init模块，并通过init_enable控制两个模块：
//                 当init_enable为高电平时，data_init模块工作，初始化存储器，
//                 当init_enable为低电平时，socmem模块工作，从存储器中读出数据
//
//-----------------------------------------------
`timescale 1ns / 1ps
module memory_slot(
    //来自总线的信号
    input  wire         clk,
    input  wire         rstn,
    input  wire         init_enable,
    input  wire         init_rx,
    //输出到tx引脚
    output wire         init_tx,

    //Icache总线数据
    input  wire [ 3:0]   ibus_we        ,
    input  wire [31:0]  ibus_addr_i,
    input  wire [31:0]  ibus_data_i,
    output wire [31:0]  ibus_data_o,

    //Dcache总线数据
    input  wire [ 3:0]     dbus_we        ,
    input  wire [31:0]     dbus_addr_i,
    input  wire [31:0]     dbus_data_i,
    output wire [31:0]     dbus_data_o,

    //Dcache总线信号          
    input  wire          dbus_req_valid_i,
    output wire          dbus_req_ready_o,
    output wire          dbus_rsp_valid_o,
    input  wire          dbus_rsp_ready_i

    );
   //Icache 存储信号线
    wire [3:0]          memory_ibus_we;
    wire [31:0]         memory_ibus_addr_i;      
    wire [31:0]         memory_ibus_data_i;        
    wire [31:0]         memory_ibus_data_o;   

   //Dcache 存储信号线
    wire [3:0]          memory_dbus_we;        
    wire [31:0]         memory_dbus_addr_i; 
    wire [31:0]         memory_dbus_data_i; 
    wire [31:0]         memory_dbus_data_o; 
    
    //Icache存储器 初始化信号线
    wire                init_ibus_we    ;
    wire [31:0]         init_ibus_addr    ;      
    wire [31:0]         init_ibus_data    ;        
    
    //Dcache存储器 初始化信号线
    wire                init_dbus_we    ;        
    wire [31:0]         init_dbus_addr    ; 
    wire [31:0]         init_dbus_data    ; 

    //根据init_enable选择总线数据
    assign memory_ibus_we      = init_enable ?  {4{init_ibus_we}} :  ibus_we      ;
    assign memory_ibus_addr_i  = init_enable ?  init_ibus_addr    :  ibus_addr_i  ;
    assign memory_ibus_data_i  = init_enable ?  init_ibus_data    :  ibus_data_i  ;
    assign memory_dbus_we      = init_enable ?  {4{init_dbus_we}} :  dbus_we      ;
    assign memory_dbus_addr_i  = init_enable ?  init_dbus_addr    :  dbus_addr_i  ;
    assign memory_dbus_data_i  = init_enable ?  init_dbus_data    :  dbus_data_i  ;
    
    assign ibus_data_o = memory_ibus_data_o;
    assign dbus_data_o = memory_dbus_data_o;

    socmem socmem(
        .rstn                   (rstn),
        .clk                    (clk),
        
        .i_iwen_4               (memory_ibus_we    ),          
        .i_iaddress_32          (memory_ibus_addr_i), 
        .i_idataW_32            (memory_ibus_data_i),  
        .o_idataR_32            (memory_ibus_data_o),
        
        .i_dwen_4               (memory_dbus_we    ),              
        .i_daddress_32          (memory_dbus_addr_i),
        .i_ddataW_32            (memory_dbus_data_i),   
        .o_ddataR_32            (memory_dbus_data_o), 
        
        .req_valid_i            (dbus_req_valid_i  ),
        .req_ready_o            (dbus_req_ready_o  ),
        .rsp_valid_o            (dbus_rsp_valid_o  ),
        .rsp_ready_i            (dbus_rsp_ready_i  )
    );
    
    data_init data_init(
       .clk             (clk),        
       .rst_n           (rstn),
       
       .uart_rx         (init_rx),         
       .enable          (init_enable),
       .uart_tx         (init_tx),         

       .ibus_we         (init_ibus_we),
       .ibus_addr_o     (init_ibus_addr),
       .ibus_data_o     (init_ibus_data),
       .dbus_we         (init_dbus_we ),
       .dbus_addr_o     (init_dbus_addr),
       .dbus_data_o     (init_dbus_data)
    );
    
endmodule    