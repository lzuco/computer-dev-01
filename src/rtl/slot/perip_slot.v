//-----------------------------------------------
//    module name: 
//    author:  LuYihua
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module perip_slot(

    input  wire         clk,
    input  wire         rstn,

    output reg          in_R_to      ,
    input  wire         out_R_from   ,
    output reg  [50:0]  data_to      ,
    input  wire [50:0]  data_from    ,
    output wire         click_out_to ,
    input  wire         click_in_from,

    output reg          we,
    output reg [31:0]   addr_i,
    output reg [31:0]   data_i,
    input  wire [31:0]  data_o
    
    );
    
    assign click_out_to = out_R_from;
    
    reg  state;
    reg r_out_R_from;
    localparam      IDLE = 1'b0;
    localparam      WAIT = 1'b1;

    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            in_R_to <= 0;
            data_to <= 0;
            r_out_R_from <= 0;
            state <= 0;
            we <= 0;
            addr_i <= 0;
            data_i <=0;
        end
        else begin
                r_out_R_from <= out_R_from;
                case(state)
                    IDLE: begin 
                                if(out_R_from != r_out_R_from) begin
                                    we     <= data_from[50];
                                    addr_i <= {24'b0,data_from[49:42]};
                                    data_i <= data_from[41:10];
                                    state  <= data_from[50] ? IDLE:WAIT;
                                end
                                else begin
                                    we     <=    1'b0;
                                end
                                end
                    WAIT: begin 
                                we <= 1'b0;
                                in_R_to <= ~in_R_to;
                                data_to <= {1'b0,data_from[49:42],data_o,10'b0};
                                state <= IDLE;
                            end
                endcase
            end
    end

endmodule    