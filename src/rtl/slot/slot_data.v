//-----------------------------------------------
//    module name: slot_data
//    author: Wei Ren
//  
//    version: 1st version (2021-10-01)
//    description: 该模块用于CPU与MEMOEY以及MESH网络之间的数据传输，
//                  MESH网络上传输的数据采用数据包的格式
//
//
//-----------------------------------------------
`timescale 1ns / 1ps

module slot_data(

    input  wire        clk,
    input  wire        rstn,

    //CPU信号
    input  wire [ 3:0] data_bus_we,                // 数据写使能
    input  wire [31:0] data_bus_addr_i,            // 数据地址
    input  wire [31:0] data_bus_data_i,            // slot数据输入
    output wire [31:0] data_bus_data_o,            // slot数据输出

    input  wire        data_req_valid_i,           // 数据请求有效输入
    output wire        data_req_ready_o,           // 数据请求就绪输出
    output wire        data_rsp_valid_o,           // 数据响应有效输出
    input  wire        data_rsp_ready_i,           // 数据响应就绪输入

    //MEMOEY信号
    output wire [ 3:0] dcache_we,                  // dcache写使能输出
    output wire [31:0] dcache_addr_o,              // dcache写入地址输出
    output wire [31:0] dcache_data_o,              // dcache写入数据输出
    input  wire [31:0] dcache_data_i,

    output wire        dcache_req_valid_o,         // dcache请求有效输出
    input  wire        dcache_req_ready_i,         // dcache请求就绪输入
    input  wire        dcache_rsp_valid_i,         // dcache响应有效输入
    output wire        dcache_rsp_ready_o,         // dcache响应就绪输出

    output wire        in_R_to,
    input  wire        out_R_from,
    output wire [50:0] data_to,
    input  wire [50:0] data_from,
    output wire        click_out_to,
    input  wire        click_in_from
    
    );
    
    //CPU <-> MEMOEY 传送时 与 MESH无关 
    //CPU <-> MESH   传送时 与 MEMORY 无关
    
    //MESH 数据包格式 1bit wen + 8bit addr + 32bit data + 10xy 
    
    //cpu   -> local
    //uart  -> west
    //gpio  -> south
    //timer -> north

    /******************** 来自CPU的信号解析 ********************/
    reg  [31:0] r_data_bus_data_o;
    // 信号拆分
    wire        w_meshWen_1;      // mesh数据包写使能位
    wire [ 7:0] w_meshAddr_8;     // mesh数据包地址段
    wire [31:0] w_meshData_32;    // mesh数据包数据段
    wire [ 4:0] w_meshX_5;        // mesh数据包X方向段
    wire [ 4:0] w_meshY_5;        // mesh数据包Y方向段

    // 信号重命名
    wire w_toMesh_1;              // 与Mesh交互
    wire w_toCache_1;             // 与cache交互
    wire w_toUART_1;              // 与UART交互
    wire w_toGPIO_1;              // 与GPIO交互
    wire w_toTIMER_1;             // 与TIMER交互
    wire w_toSPI_1;               // 与SPI交互
    
    assign w_toMesh_1    = data_req_valid_i & data_bus_addr_i[15];                 // 当CPU访问地址第15位为1时与mesh交互
    assign w_toCache_1   = data_req_valid_i & ~data_bus_addr_i[15];                // 当CPU访问地址第15位为1时与mesh交互
    assign w_toUART_1    = w_toMesh_1       & data_bus_addr_i[12];                 // 当与mesh交互时,访问地址第十二位为1时与UART交互
    assign w_toGPIO_1    = w_toMesh_1       & data_bus_addr_i[13];                 // 当与mesh交互时,访问地址第十三位为1时与GPIO交互
    assign w_toTIMER_1   = w_toMesh_1       & data_bus_addr_i[14];                 // 当与mesh交互时,访问地址第十四位为1时与TIMER交互
    assign w_toSPI_1     = w_toMesh_1       & ~data_bus_addr_i[12]& ~data_bus_addr_i[13]& ~data_bus_addr_i[14];    
    
    // 写使能信号输出
    assign dcache_we     =  w_toCache_1?data_bus_we :4'b0;                         // dcache写使能
    assign w_meshWen_1   = |data_bus_we & w_toMesh_1;                              // mesh数据包中写使能位

    // 地址信号输出
    assign dcache_addr_o = data_bus_addr_i         & {32{w_toCache_1}};            // 当与cache交互时,输出至dcache的地址为CPU输入的访问地址
    assign w_meshAddr_8  = data_bus_addr_i[7:0]    & { 8{w_toMesh_1 }};            // 当与mesh交互时,输出至mesh网中数据包内地址段为CPU输入的访问地址低八位

    // 数据信号输出
    assign dcache_data_o = data_bus_data_i & {32{w_toCache_1}};                    // 当与cache交互时,输出至dcache的数据为CPU输入的数据
    assign w_meshData_32 = data_bus_data_i & {32{w_toMesh_1 }};                    // 当与mesh交互时,输出至mesh网中数据包内数据段的数据为CPU输入的数据
    
    // mesh传输方向解析(生成mesh数据包方向部分)
    assign w_meshX_5     = {{5{w_toUART_1}}                &    {5'b00001}}        // 当与UART交互时,X方向为00001
                         | {{5{w_toGPIO_1|w_toTIMER_1}}    &    {5'b00000}}        // 其他情况下,X方向为全零
                         | {{5{w_toSPI_1   }}              & {5'b10001}};
                    
    assign w_meshY_5     = {{5{w_toUART_1    }}    & {5'b00000}}                   // 当与UART交互时,Y方向全零
                         | {{5{w_toGPIO_1    }}    & {5'b00001}}                   // 当与GPIO交互时,Y方向为10001
                         | {{5{w_toTIMER_1   }}    & {5'b10001}}                   // 当与TIMER交互时,Y方向为00001
                         | {{5{w_toSPI_1   }}      & {5'b00000}};
    reg r_data_req_ready_o;
    reg r_data_rsp_valid_o;
    assign data_req_ready_o     = {w_toCache_1    & dcache_req_ready_i    }        // 当与cache交互时,直接返回cache的req_ready信号
                                | {w_toMesh_1     & r_data_req_ready_o    };       // 当与mesh交互时,非满写或非空读情况下有效

    assign data_rsp_valid_o     = {w_toCache_1    & dcache_rsp_valid_i    }        // 当与cache交互时,直接返回cache的rsp_valid信号
                                | {w_toMesh_1     & r_data_rsp_valid_o    };       // 当与mesh交互时,前一周期读或当前周期非满写时有效
                                
    reg  state;
    localparam      IDLE = 1'b0;
    localparam      WAIT = 1'b1;
    reg r_out_R_from;
    reg r_in_R_to;
    assign in_R_to = r_in_R_to ;
    assign data_to = {w_meshWen_1,w_meshAddr_8,w_meshData_32,w_meshX_5,w_meshY_5};
                               
    assign click_out_to = out_R_from;
    always @(posedge clk or negedge rstn) begin
        if(!rstn) begin
            r_in_R_to          <= 0;
            r_out_R_from       <= 0;
            state              <= 0;
            r_data_rsp_valid_o <= 0;
            r_data_req_ready_o <= 0;
            r_data_bus_data_o  <= 0;
        end
        else begin
            r_out_R_from       <= out_R_from;
            r_data_rsp_valid_o <= 0;
            case(state)
                IDLE: begin 
                            if(w_toMesh_1) begin
                                r_in_R_to <= ~r_in_R_to;
                                r_data_req_ready_o     <= 1'b1;
                                if(w_meshWen_1) begin
                                    state <= IDLE; 
                                    r_data_rsp_valid_o <= 1'b1;
                                end  else begin
                                    state <= WAIT;//读等待返回
                                end
                            end
                            else begin
                                state <= IDLE;
                            end
                        end 
                WAIT: begin 
                            if(out_R_from != r_out_R_from) //out_R没有翻转
                            begin
                                state              <= IDLE;
                                r_data_bus_data_o  <= data_from[42:10];    
                                r_data_rsp_valid_o <= 1'b1;        
                            end else begin
                                state <= WAIT;
                            end  
                        end
            endcase
            end
        end

    // CPU与cache握手信号输出
    assign dcache_req_valid_o = data_req_valid_i & w_toCache_1;        // 与cache交互时,将CPU输入的req_valid信号直接与输出至cache的req_valid信号相连
    assign dcache_rsp_ready_o = data_rsp_ready_i & w_toCache_1;        // 与cache交互时,将CPU输入的rsp_ready信号直接与输出至cache的rsp_ready信号相连
    assign data_bus_data_o    = r_data_bus_data_o;
endmodule    