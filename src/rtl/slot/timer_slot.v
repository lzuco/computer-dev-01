//-----------------------------------------------
//    module name: 
//    author: Liang
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module timer_slot(
    input  wire         clk,
    input  wire         rstn,
    output wire         int_sig_o,
    
    output wire         in_R_to      ,
    input  wire         out_R_from   ,
    output wire [50:0]  data_to      ,
    input  wire [50:0]  data_from    ,
    output wire         click_out_to ,
    input  wire         click_in_from
    );
    
    wire         we;
    wire [31:0]    addr_i;
    wire [31:0]    data_i;
    wire [31:0]    data_o;
  
    perip_slot slot(

        .clk            (clk            ),
        .rstn            (rstn            ),
                        
        .in_R_to            (in_R_to       ),
        .out_R_from         (out_R_from    ),
        .data_to            (data_to       ),
        .data_from          (data_from     ),
        .click_out_to       (click_out_to  ),
        .click_in_from      (click_in_from ),
                        
        .we                (we                ),
        .addr_i            (addr_i            ),
        .data_i            (data_i            ),
        .data_o            (data_o            )
    
    );
    
    timer_module timer_module(
        .clk            (clk            ),
        .rstn            (rstn            ),
        .we_i            (we                ),
        .addr_i            (addr_i            ),
        .data_i            (data_i            ),
        .data_o            (data_o            ),
        .int_sig_o        (int_sig_o        )
    );

    
endmodule
