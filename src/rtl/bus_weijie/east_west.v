`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/03/27 14:33:55
// Design Name: 
// Module Name: west_east
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module east_west(West_out,South_north_message,East_merge_out);
input [50:0] East_merge_out;
output [50:0] West_out;
output [50:0] South_north_message;

(* KEEP="TRUE"*) wire [50:0] East_merge_out;
(* KEEP="TRUE"*) wire a;
(* KEEP="TRUE"*) wire [3:0] b;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) subtracter_7bit i0 (b,East_merge_out[8:5],4'b0001);
assign a=East_merge_out[8]|East_merge_out[7]|East_merge_out[6]|East_merge_out[5];


//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] West_out;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] South_north_message;

// always@*
// begin
// if(!rst)begin
// South_north_message<=51'b0;
// West_out<=51'b0;end
// else if (a==0)begin
// South_north_message<=East_merge_out;
// West_out<=51'b0;end
// else if (a==1)begin
// South_north_message<=51'b0;
// West_out<={East_merge_out[31:15],b,East_merge_out[7:0]};end
// else begin
// South_north_message<=51'b0;
// West_out<=51'b0;end
// end


//--------------updated code-------------------------------
(* KEEP="TRUE"*) wire [50:0] West_out;
(* KEEP="TRUE"*) wire [50:0] South_north_message;

assign    West_out = (a==1) ? {East_merge_out[50:9],b,East_merge_out[4:0]} : 51'b0;
assign    South_north_message = (a==0) ? East_merge_out : 51'b0;


endmodule
