`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/03/27 14:33:55
// Design Name: 
// Module Name: north_south
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module south_north(North_out,message,North_merge_out);
output [50:0] North_out;
output [40:0] message;
input [50:0] North_merge_out;
(* KEEP="TRUE"*) wire [50:0] North_merge_out;

(* KEEP="TRUE"*) wire a;

(* KEEP="TRUE"*) wire [3:0] b;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) subtracter_7bit i0 (b,North_merge_out[3:0],4'b0001);
assign a=North_merge_out[3]|North_merge_out[2]|North_merge_out[1]|North_merge_out[0];


//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *)  reg [15:0] message;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *)  reg [50:0] North_out;
// always@*
// begin
// if(!rst)begin
// message<=16'b0;
// North_out<=51'b0;end
// else if (a==0)begin
// message<=North_merge_out [31:16];
// North_out<=51'b0;end
// else if(a==1)begin
// message<=16'b0;
// North_out<={North_merge_out [31:7],b};end
// else begin
// message<=16'b0;
// North_out<=51'b0;end
// end

//--------------updated code-------------------------------
(* KEEP="TRUE"*) wire [40:0] message;
(* KEEP="TRUE"*) wire [50:0] North_out;
assign message = (a==0) ? North_merge_out[50:10] : 41'b0;
assign North_out = (a==1) ? {North_merge_out[50:4],b} : 51'b0;


endmodule
