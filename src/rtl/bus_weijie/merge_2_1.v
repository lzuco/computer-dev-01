`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/03/27 11:04:48
// Design Name: 
// Module Name: merge_2_1
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module merge_2_1(out,a,b);
input [50:0] a;
input [50:0] b;
output[50:0] out;

(* KEEP="TRUE"*) wire a_latch;
(* KEEP="TRUE"*) wire b_latch;
assign a_latch=|a;
assign b_latch=|b;
//assign a_latch=a[0]|a[1]|a[2]|a[3]|a[4]|a[5]|a[6]|a[7]|a[8]|a[9]|a[10]|a[11]|a[12]|a[13]|a[14]|a[15]|a[16]|a[17]|a[18]|a[19]|a[20]|a[21]|a[22]|a[23]|a[24]|a[25]|a[26]|a[27]|a[28]|a[29]|a[30]|a[31];
//assign b_latch=b[0]|b[1]|b[2]|b[3]|b[4]|b[5]|b[6]|b[7]|b[8]|b[9]|b[10]|b[11]|b[12]|b[13]|b[14]|b[15]|b[16]|b[17]|b[18]|b[19]|b[20]|b[21]|b[22]|b[23]|b[24]|b[25]|b[26]|b[27]|b[28]|b[29]|b[30]|b[31];


//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] out;

// always@*
// begin
// if(!rst)begin
        // out<=51'b0;end
// else if(a_latch==1)begin
        // out<=a;end
// else if(b_latch==1)begin
        // out<=b;end
// end

//--------------updated code-------------------------------
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] out;

assign    out = (a_latch==1) ? a : (b_latch==1) ? b : 51'b0;


endmodule
