`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/04/19 10:41:41
// Design Name: 
// Module Name: arbiter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module arbiter(a_in,a_out,fire2,priority);
input [4:0] a_in;
input fire2;
output [4:0] a_out;
//input rst;
input [4:0] priority;
(* KEEP="TRUE"*) wire [4:0] a_in;
(* KEEP="TRUE"*) wire [4:0] a_out;
(* KEEP="TRUE"*) wire [4:0] avp_out;
(* KEEP="TRUE"*) wire [3:0] Q;


//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [4:0] X;

// always@*
// begin
    // if(!rst)
        // X=5'b00000;
    // else
// begin
// case(Q)
// 4'b0100: X=5'b10000;
// 4'b0101: X=5'b01000;
// 4'b0110: X=5'b00100;
// 4'b0111: X=5'b00010;
// 4'b1000: X=5'b00001;
// default: X=5'b10000; 
// endcase
// end
// end

//--------------updated code-------------------------------
(* KEEP="TRUE"*) wire [4:0] X;

               

(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) arbitrator_variable_priority avp(a_in,X,avp_out);


//(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) count c(priority,Q,fire2,rst);
//assign    X = (Q == 4'b0100) ? 5'b10000 : (Q == 4'b0101) ? 5'b01000 : (Q == 4'b0110) ? 5'b00100 : (Q == 4'b0111) ? 5'b00010 : (Q == 4'b1000) ? 5'b00001 : 5'b10000; 
assign X=5'b10000;

assign a_out=avp_out;
endmodule

// case(C) avp_out
// 5'b10000:  P=4'b0100;        Q=4'b0101;      X=5'b01000  
// 5'b01000:  P=4'b0101;        Q=4'b0110;      X=5'b00100
// 5'b00100:  P=4'b0110;        Q=4'b0111;      X=5'b00010
// 5'b00010:  P=4'b0111;        Q=4'b1000;      X=5'b00001
// 5'b00001:  P=4'b1000;        Q=4'b0100;      X=5'b10000
// default:   P=4'b0011;        Q=4'b0100;      X=5'b10000
// endcase