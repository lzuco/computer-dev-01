`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/26 01:19:20
// Design Name: 
// Module Name: delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module delay(in_R, in_A, out_R, out_A);
input   in_R,out_A;
output  out_R,in_A; 
 (* dont_touch = "true" *)wire in_R,out_A,out_R,in_A;
 (* dont_touch = "true" *)wire out_R_1,out_A_1,out_R_2,out_A_2;
 (* dont_touch = "true" *)wire out_R_3,out_A_3,out_R_4,out_A_4,out_R_5,out_A_5,out_R_6,out_A_6,out_R_7,out_A_7;


telescope telescope_1(in_R,in_A,out_R_1,out_A_1);
telescope telescope_2(out_R_1,out_A_1,out_R_2,out_A_2);
telescope telescope_3(out_R_2,out_A_2,out_R_3,out_A_3);
telescope telescope_4(out_R_3,out_A_3,out_R_4,out_A_4);
telescope telescope_5(out_R_4,out_A_4,out_R_5,out_A_5);
telescope telescope_6(out_R_5,out_A_5,out_R_6,out_A_6);
telescope telescope_7(out_R_6,out_A_6,out_R_7,out_A_7);
telescope telescope_8(out_R_7,out_A_7,out_R,out_A);




endmodule
