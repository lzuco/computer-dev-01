`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/03/27 11:20:44
// Design Name: 
// Module Name: subtracter_7bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module subtracter_7bit(s,a,b );
input [3:0] a,b;
output [3:0] s;

(* KEEP="TRUE"*) wire [3:0] p;
(* KEEP="TRUE"*) wire [2:0] Cb;
(* KEEP="TRUE"*) wire C0=0;

assign p[0] = a[0]^b[0];
assign p[1] = a[1]^b[1];
assign p[2] = a[2]^b[2];
assign p[3] = a[3]^b[3];
//assign p[4] = a[4]^b[4];
//assign p[5] = a[5]^b[5];
//assign p[6] = a[6]^b[6];

assign Cb[0]=(p[0]&b[0])|(~p[0]&C0);
assign Cb[1]=(p[1]&b[1])|(~p[1]&Cb[0]);
assign Cb[2]=(p[2]&b[2])|(~p[2]&Cb[1]);
//assign Cb[4]=(p[3]&b[3])|(~p[3]&Cb[3]);
//assign Cb[5]=(p[4]&b[4])|(~p[4]&Cb[4]);
//assign Cb[6]=(p[5]&b[5])|(~p[0]&Cb[5]);

assign s[0]=p[0]^C0;
assign s[1]=p[1]^Cb[0];
assign s[2]=p[2]^Cb[1];
assign s[3]=p[3]^Cb[2];
//assign s[4]=p[4]^Cb[4];
//assign s[5]=p[5]^Cb[5];
//assign s[6]=p[6]^Cb[6];


//assign b=a-1;
endmodule
