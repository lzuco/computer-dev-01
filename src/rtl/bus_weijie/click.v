`timescale 1ns / 1ps
//

module click(in_R, in_A, out_R, out_A, fire); //RESETn fire);
input in_R;
output in_A;
output out_R;
input out_A;
output fire;
//input  rst;

wire fire;
wire tmp;
wire request;
wire empty;
wire state;
wire extern_state;
wire clock;
wire in_R_delayed;
wire out_A_delayed;
//reg state;
//always @(posedge rst)
//begin
//    state = 1'b0;
//end
//xor neq2_request_in(delayed_in_R, in_A, request_in);
//assign request_in = delayed_in_R ^ in_A;
//assign request = in_R_delayed ^ in_A;
LUT2 #(.INIT(4'b0110)) neq2_request_in 
(
   .O(request),  
   .I0(in_R_delayed), 
   .I1(in_A)  
);


//not eq2_empty_out_part1(anping_tmp, empty_out);
//xor eq2_empty_out(delayed_out_A, out_R, anping_tmp);
//assign empty_out = ~(delayed_out_A ^ out_R);









//assign empty = ~(out_A_delayed ^ out_R);
LUT2 #(.INIT(4'b1001)) eq2_empty_out 
(
   .O(empty),  
   .I0(out_A_delayed), 
   .I1(out_R)  
);


//and and2_clock(request, empty, clock);
//assign clock = request & empty;




//assign clock = request & empty;

LUT2 #(.INIT(4'b1000)) and2_clock (
   .O(clock),   
   .I0(request), 
   .I1(empty) 
);


//not tmp_inv(state_delayed, tmp);
//assign tmp = ~state_delayed;
//assign tmp = ~state;



LUT1 #(.INIT(2'b01)) tmp_inv 
(
   .O(tmp),   
   .I0(state)  
);


    
//The following latch is from Xilinx prim


FDPE_1bit  FDPE_1(tmp,clock,state);
//FDPE #(.INIT(1'b0)) ff_state (
//      .Q(state),      // 1-bit Data output
//      .C(clock),      // 1-bit Clock input
//      .CE(1'b1),    // 1-bit Clock enable input
//      .PRE(1'b0),  // 1-bit Asynchronous preset input
//      .D(tmp)       // 1-bit Data input
//   );
   
/*always @(posedge clock or negedge rst)
begin
    if(!rst)
        state <= 1'b0;
    else
        state <= #2 tmp;
end
*/
//assign tmp = ~state;




//assign in_R_delayed = in_R;

LUT1 #(.INIT(2'b10)) in_R_delay 
( 
   .O(in_R_delayed),   
   .I0(in_R)      
);





//assign out_A_delayed = out_A;


LUT1 #(.INIT(2'b10)) out_A_delay 
( 
   .O(out_A_delayed),   
   .I0(out_A)      
);



//assign extern_state = state;


LUT1 #(.INIT(2'b10)) fall_delay 
( 
   .O(extern_state),   
   .I0(state)      
);

assign in_A = extern_state;
assign out_R = extern_state;
assign fire = clock;

endmodule