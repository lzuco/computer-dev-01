`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/07/02 11:28:45
// Design Name: 
// Module Name: center_node
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module node_input(data_in,
Local_out_1,West_out,East_out,North_out,South_out,
arbiter_in,
Local_in_R,West_in_R,East_in_R,North_in_R,South_in_R);
//////////////////////////////////////////////////////////////////////////////////
input arbiter_in;
input [50:0] data_in;


output Local_out_1;
output [50:0] West_out;
output [50:0] East_out;
output [50:0] North_out;
output [50:0] South_out;

output Local_in_R;
output West_in_R;
output East_in_R;
output North_in_R;
output South_in_R;

//input rst;

(* keep = "true" *)reg [40:0] Local_out;
(* dont_touch = "true" *)wire [50:0] Local_out_1;
//assign  Local_out_1={Local_out,10'b0};
//////////////////////////////////////////////////////////////////////////////////
//(* dont_touch = "true" *) wire [7:0] fire;
 wire [7:0] fire;
//(* KEEP="TRUE"*) wire [4:0] arbiter_out;

//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] Total_in;
// always@*
// begin
    // if(!rst)
        // Total_in<=51'b0;
    // else
// begin
// case(arbiter_out)
// 5'b10000: Total_in<=Local_in;
// 5'b01000: Total_in<=West_in;
// 5'b00100: Total_in<=East_in;
// 5'b00010: Total_in<=North_in;
// 5'b00001: Total_in<=South_in;
// default:  Total_in<=51'b0; 
// endcase
// end
// end

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
//(* KEEP="TRUE"*) wire [50:0] Total_in;

//assign    Total_in = arbiter_out[4] ? Local_in : arbiter_out[3] ? West_in : arbiter_out[2] ? East_in : 
//                   arbiter_out[1] ? North_in : arbiter_out[0] ? South_in : 51'b0;

//////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] Total_in_0;
//always@(posedge fire[0] or negedge rst)
//begin
//    if(!rst)
//        Total_in_0 <= 51'b0;
//    else
//        Total_in_0 <= data_in;
//end

assign Total_in_0 = data_in;
////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] South_north_message_0;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] North_south_message_0;
(* KEEP="TRUE"*) wire [50:0] South_north_message;
(* KEEP="TRUE"*) wire [50:0] North_south_message;
//always@(posedge fire[1] or negedge rst)
//begin
//    if(!rst) begin
//        South_north_message_0 <= 51'b0;
//        North_south_message_0 <= 51'b0;end
//    else begin
//        South_north_message_0 <= South_north_message;
//        North_south_message_0 <= North_south_message;end
//end

//(* KEEP="TRUE"*) wire fire_2_1,fire_2_2;  
 wire fire_2_1,fire_2_2;  

 FDPE_51bit  FDPE_1(South_north_message,fire_2_1,South_north_message_0);
 FDPE_51bit  FDPE_2(North_south_message,fire_2_2,North_south_message_0);


////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire [40:0] Local_out_0;   
(* KEEP="TRUE"*) wire [50:0] West_out_0;     
(* KEEP="TRUE"*) wire [50:0] East_out_0;     
(* KEEP="TRUE"*) wire [50:0] North_out_0;   
(* KEEP="TRUE"*) wire [50:0] South_out_0;   
//(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [25:0] Local_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] West_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] East_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] North_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [50:0] South_out;
(* KEEP="TRUE"*) wire sign_default;
//always@(posedge fire[2] or negedge rst)
//begin
//    if(!rst) begin
//        Local_out_1 <= 51'b0;
//        West_out <= 51'b0;
//        East_out <= 51'b0;
//        North_out <= 51'b0;
//        South_out <= 51'b0;
//        end
//    else begin
//        Local_out_1<={Local_out_0,10'b0};
//        West_out <= West_out_0 ;
//        East_out <= East_out_0 ;
//        North_out<=North_out_0;
//        South_out<=South_out_0;
//        end
//end
//(* dont_touch = "true" *) wire fire_6_1,fire_6_2,fire_6_3,fire_6_4,fire_6_5,fire_6_6;
wire fire_6_1,fire_6_2,fire_6_3,fire_6_4,fire_6_5,fire_6_6;
 FDPE_51bit  FDPE_3({Local_out_0,10'b0},fire_6_1,Local_out_1);
 FDPE_51bit  FDPE_4(West_out_0 ,fire_6_2,West_out );
 FDPE_51bit  FDPE_5(East_out_0 ,fire_6_3,East_out );
 FDPE_51bit  FDPE_6(North_out_0,fire_6_4,North_out);
 FDPE_51bit  FDPE_7(South_out_0,fire_6_5,South_out);
 

//(* dont_touch = "true" *) wire fire_1;
wire fire_1;
assign fire_1=fire_6_6|fire[5];

//always@(posedge fire_1 or negedge rst)
//begin
//    if(!rst) begin
//        sign_default <= 1'b0;end
//    else begin
//        sign_default <= ~sign_default;end
//end

(* dont_touch = "true" *) wire  sign_default_1;
assign sign_default_1=~sign_default;

 FDPE_1bit  FDPE_8(sign_default_1,fire_1,sign_default);
 
//////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire [4:0] sign_0;
(* KEEP="TRUE"*) wire [4:0] sign;

//(* dont_touch = "true" *) wire fire_2;
wire fire_2;

assign fire_2=fire[3]|fire[6];

//always@(posedge fire_2 or negedge rst)
//begin
//    if(!rst)
//        sign_0 <= 5'b0;
//    else
//        sign_0 <= sign;
//end

 FDPE_5bit  FDPE_9(sign,fire_2,sign_0);



(* KEEP="TRUE"*) wire sign_value_y;
(* KEEP="TRUE"*) wire sign_value_x;
(* KEEP="TRUE"*) wire sign_side_y;
(* KEEP="TRUE"*) wire sign_side_x;
assign sign_value_y=Total_in_0[3]|Total_in_0[2]|Total_in_0[1]|Total_in_0[0];
assign sign_value_x=Total_in_0[8]|Total_in_0[7]|Total_in_0[6]|Total_in_0[5];
assign sign_side_y=Total_in_0[4];    //side signal helps decide which way should the data goes(east or west when x is not zero)
assign sign_side_x=Total_in_0[9];
assign sign={sign_default,sign_value_x,sign_side_x,sign_value_y,sign_side_y};


//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [4:0] sign_out_R;

// always@*
// begin
    // if(!rst)
        // sign_out_R=5'b00000;
    // else
    // begin
// case(sign_0)
// 5'b10000: sign_out_R=5'b10000;//local, west, east, north, south
// 5'b10001: sign_out_R=5'b10000;
// 5'b10101: sign_out_R=5'b10000;
// 5'b10100: sign_out_R=5'b10000;

// 5'b11000: sign_out_R=5'b01000;
// 5'b11001: sign_out_R=5'b01000;
// 5'b11010: sign_out_R=5'b01000;
// 5'b11011: sign_out_R=5'b01000;

// 5'b11100: sign_out_R=5'b00100;
// 5'b11101: sign_out_R=5'b00100;
// 5'b11110: sign_out_R=5'b00100;
// 5'b11111: sign_out_R=5'b00100;

// 5'b10011: sign_out_R=5'b00010;
// 5'b10111: sign_out_R=5'b00010;

// 5'b10010: sign_out_R=5'b00001;
// 5'b10110: sign_out_R=5'b00001;

// default: sign_out_R=5'b00000; 
// endcase
// end
// end

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
(* KEEP="TRUE"*) wire [4:0] sign_out_R;

assign sign_out_R = ((sign_0 == 5'b10000) | (sign_0 == 5'b10001) | (sign_0 == 5'b10101) | (sign_0 == 5'b10100)) ? 5'b10000 : 
                    ((sign_0 == 5'b11000) | (sign_0 == 5'b11001) | (sign_0 == 5'b11010) | (sign_0 == 5'b11011)) ? 5'b01000 : 
                    ((sign_0 == 5'b11100) | (sign_0 == 5'b11101) | (sign_0 == 5'b11110) | (sign_0 == 5'b11111)) ? 5'b00100 : 
                    ((sign_0 == 5'b10011) | (sign_0 == 5'b10111)) ? 5'b00010 : 
                    ((sign_0 == 5'b10010) | (sign_0 == 5'b10110)) ? 5'b00001 :  5'b00000; 


wire [4:0] sign_out_R_0;
//(* dont_touch = "true" *) wire fire_47;

 wire fire_47;

assign fire_47 = fire[4]|fire[7];
//always@(posedge fire_47 or negedge rst)
//begin
//    if(!rst)
//        sign_out_R_0 <= 5'b0;
//    else
//        sign_out_R_0 <= sign_out_R;
//end

 FDPE_5bit  FDPE_10(sign_out_R,fire_47,sign_out_R_0);

//////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire Q_Local;
(* KEEP="TRUE"*) wire Q_West;
(* KEEP="TRUE"*) wire Q_East;
(* KEEP="TRUE"*) wire Q_North;
(* KEEP="TRUE"*) wire Q_South;
(* KEEP="TRUE"*) wire sign_out_R_0_0,sign_out_R_0_1,sign_out_R_0_2,sign_out_R_0_3,sign_out_R_0_4;
assign sign_out_R_0_0= ~Q_Local;
assign sign_out_R_0_1= ~Q_West ;
assign sign_out_R_0_2= ~Q_East ;
assign sign_out_R_0_3= ~Q_North;
assign sign_out_R_0_4= ~Q_South;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) FDPE_1bit FDPE_20(sign_out_R_0_0,sign_out_R_0[4],Q_Local);
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) FDPE_1bit FDPE_21 (sign_out_R_0_1,sign_out_R_0[3],Q_West );
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) FDPE_1bit FDPE_22 (sign_out_R_0_2,sign_out_R_0[2],Q_East );
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) FDPE_1bit FDPE_23(sign_out_R_0_3,sign_out_R_0[1],Q_North);
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) FDPE_1bit FDPE_24(sign_out_R_0_4,sign_out_R_0[0],Q_South);
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire Q_Local_0;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire Q_West_0;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire Q_East_0;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire Q_North_0;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) wire Q_South_0;
//always@(posedge fire[6] or negedge rst)
//begin
//    if(!rst) begin
//        Q_Local_0<= 1'b0;
//        Q_West_0<= 1'b0;
//        Q_East_0<= 1'b0;
//        Q_North_0<= 1'b0;
//        Q_South_0<= 1'b0;end
//    else begin
//        Q_Local_0 <=Q_Local ;
//        Q_West_0 <= Q_West  ;
//        Q_East_0 <= Q_East  ;
//        Q_North_0<= Q_North ;
//        Q_South_0<= Q_South ;end
//end


 FDPE_1bit  FDPE_11(Q_Local,fire[6],Q_Local_0);
 FDPE_1bit  FDPE_12(Q_West ,fire[6],Q_West_0 );
 FDPE_1bit  FDPE_13(Q_East ,fire[6],Q_East_0 );
 FDPE_1bit  FDPE_14(Q_North,fire[6],Q_North_0);
 FDPE_1bit  FDPE_15(Q_South,fire[6],Q_South_0);
////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] Local_east;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] Local_west;
// always@*
// begin
// if(!rst)begin
// Local_east<=51'b0;
// Local_west<=51'b0;
// end
// else if(Total_in_0[15]==0)begin
// Local_east<=Total_in_0;
// Local_west<=51'b0;
// end
// else if(Total_in_0[15]==1)begin
// Local_east<=51'b0;
// Local_west<=Total_in_0;
// end
// else;
// end

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
(* KEEP="TRUE"*) wire [50:0] Local_east;
(* KEEP="TRUE"*) wire [50:0] Local_west;

assign    Local_east = (Total_in_0[9]==0) ? Total_in_0 : 51'b0;
assign    Local_west = (Total_in_0[9]==1) ? Total_in_0 : 51'b0;
////////////////////////////////////////////////////////////////////////////////////

(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) west_east we (East_out_0,North_south_message,Local_west);
////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] South_message_0;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] North_message_0;
// always@*
// begin
// if(!rst)begin
// South_message_0<=51'b0;
// North_message_0<=51'b0;
// end
// else if (North_south_message_0[7]==0)begin
// South_message_0<=North_south_message_0;
// North_message_0<=51'b0;
// end
// else if(North_south_message_0[7]==1)begin
// South_message_0<=51'b0;
// North_message_0<=North_south_message_0;
// end
// end

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
(* KEEP="TRUE"*) wire [50:0] South_message_0;
(* KEEP="TRUE"*) wire [50:0] North_message_0;
assign    South_message_0 = (North_south_message_0[4]==0) ? North_south_message_0 : 51'b0;
assign    North_message_0 = (North_south_message_0[4]==1) ? North_south_message_0 : 51'b0;

////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) east_west ew (West_out_0,South_north_message,Local_east);
////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] South_message_1;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] North_message_1;
// always@*
// begin
// if(!rst)begin
// South_message_1<=51'b0;
// North_message_1<=51'b0;
// end
// else if (South_north_message_0[7]==0)begin
// South_message_1<=South_north_message_0;
// North_message_1<=51'b0;
// end
// else if(South_north_message_0[7]==1)begin
// South_message_1<=51'b0;
// North_message_1<=South_north_message_0;
// end
// end

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
(* KEEP="TRUE"*) wire [50:0] South_message_1;
(* KEEP="TRUE"*) wire [50:0] North_message_1;

assign    South_message_1 = (South_north_message_0[4]==0) ? South_north_message_0 : 51'b0;
assign    North_message_1 = (South_north_message_0[4]==1) ? South_north_message_0 : 51'b0;

////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire [40:0] message_0;
(* KEEP="TRUE"*) wire [50:0] North_merge_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) merge_2_1 m2 (North_merge_out,North_message_1,North_message_0);
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) south_north sn (North_out_0,message_0,North_merge_out);
////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire [40:0] message_1;
(* KEEP="TRUE"*) wire [50:0] South_merge_out;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) merge_2_1 m3 (South_merge_out,South_message_1,South_message_0);
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) north_south ns (South_out_0,message_1,South_merge_out);
////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) merge_2_1_15bit m4 (Local_out_0,message_0,message_1);

////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire  surplus_Local;
(* KEEP="TRUE"*) wire  surplus_West;
(* KEEP="TRUE"*) wire  surplus_East;
(* KEEP="TRUE"*) wire  surplus_North;
(* KEEP="TRUE"*) wire  surplus_South;
assign surplus_Local=Q_Local_0%2;
assign surplus_West=Q_West_0%2;
assign surplus_East=Q_East_0%2;
assign surplus_North=Q_North_0%2;
assign surplus_South=Q_South_0%2;


//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg  Local_in_R;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg  West_in_R;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg  East_in_R;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg  North_in_R;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg  South_in_R;

// always@*
// begin
// if(!rst)begin
// Local_in_R=1'b0;end
    // else if(surplus_Local==1) begin 
        // Local_in_R=1'b1;end
        // else if(surplus_Local==0) begin
            // Local_in_R=1'b0;end
// end

// always@*
// begin
// if(!rst)begin
// West_in_R=1'b0;end
    // else if (surplus_West==1) begin 
        // West_in_R=1'b1;end
        // else if(surplus_West==0) begin
            // West_in_R=1'b0;end
// end   
      
// always@*
// begin
// if(!rst)begin
// East_in_R=1'b0;end
    // else if (surplus_East==1) begin 
        // East_in_R=1'b1;end
        // else if(surplus_East==0) begin
            // East_in_R=1'b0;end
// end  
         
// always@*
// begin
// if(!rst)begin
// North_in_R=1'b0;end
    // else if (surplus_North==1) begin 
        // North_in_R=1'b1;end
        // else if(surplus_North==0) begin
            // North_in_R=1'b0;end
            // else
            // ;
// end 
         
// always@*
// begin
// if(!rst)begin
// South_in_R=1'b0;end
    // else if (surplus_South==1) begin 
        // South_in_R=1'b1;end
        // else if(surplus_South==0) begin
            // South_in_R=1'b0;end
// end      

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------
(* KEEP="TRUE"*) wire  Local_in_R;
(* KEEP="TRUE"*) wire  West_in_R;
(* KEEP="TRUE"*) wire  East_in_R;
(* KEEP="TRUE"*) wire  North_in_R;
(* KEEP="TRUE"*) wire  South_in_R;

assign    Local_in_R = surplus_Local;
assign    West_in_R = surplus_West;
assign    East_in_R = surplus_East;
assign    North_in_R = surplus_North;
assign    South_in_R = surplus_South;

////////////////////////////////////////////////////////////////////////////////////
(* KEEP="TRUE"*) wire Q_pipeline_in_R;
(* KEEP="TRUE"*) wire Q_R;
//(* KEEP="TRUE"*) wire arbiter_in;
wire arbiter_in;
(* KEEP="TRUE"*) wire pipeline_in_R;
//(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) count_2bin count(Q_pipeline_in_R,arbiter_in);
assign Q_R=~Q_pipeline_in_R;
 FDPE_1bit  FDPE_16(Q_R,arbiter_in,Q_pipeline_in_R);

(* KEEP="TRUE"*) wire surpius_pipeline_in_R;
assign pipeline_in_R=Q_pipeline_in_R;

//----------------------------------------------------------
//--------------original code-------------------------------
//----------------------------------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg pipeline_in_R;
// always@*
// begin
// if(!rst)begin
// pipeline_in_R=1'b0;end
    // else if (surpius_pipeline_in_R==1) begin 
        // pipeline_in_R=1'b1;end
        // else if(surpius_pipeline_in_R==0) begin
            // pipeline_in_R=1'b0;end
// end  

//---------------------------------------------------------
//--------------updated code-------------------------------
//---------------------------------------------------------


//assign    pipeline_in_R = surpius_pipeline_in_R;

////////////////////////////////////////////////////////////////////////////////////

(* dont_touch = "true" *) wire node_control_out_R;
(* dont_touch = "true" *) wire [36:0] control_out_R;
(* dont_touch = "true" *) wire [36:0] control_out_A;
(* dont_touch = "true" *) wire control_in_A;

////////////////////////////////////////////////////////////////////////////////////
telescope telescope_0 (pipeline_in_R,    control_in_A,     control_out_R[1],  control_out_A[1]);
telescope telescope_1 (control_out_R[1],  control_out_A[1],     control_out_R[2],  control_out_A[2]);
telescope telescope_3 (control_out_R[2],  control_out_A[2],     control_out_R[3],  control_out_A[3]);
telescope telescope_4 (control_out_R[3],  control_out_A[3],     control_out_R[4],  control_out_A[4]);
telescope telescope_5 (control_out_R[4],  control_out_A[4],     control_out_R[5],  control_out_A[5]);
telescope telescope_6 (control_out_R[5],  control_out_A[5],     control_out_R[6],  control_out_A[6]);
telescope telescope_7 (control_out_R[6],  control_out_A[6],     control_out_R[0],  control_out_A[0]);
//click click_control_1 (control_out_R[0], control_out_A[0], control_out_R[1],  control_out_A[1],  fire[0],rst);
//telescope click_control_2 (control_out_R[1], control_out_A[1], control_out_R[2],  control_out_A[2],rst);
//telescope click_control_3 (control_out_R[2], control_out_A[2], control_out_R[3],  control_out_A[3],rst);
//telescope click_control_4 (control_out_R[3], control_out_A[3], control_out_R[4],  control_out_A[4],rst);
//telescope click_control_5 (control_out_R[4], control_out_A[4], control_out_R[5],  control_out_A[5],rst);
//click click_control_6 (control_out_R[5], control_out_A[5], control_out_R[6],  control_out_A[6],  fire[1],rst);
click_2 click_2_2 (control_out_R[0], control_out_A[0], control_out_R[7],  control_out_A[7],  fire_2_1,fire_2_2);
//telescope click_control_7 (control_out_R[6], control_out_A[6], control_out_R[7],  control_out_A[7],rst);
//telescope click_control_8 (control_out_R[7], control_out_A[7], control_out_R[8],  control_out_A[8],rst);
//telescope click_control_9 (control_out_R[8], control_out_A[8], control_out_R[9],  control_out_A[9],rst);
//telescope click_control_20 (control_out_R[7], control_out_A[7], control_out_R[19],  control_out_A[19],rst);
//telescope click_control_21 (control_out_R[19], control_out_A[19], control_out_R[20],  control_out_A[20],rst);
//telescope click_control_20 (control_out_R[7], control_out_A[7], control_out_R[19],  control_out_A[19],rst);
//telescope click_control_21 (control_out_R[19], control_out_A[19], control_out_R[20],  control_out_A[20],rst);
telescope telescope_22 (control_out_R[7], control_out_A[7], control_out_R[21],  control_out_A[21]);
telescope telescope_23 (control_out_R[21], control_out_A[21], control_out_R[22],  control_out_A[22]);
telescope telescope_24 (control_out_R[22], control_out_A[22], control_out_R[24],  control_out_A[24]);
telescope telescope_25 (control_out_R[24], control_out_A[24], control_out_R[25],  control_out_A[25]);
telescope telescope_11 (control_out_R[25], control_out_A[25], control_out_R[10],  control_out_A[10]);
telescope telescope_12(control_out_R[10], control_out_A[10], control_out_R[11], control_out_A[11]);
click_6    click_6_6(control_out_R[11],control_out_A[11],control_out_R[12], control_out_A[12], fire_6_1,fire_6_2,fire_6_3,fire_6_4,fire_6_5,fire_6_6);
//telescope click_control_12(control_out_R[11],control_out_A[11],control_out_R[12], control_out_A[12],rst);
//telescope click_control_13(control_out_R[12],control_out_A[12],control_out_R[13], control_out_A[13],rst);
//telescope click_control_14(control_out_R[13],control_out_A[13],control_out_R[14], control_out_A[14],rst);
//telescope click_control_15(control_out_R[14],control_out_A[14],control_out_R[15], control_out_A[15],rst);
//telescope click_control_16(control_out_R[15],control_out_A[15],control_out_R[16], control_out_A[16],rst);
//telescope click_control_17(control_out_R[16],control_out_A[16],control_out_R[17], control_out_A[17],rst);
//telescope click_control_18(control_out_R[17],control_out_A[17],control_out_R[18], control_out_A[18],rst);
//telescope click_control_19(control_out_R[18],control_out_A[18],control_out_R[19], control_out_A[19],rst);
//telescope click_control_20(control_out_R[19],control_out_A[19],control_out_R[20], control_out_A[20],rst);
//telescope click_control_21(control_out_R[20],control_out_A[20],control_out_R[21], control_out_A[21],rst);
//telescope click_control_22(control_out_R[21],control_out_A[21],control_out_R[22], control_out_A[22],rst);
//telescope click_control_23(control_out_R[22],control_out_A[22],control_out_R[23], control_out_A[23],rst);
//telescope click_control_24(control_out_R[23],control_out_A[23],control_out_R[24], control_out_A[24],rst);
//telescope click_control_25(control_out_R[24],control_out_A[24],control_out_R[25], control_out_A[25],rst);
//telescope click_control_26(control_out_R[25],control_out_A[25],control_out_R[26], control_out_A[26],rst);
//telescope click_control_27(control_out_R[26],control_out_A[26],control_out_R[27], control_out_A[27],rst);
//telescope click_control_28(control_out_R[27],control_out_A[27],control_out_R[28], control_out_A[28],rst);
telescope telescope_14(control_out_R[12],control_out_A[12],control_out_R[13], control_out_A[13]);
telescope telescope_15(control_out_R[13],control_out_A[13],control_out_R[14], control_out_A[14]);
click click_control_16(control_out_R[14],control_out_A[14],control_out_R[15], control_out_A[15], fire[3]);
telescope telescope_17(control_out_R[15],control_out_A[15],control_out_R[16], control_out_A[16]);
telescope telescope_18(control_out_R[16],control_out_A[16],control_out_R[17], control_out_A[17]);
telescope telescope_19(control_out_R[17],control_out_A[17],control_out_R[31], control_out_A[31]);
click click_control_32(control_out_R[31],control_out_A[31],control_out_R[32], control_out_A[32], fire[4]);
click click_control_33(control_out_R[32],control_out_A[32],control_out_R[33], control_out_A[33], fire[5]);     //sign_default��λ
click click_control_34(control_out_R[33],control_out_A[33],control_out_R[34], control_out_A[34], fire[6]);
delay_5  delay_c(control_out_R[34], control_out_A[34],control_out_R[35], control_out_A[35]);
click click_control_35(control_out_R[35],control_out_A[35],node_control_out_R,node_control_out_R,fire[7]);
////////////////////////////////////////////////////////////////////////////////////               
// ila_node_input   ila_node_input  (
//.clk(clk650),
//.probe0(arbiter_in),
//.probe1(data_in),
//.probe2(Total_in_0),
//.probe3(Local_east),
//.probe4(South_north_message),
//.probe5(South_north_message_0),
//.probe6(South_message_1),
//.probe7(South_merge_out),
//.probe8(message_1),
//.probe9(Local_out_0),
//.probe10(pipeline_in_R),
//.probe11(Local_out_1)
//);

endmodule
