`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/26 01:19:20
// Design Name: 
// Module Name: delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module click_6(in_R, in_A, out_R, out_A,fire_1,fire_2,fire_3,fire_4,fire_5,fire_6);
input   in_R,out_A;
output  out_R,in_A; 
output fire_1,fire_2,fire_3,fire_4,fire_5,fire_6;
 (* dont_touch = "true" *)wire in_R,out_A,out_R,in_A;
 (* dont_touch = "true" *)wire out_R_1,out_A_1,out_R_2,out_A_2;
 (* dont_touch = "true" *)wire out_R_3,out_A_3,out_R_4,out_A_4,out_R_5,out_A_5;
wire fire_1,fire_2,fire_3,fire_4,fire_5,fire_6;


click delay_1(in_R,in_A,out_R_1,out_A_1,fire_1);
click delay_2(out_R_1,out_A_1,out_R_2,out_A_2,fire_2);
click delay_3(out_R_2,out_A_2,out_R_3,out_A_3,fire_3);
click delay_4(out_R_3,out_A_3,out_R_4,out_A_4,fire_4);
click delay_5(out_R_4,out_A_4,out_R_5,out_A_5,fire_5);
click delay_6(out_R_5,out_A_5,out_R,out_A,fire_6);




endmodule
