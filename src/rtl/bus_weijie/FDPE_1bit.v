`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/05/24 22:53:58
// Design Name: 
// Module Name: FDPE_1bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FDPE_1bit(D,TRIG,Q);
    input D;
    input TRIG;
    output Q;
    
    FDPE #(.INIT(1'b0)) ff_state (
          .Q(Q),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D)       // 1-bit Data input
       );
       
       
endmodule
