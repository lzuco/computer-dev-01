`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/04/19 11:14:01
// Design Name: 
// Module Name: arbitrator_variable_priority
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module arbitrator_variable_priority(R,P,G);
//input fire;
input [4:0] P;
input [4:0] R;
output [4:0] G;
//input rst;

(* KEEP="TRUE"*) wire [3:0] A;
(* KEEP="TRUE"*) wire [3:0] B;
(* KEEP="TRUE"*) wire [4:0] P;
(* KEEP="TRUE"*) wire [4:0] i;
(* KEEP="TRUE"*) wire [4:0] R;
(* KEEP="TRUE"*) wire [4:0] G;


assign i=R;
//always@(posedge fire or negedge rst)
//begin
//if (!rst)
//    i<=4'b0;
//else
//    i<=R;

//end
/*
assign A[4]=P[4]|B[0];
assign G[4]=R[4]&A[4];
assign B[4]=(~R[4])&A[4];

assign A[3]=P[3]|B[4];
assign G[3]=R[3]&A[3];
assign B[3]=(~R[3])&A[3];

assign A[2]=P[2]|B[3];
assign G[2]=R[2]&A[2];
assign B[2]=(~R[2])&A[2];

assign A[1]=P[1]|B[2];
assign G[1]=R[1]&A[1];
assign B[1]=(~R[1])&A[1];

assign A[0]=P[0]|B[1];
assign G[0]=R[0]&A[0];
assign B[0]=(~R[0])&A[0];
*/
//assign A[4]=P[4]|B[0];
assign G[4]=i[4]&P[4];
assign B[3]=(~i[4])&P[4];

assign A[3]=P[3]|B[3];
assign G[3]=i[3]&A[3];
assign B[2]=(~i[3])&A[3];

assign A[2]=P[2]|B[2];
assign G[2]=i[2]&A[2];
assign B[1]=(~i[2])&A[2];

assign A[1]=P[1]|B[1];
assign G[1]=i[1]&A[1];
assign B[0]=(~i[1])&A[1];

assign A[0]=P[0]|B[0];
assign G[0]=i[0]&A[0];
//assign B[0]=(~i[0])&A[0];
endmodule
