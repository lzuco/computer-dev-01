`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/05/24 22:53:58
// Design Name: 
// Module Name: FDPE_1bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FDPE_5bit(D,TRIG,Q);
    input  D;
    input TRIG;
    output  Q;
    
    wire  [4:0] D;
    wire  [4:0] Q;
       
   FDPE #(.INIT(1'b0)) ff_state_0 (
             .Q(Q[0]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[0])       // 1-bit Data input
          );
          
                
       FDPE #(.INIT(1'b0)) ff_state_1 (
             .Q(Q[1]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[1])       // 1-bit Data input
          );
          
          
              
       FDPE #(.INIT(1'b0)) ff_state_2 (
             .Q(Q[2]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[2])       // 1-bit Data input
          );
          
          
                 
       FDPE #(.INIT(1'b0)) ff_state_3 (
             .Q(Q[3]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[3])       // 1-bit Data input
          );
          
          
                 
       FDPE #(.INIT(1'b0)) ff_state_4 (
             .Q(Q[4]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[4])       // 1-bit Data input
          );
       
endmodule
