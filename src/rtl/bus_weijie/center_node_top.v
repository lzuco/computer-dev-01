`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/25 09:01:18
// Design Name: 
// Module Name: center_node_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module center_node_top_code(
Local_in_R,Local_out_R,Local_in,Local_out,Local_click_in_A,Local_click_out_A,
West_in_R,West_out_R,West_in,West_out,West_click_in_A,West_click_out_A,
East_in_R,East_out_R,East_in,East_out,East_click_in_A,East_click_out_A,
North_in_R,North_out_R,North_in,North_out,North_click_in_A,North_click_out_A,
South_in_R,South_out_R,South_in,South_out,South_click_in_A,South_click_out_A,rstn
);

//output reg [20:0]  count_1;
//output reg [20:0]  count_2;
input rstn;
/////////////////////////////////////////////////////////////////////////////////////
input Local_in_R;
output Local_out_R;
input [50:0] Local_in;
output [50:0] Local_out;
input Local_click_out_A;
output Local_click_in_A;
////////////////////////////////////////////////////////////////////////////////////
input West_in_R;
output West_out_R;
input [50:0] West_in;
output [50:0] West_out;
input West_click_out_A;
output West_click_in_A;
////////////////////////////////////////////////////////////////////////////////////
input East_in_R;
output East_out_R;
input [50:0] East_in;
output [50:0] East_out;
input East_click_out_A;
output East_click_in_A;
////////////////////////////////////////////////////////////////////////////////////
input North_in_R;
output North_out_R;
input [50:0] North_in;
output [50:0] North_out;
input North_click_out_A;
output North_click_in_A;
////////////////////////////////////////////////////////////////////////////////////
input South_in_R;
output South_out_R;
input [50:0] South_in;
output [50:0] South_out;
input South_click_out_A;
output South_click_in_A;

//input rst;
///////////////////////////////////////////////////////
//(* dont_touch = "true" *)wire local_out_R_1,local_out_fire;
//(* dont_touch = "true" *)wire west_out_R_1 , west_out_fire;
//(* dont_touch = "true" *)wire east_out_R_1 , east_out_fire;
//(* dont_touch = "true" *)wire north_out_R_1,north_out_fire;
//(* dont_touch = "true" *)wire south_out_R_1,south_out_fire;


(* dont_touch = "true" *)wire local_out_R_1;
(* dont_touch = "true" *)wire west_out_R_1 ;
(* dont_touch = "true" *)wire east_out_R_1 ;
(* dont_touch = "true" *)wire north_out_R_1;
(* dont_touch = "true" *)wire south_out_R_1;

wire local_out_fire;
wire  west_out_fire;
wire  east_out_fire;
wire north_out_fire;
wire south_out_fire;




(* dont_touch = "true" *)wire local_out_A_1, west_out_A_1 ,east_out_A_1 ,north_out_A_1,south_out_A_1;

(* dont_touch = "true" *)wire Local_click_in_A_2,local_out_R_2;
wire local_out_fire_2;
click click_local(Local_in_R,Local_click_in_A,local_out_R_1,local_out_A_1,local_out_fire);
click click_local_1(local_out_R_1,Local_click_in_A_2,local_out_R_2,local_out_R_2,local_out_fire_2);


(* dont_touch = "true" *)wire West_click_in_A_2,west_out_R_2;
wire west_out_fire_2;
click click_west (West_in_R,West_click_in_A  ,west_out_R_1 ,west_out_A_1 , west_out_fire);
click click_west_1 (west_out_R_1,West_click_in_A_2  ,west_out_R_2 ,west_out_R_2 , west_out_fire_2);

(* dont_touch = "true" *)wire East_click_in_A_2,east_out_R_2;
wire east_out_fire_2;
click click_east (East_in_R,East_click_in_A  ,east_out_R_1 ,east_out_A_1 , east_out_fire);
click click_east_1 (east_out_R_1,East_click_in_A_2  ,east_out_R_2 ,east_out_R_2, east_out_fire_2);

(* dont_touch = "true" *)wire North_click_in_A_2,north_out_R_2;
wire north_out_fire_2;
click click_north(North_in_R,North_click_in_A,north_out_R_1,north_out_A_1,north_out_fire);
click click_north_1(north_out_R_1,North_click_in_A_2,north_out_R_2,north_out_R_2,north_out_fire_2);

(* dont_touch = "true" *)wire South_click_in_A_2,south_out_R_2;
wire south_out_fire_2;
click click_south(South_in_R,South_click_in_A,south_out_R_1,south_out_A_1,south_out_fire);
click click_south_1(south_out_R_1,South_click_in_A_2,south_out_R_2,south_out_R_2,south_out_fire_2);



//always@(posedge local_out_fire or negedge rst) ////////////��¼�������?
//begin
//    if(!rst)
//        count_1<= 20'b0;
//    else
//        count_1<=count_1+1'b1;
//end



(* dont_touch = "true" *) wire [50:0] Local_in_1,west_in_1,east_in_1,north_in_1,south_in_1;

//always@(posedge local_out_fire or negedge rst)
//begin
//    if(!rst)
//        Local_in_1<= 51'b0;
//    else
//        Local_in_1<=Local_in;
//end
 FDPE_51bit  FDPE_1(Local_in,local_out_fire,Local_in_1);


//always@(posedge west_out_fire or negedge rst)
//begin
//    if(!rst)
//        west_in_1<= 51'b0;
//    else
//        west_in_1<=West_in;
//end

 FDPE_51bit  FDPE_2(West_in,west_out_fire,west_in_1);

//always@(posedge east_out_fire or negedge rst)
//begin
//    if(!rst)
//        east_in_1<= 51'b0;
//    else
//        east_in_1<=East_in;
//end

 FDPE_51bit  FDPE_3(East_in,east_out_fire,east_in_1);


//always@(posedge north_out_fire or negedge rst)
//begin
//    if(!rst)
//        north_in_1<= 51'b0;
//    else
//        north_in_1<=North_in;
//end

 FDPE_51bit  FDPE_4(North_in,north_out_fire,north_in_1);

//always@(posedge south_out_fire or negedge rst)
//begin
//    if(!rst)
//        south_in_1<= 51'b0;
//    else
//        south_in_1<=South_in;
//end

 FDPE_51bit  FDPE_5(South_in,south_out_fire,south_in_1);

(* dont_touch = "true" *)wire [50:0] Local_out_l,West_out_l,East_out_l,North_out_l,South_out_l;
(* dont_touch = "true" *)wire Local_out_R_l,West_out_R_l,East_out_R_l,North_out_R_l,South_out_R_l;

(* dont_touch = "true" *)wire [50:0] Local_out_w,West_out_w,East_out_w,North_out_w,South_out_w;
(* dont_touch = "true" *)wire Local_out_R_w,West_out_R_w,East_out_R_w,North_out_R_w,South_out_R_w;

(* dont_touch = "true" *)wire [50:0] Local_out_e,West_out_e,East_out_e,North_out_e,South_out_e;
(* dont_touch = "true" *)wire Local_out_R_e,West_out_R_e,East_out_R_e,North_out_R_e,South_out_R_e;

(* dont_touch = "true" *)wire [50:0] Local_out_n,West_out_n,East_out_n,North_out_n,South_out_n;
(* dont_touch = "true" *)wire Local_out_R_n,West_out_R_n,East_out_R_n,North_out_R_n,South_out_R_n;

(* dont_touch = "true" *)wire [50:0] Local_out_s,West_out_s,East_out_s,North_out_s,South_out_s;
(* dont_touch = "true" *)wire Local_out_R_s,West_out_R_s,East_out_R_s,North_out_R_s,South_out_R_s;



/////////////////receive
node_input local_receive(Local_in_1,
Local_out_l,West_out_l,East_out_l,North_out_l,South_out_l,
local_out_fire_2,
Local_out_R_l,West_out_R_l,East_out_R_l,North_out_R_l,South_out_R_l); 

node_input west_receive(west_in_1,
Local_out_w,West_out_w,East_out_w,North_out_w,South_out_w,
west_out_fire_2,
Local_out_R_w,West_out_R_w,East_out_R_w,North_out_R_w,South_out_R_w); 

node_input  east_receive(east_in_1,
Local_out_e,West_out_e,East_out_e,North_out_e,South_out_e,
east_out_fire_2,
Local_out_R_e,West_out_R_e,East_out_R_e,North_out_R_e,South_out_R_e);

node_input  north_receive(north_in_1,
Local_out_n,West_out_n,East_out_n,North_out_n,South_out_n,
north_out_fire_2,
Local_out_R_n,West_out_R_n,East_out_R_n,North_out_R_n,South_out_R_n);

node_input  south_receive(south_in_1,
Local_out_s,West_out_s,East_out_s,North_out_s,South_out_s,
south_out_fire_2,
Local_out_R_s,West_out_R_s,East_out_R_s,North_out_R_s,South_out_R_s);

////////////////////////////
//(* dont_touch = "true" *)wire fire_local_2l,fire_west_2l ,fire_east_2l ,fire_north_2l,fire_south_2l;
//(* dont_touch = "true" *)wire fire_local_2w,fire_west_2w ,fire_east_2w ,fire_north_2w,fire_south_2w;
//(* dont_touch = "true" *)wire fire_local_2e,fire_west_2e ,fire_east_2e ,fire_north_2e,fire_south_2e;
//(* dont_touch = "true" *)wire fire_local_2n,fire_west_2n ,fire_east_2n ,fire_north_2n,fire_south_2n;
//(* dont_touch = "true" *)wire fire_local_2s,fire_west_2s ,fire_east_2s ,fire_north_2s,fire_south_2s;

wire fire_local_2l,fire_west_2l ,fire_east_2l ,fire_north_2l,fire_south_2l;
wire fire_local_2w,fire_west_2w ,fire_east_2w ,fire_north_2w,fire_south_2w;
wire fire_local_2e,fire_west_2e ,fire_east_2e ,fire_north_2e,fire_south_2e;
wire fire_local_2n,fire_west_2n ,fire_east_2n ,fire_north_2n,fire_south_2n;
wire fire_local_2s,fire_west_2s ,fire_east_2s ,fire_north_2s,fire_south_2s;

//////////////////send
node_output  local_send(Local_out_R_l,Local_out_R_w,Local_out_R_e,Local_out_R_n,Local_out_R_s,
                        Local_out_l,Local_out_w,Local_out_e,Local_out_n,Local_out_s,
                        Local_out_R,Local_click_out_A,Local_out,
                        fire_local_2l,fire_west_2l ,fire_east_2l ,fire_north_2l,fire_south_2l);
                       
node_output  west_send(West_out_R_l,West_out_R_w,West_out_R_e,West_out_R_n,West_out_R_s,
                        West_out_l,West_out_w,West_out_e,West_out_n,West_out_s,
                        West_out_R,West_click_out_A,West_out,
                        fire_local_2w,fire_west_2w ,fire_east_2w ,fire_north_2w,fire_south_2w);
                        
                        
node_output  east_send(East_out_R_l,East_out_R_w,East_out_R_e,East_out_R_n,East_out_R_s,
                       East_out_l,East_out_w,East_out_e,East_out_n,East_out_s,
                       East_out_R,East_click_out_A,East_out,
                       fire_local_2e,fire_west_2e ,fire_east_2e ,fire_north_2e,fire_south_2e);
                       
node_output  north_send(North_out_R_l,North_out_R_w,North_out_R_e,North_out_R_n,North_out_R_s,
                        North_out_l,North_out_w,North_out_e,North_out_n,North_out_s,
                        North_out_R,North_click_out_A,North_out,
                        fire_local_2n,fire_west_2n ,fire_east_2n ,fire_north_2n,fire_south_2n);

node_output  south_send(South_out_R_l,South_out_R_w,South_out_R_e,South_out_R_n,South_out_R_s,
                       South_out_l,South_out_w,South_out_e,South_out_n,South_out_s,
                       South_out_R,South_click_out_A,South_out,
                       fire_local_2s,fire_west_2s ,fire_east_2s ,fire_north_2s,fire_south_2s);


//(* dont_touch = "true" *)wire fire_l,fire_w,fire_e,fire_n,fire_s;
wire fire_l,fire_w,fire_e,fire_n,fire_s;

assign fire_l=fire_local_2l|fire_local_2w|fire_local_2e|fire_local_2n|fire_local_2s;
assign fire_w=fire_west_2l|fire_west_2w|fire_west_2e|fire_west_2n|fire_west_2s;
assign fire_e=fire_east_2l|fire_east_2w|fire_east_2e|fire_east_2n|fire_east_2s;
assign fire_n=fire_north_2l|fire_north_2w|fire_north_2e|fire_north_2n|fire_north_2s;
assign fire_s=fire_south_2l|fire_south_2w|fire_south_2e|fire_south_2n|fire_south_2s;




//always@(posedge fire_l or negedge rst)
//begin
//    if(!rst)
//        local_out_A_1<= 1'b0;
//    else
//        local_out_A_1<=local_out_R_1;
//end
FDPE_1bit  FDPE_6(local_out_R_1,fire_l,local_out_A_1);

//always@(posedge fire_w or negedge rst)
//begin
//    if(!rst)
//        west_out_A_1<= 1'b0;
//    else
//        west_out_A_1<=west_out_R_1;
//end

FDPE_1bit  FDPE_7(west_out_R_1,fire_w,west_out_A_1);

//always@(posedge fire_e or negedge rst)
//begin
//    if(!rst)
//        east_out_A_1<= 1'b0;
//    else
//        east_out_A_1<=east_out_R_1;
//end

FDPE_1bit  FDPE_8(east_out_R_1,fire_e,east_out_A_1);


//always@(posedge fire_n or negedge rst)
//begin
//    if(!rst)
//        north_out_A_1<= 1'b0;
//    else
//        north_out_A_1<=north_out_R_1;
//end

FDPE_1bit  FDPE_9(north_out_R_1,fire_n,north_out_A_1);


//always@(posedge fire_s or negedge rst)
//begin
//    if(!rst)
//        south_out_A_1<= 1'b0;
//    else
//        south_out_A_1<=south_out_R_1;
//end

FDPE_1bit  FDPE_10(south_out_R_1,fire_s,south_out_A_1);

endmodule
