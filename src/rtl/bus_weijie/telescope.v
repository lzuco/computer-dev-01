`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:22:48 11/03/2017 
// Design Name: 
// Module Name:    telescope 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//// lipengfei _v1
//////////////////////////////////////////////////////////////////////////////////
module telescope(
    in_R, in_A, out_R, out_A
    );
    
    input    in_R, out_A;
    output    out_R, in_A;
    
    wire    in_R_nor, out_R_delayed, out_R_tmp;
    wire    out_A_nor, in_A_delayed, in_A_tmp;
    wire out_R;
    wire in_A;
    
    assign in_R_nor=in_R^out_R;
    assign out_A_nor=in_A^out_A;
//    always @(posedge in_R_nor or negedge rst)
//       begin
//          if(!rst)
//              out_R<=1'b0;
//          else
//              out_R<=#2 ~out_R;
//       end
    
    wire    out_R_1;
    assign out_R_1=~out_R;
    FDPE_1bit  FDPE_1(out_R_1,in_R_nor,out_R);        
//    always @(posedge out_A_nor or negedge rst)
//       begin
//       if(!rst)
//          in_A<= 1'b0;
//       else
//          in_A<= #2 ~ in_A;
//    end
    
        wire    in_A_1;
    assign in_A_1=~in_A;
    FDPE_1bit  FDPE_2(in_A_1,out_A_nor,in_A); 
    
endmodule
