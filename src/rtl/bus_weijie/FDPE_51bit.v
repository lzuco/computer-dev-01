`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/05/24 22:53:58
// Design Name: 
// Module Name: FDPE_1bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FDPE_51bit(D,TRIG,Q);
    input  D;
    input TRIG;
    output  Q;
    
    wire  [50:0] D;
    wire  [50:0] Q;
       
       
       
   genvar i;
   generate
    for(i=0; i<51; i=i+1)begin: ff_state
    FDPE #(.INIT(1'b0)) ff (
          .Q(Q[i]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[i])       // 1-bit Data input
       );
    end
endgenerate

/*    
    FDPE #(.INIT(1'b0)) ff_state_0 (
          .Q(Q[0]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[0])       // 1-bit Data input
       );
       
             
    FDPE #(.INIT(1'b0)) ff_state_1 (
          .Q(Q[1]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[1])       // 1-bit Data input
       );
       
       
           
    FDPE #(.INIT(1'b0)) ff_state_2 (
          .Q(Q[2]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[2])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_3 (
          .Q(Q[3]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[3])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_4 (
          .Q(Q[4]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[4])       // 1-bit Data input
       );
       
      FDPE #(.INIT(1'b0)) ff_state_5 (
             .Q(Q[5]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[5])       // 1-bit Data input
          );
          
                
    FDPE #(.INIT(1'b0)) ff_state_6 (
             .Q(Q[6]),      // 1-bit Data output
             .C(TRIG),      // 1-bit Clock input
             .CE(1'b1),    // 1-bit Clock enable input
             .PRE(1'b0),  // 1-bit Asynchronous preset input
             .D(D[6])       // 1-bit Data input
          );
          
          
           
    FDPE #(.INIT(1'b0)) ff_state_7 (
          .Q(Q[7]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[7])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_8 (
          .Q(Q[8]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[8])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_9 (
          .Q(Q[9]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[9])       // 1-bit Data input
       );
          

   FDPE #(.INIT(1'b0)) ff_state_10 (
     .Q(Q[10]),      // 1-bit Data output
     .C(TRIG),      // 1-bit Clock input
     .CE(1'b1),    // 1-bit Clock enable input
     .PRE(1'b0),  // 1-bit Asynchronous preset input
     .D(D[10])       // 1-bit Data input
  );
             
                   
    FDPE #(.INIT(1'b0)) ff_state_11 (
          .Q(Q[11]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[11])       // 1-bit Data input
       );
       
       
           
    FDPE #(.INIT(1'b0)) ff_state_12 (
                .Q(Q[12]),      // 1-bit Data output
                .C(TRIG),      // 1-bit Clock input
                .CE(1'b1),    // 1-bit Clock enable input
                .PRE(1'b0),  // 1-bit Asynchronous preset input
                .D(D[12])       // 1-bit Data input
             );
             
             
           
   FDPE #(.INIT(1'b0)) ff_state_13 (
       .Q(Q[13]),      // 1-bit Data output
       .C(TRIG),      // 1-bit Clock input
       .CE(1'b1),    // 1-bit Clock enable input
       .PRE(1'b0),  // 1-bit Asynchronous preset input
       .D(D[13])       // 1-bit Data input
    );
    
    
           
   FDPE #(.INIT(1'b0)) ff_state_14 (
       .Q(Q[14]),      // 1-bit Data output
       .C(TRIG),      // 1-bit Clock input
       .CE(1'b1),    // 1-bit Clock enable input
       .PRE(1'b0),  // 1-bit Asynchronous preset input
       .D(D[14])       // 1-bit Data input
    );
    
        FDPE #(.INIT(1'b0)) ff_state_15 (
          .Q(Q[15]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[15])       // 1-bit Data input
       );
       
             
    FDPE #(.INIT(1'b0)) ff_state_16 (
          .Q(Q[16]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[16])       // 1-bit Data input
       );
       
       
           
    FDPE #(.INIT(1'b0)) ff_state_17 (
          .Q(Q[17]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[17])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_18(
          .Q(Q[18]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[18])       // 1-bit Data input
       );
       
       
              
    FDPE #(.INIT(1'b0)) ff_state_19 (
          .Q(Q[19]),      // 1-bit Data output
          .C(TRIG),      // 1-bit Clock input
          .CE(1'b1),    // 1-bit Clock enable input
          .PRE(1'b0),  // 1-bit Asynchronous preset input
          .D(D[19])       // 1-bit Data input
                );
                
       FDPE #(.INIT(1'b0)) ff_state_20 (
         .Q(Q[20]),      // 1-bit Data output
         .C(TRIG),      // 1-bit Clock input
         .CE(1'b1),    // 1-bit Clock enable input
         .PRE(1'b0),  // 1-bit Asynchronous preset input
         .D(D[20])       // 1-bit Data input
      );
      
            
   FDPE #(.INIT(1'b0)) ff_state_21 (
         .Q(Q[21]),      // 1-bit Data output
         .C(TRIG),      // 1-bit Clock input
         .CE(1'b1),    // 1-bit Clock enable input
         .PRE(1'b0),  // 1-bit Asynchronous preset input
         .D(D[21])       // 1-bit Data input
      );
      
      
          
   FDPE #(.INIT(1'b0)) ff_state_22 (
         .Q(Q[22]),      // 1-bit Data output
         .C(TRIG),      // 1-bit Clock input
         .CE(1'b1),    // 1-bit Clock enable input
         .PRE(1'b0),  // 1-bit Asynchronous preset input
         .D(D[22])       // 1-bit Data input
      );
      
      
             
   FDPE #(.INIT(1'b0)) ff_state_23 (
         .Q(Q[23]),      // 1-bit Data output
         .C(TRIG),      // 1-bit Clock input
         .CE(1'b1),    // 1-bit Clock enable input
         .PRE(1'b0),  // 1-bit Asynchronous preset input
         .D(D[23])       // 1-bit Data input
      );
      
      
             
   FDPE #(.INIT(1'b0)) ff_state_24 (
         .Q(Q[24]),      // 1-bit Data output
         .C(TRIG),      // 1-bit Clock input
         .CE(1'b1),    // 1-bit Clock enable input
         .PRE(1'b0),  // 1-bit Asynchronous preset input
         .D(D[24])       // 1-bit Data input
      );
      
    FDPE #(.INIT(1'b0)) ff_state_25 (
            .Q(Q[25]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[25])       // 1-bit Data input
         );
         
               
      FDPE #(.INIT(1'b0)) ff_state_26 (
            .Q(Q[26]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[26])       // 1-bit Data input
         );
         
         
             
      FDPE #(.INIT(1'b0)) ff_state_27 (
            .Q(Q[27]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[27])       // 1-bit Data input
         );
         
         
                
      FDPE #(.INIT(1'b0)) ff_state_28 (
            .Q(Q[28]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[28])       // 1-bit Data input
         );
         
         
                
      FDPE #(.INIT(1'b0)) ff_state_29 (
            .Q(Q[29]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[29])       // 1-bit Data input
                 );     
     FDPE #(.INIT(1'b0)) ff_state_30 (
            .Q(Q[30]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[30])       // 1-bit Data input
         );
         
         
                
      FDPE #(.INIT(1'b0)) ff_state_31 (
            .Q(Q[31]),      // 1-bit Data output
            .C(TRIG),      // 1-bit Clock input
            .CE(1'b1),    // 1-bit Clock enable input
            .PRE(1'b0),  // 1-bit Asynchronous preset input
            .D(D[31])       // 1-bit Data input
                                      );  
                                      
*/
                                      
                                      
                                      
                                      
                                                                            
endmodule
