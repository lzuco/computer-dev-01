`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/25 11:26:52
// Design Name: 
// Module Name: node_output
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module node_output(Local_in_R,West_in_R,East_in_R,North_in_R,South_in_R,
                   Local_in,West_in,East_in,North_in,South_in,
                   out_R,out_A,data_out,
                   fire_local_2,fire_west_2 ,fire_east_2 ,fire_north_2,fire_south_2);
                       
    input  Local_in_R,West_in_R,East_in_R,North_in_R,South_in_R;                 
    input  [50:0]  Local_in,West_in,East_in,North_in,South_in;  
    input  out_A;             
//    input  rst;   
    output  fire_local_2,fire_west_2 ,fire_east_2 ,fire_north_2,fire_south_2;        
    output  out_R;             
    output   data_out;                
 //////////////////////////////////////////
(* dont_touch = "true" *) wire [50:0]  data_out; 
(* dont_touch = "true" *) wire [4:0] arbiter_in;
//(* dont_touch = "true" *) wire fire_local_1,fire_west_1 ,fire_east_1 ,fire_north_1 ,fire_south_1;
wire fire_local_1,fire_west_1 ,fire_east_1 ,fire_north_1 ,fire_south_1;
 ////////////////////////////////////////
(* dont_touch = "true" *) wire Local_in_R_1;
(* dont_touch = "true" *)  wire local_in_A,Local_in_A_1;
(* dont_touch = "true" *) wire local_out_R_1,local_out_A_1,Local_out_R_2,Local_out_A_2,Local_out_R_3,Local_out_A_3,Local_out_R_4,Local_out_A_4;
//(* dont_touch = "true" *) wire Local_fire_1,Local_fire_2;
//(* dont_touch = "true" *) wire fire_l; 
 wire Local_fire_1,Local_fire_2;
 wire fire_l; 
  
   click click_local_1(Local_in_R,local_in_A,local_out_R_1,local_out_A_1,Local_fire_1);
    assign local_out_A_1=local_out_R_1;
    
    assign  fire_l= Local_fire_1| fire_local_1;
//   always@(posedge fire_l or negedge rst)
//   begin
//       if(!rst)
//           Local_in_R_1<= 1'b0;
//       else
//           Local_in_R_1<=~Local_in_R_1;
//   end    
           
  (* dont_touch = "true" *)wire Local_in_R_10;
  
  assign Local_in_R_10=~Local_in_R_1;
   FDPE_1bit  FDPE_1(Local_in_R_10,fire_l,Local_in_R_1);
           
           
   telescope telescope_1(Local_in_R_1, Local_in_A_1,Local_out_R_2,Local_out_A_2             );        
   click click_local_3(Local_out_R_2,Local_out_A_2,Local_out_R_3,Local_out_A_3,Local_fire_2);        
   telescope telescope_2(Local_out_R_3,Local_out_A_3,Local_out_R_4,Local_out_A_4           );  
          
   assign   Local_out_A_4= Local_out_R_4;    
   //////////////////////////////////////west    
 (* dont_touch = "true" *)  wire West_in_R_1;
 (* dont_touch = "true" *)  wire   West_in_A, West_in_A_1;
 (* dont_touch = "true" *)  wire  West_out_R_1,West_out_A_1, West_out_R_2,West_out_A_2,West_out_R_3,West_out_A_3,West_out_R_4,West_out_A_4; 
// (* dont_touch = "true" *)  wire     West_fire_1,West_fire_2;
// (* dont_touch = "true" *)   wire  fire_w;
 wire     West_fire_1,West_fire_2;
 wire  fire_w;
      
     click click_west_1(West_in_R,West_in_A,West_out_R_1,West_out_A_1,West_fire_1);
      assign West_out_A_1 = West_out_R_1;
    assign    fire_w=West_fire_1| fire_west_1 ;
//   always@(posedge fire_w or negedge rst)
//   begin
//       if(!rst)
//           West_in_R_1<= 1'b0;
//       else
//           West_in_R_1<=~West_in_R_1;
//   end    
  (* dont_touch = "true" *) wire West_in_R_10;
   assign West_in_R_10=~West_in_R_1;
    FDPE_1bit  FDPE_2(West_in_R_10,fire_w,West_in_R_1);   
           
   telescope telescope_3(West_in_R_1, West_in_A_1, West_out_R_2,West_out_A_2);        
   click click_west_3(West_out_R_2,West_out_A_2,West_out_R_3,West_out_A_3,West_fire_2);        
   telescope telescope_4(West_out_R_3,West_out_A_3,West_out_R_4,West_out_A_4);  
   
   assign  West_out_A_4=West_out_R_4;
   ///////////////////////////////////////////////////////////////east
(* dont_touch = "true" *)   wire East_in_R_1;
(* dont_touch = "true" *)   wire  East_in_A,East_in_A_1;
(* dont_touch = "true" *)   wire East_out_R_1,East_out_A_1,East_out_R_2,East_out_A_2,East_out_R_3,East_out_A_3,East_out_R_4,East_out_A_4;
//(* dont_touch = "true" *)   wire East_fire_1,East_fire_2;
//(* dont_touch = "true" *)   wire fire_e;
   
wire East_fire_1,East_fire_2;
wire fire_e;
   
   click click_east_1(East_in_R,East_in_A,East_out_R_1,East_out_A_1,East_fire_1);
   assign East_out_A_1=East_out_R_1;
   
   assign   fire_e=East_fire_1|fire_east_1;
           
//   always@(posedge fire_e or negedge rst)
//   begin
//       if(!rst)
//           East_in_R_1<= 1'b0;
//       else
//           East_in_R_1<=~East_in_R_1;
//   end    
           
(* dont_touch = "true" *)     wire East_in_R_10;
   assign East_in_R_10=~East_in_R_1;
    FDPE_1bit  FDPE_3(East_in_R_10,fire_e,East_in_R_1);          
           
   telescope telescope_5(East_in_R_1 ,East_in_A_1, East_out_R_2,East_out_A_2);        
   click click_east_3(East_out_R_2,East_out_A_2,East_out_R_3,East_out_A_3,East_fire_2);        
   telescope telescope_6(East_out_R_3,East_out_A_3,East_out_R_4,East_out_A_4);          
          
     assign East_out_A_4=East_out_R_4;
 ///////////////////////////////////////////////////////////////////north       
(* dont_touch = "true" *)wire North_in_R_1;
(* dont_touch = "true" *)wire North_in_A,North_in_A_1;
(* dont_touch = "true" *)wire North_out_R_1,North_out_A_1,North_out_R_2, North_out_A_2,North_out_R_3, North_out_A_3,North_out_R_4, North_out_A_4;
//(* dont_touch = "true" *)wire North_fire_1,North_fire_2;
//(* dont_touch = "true" *)wire  fire_n;  

wire North_fire_1,North_fire_2;
wire  fire_n;  
   
   click click_north_1(North_in_R,North_in_A,North_out_R_1,North_out_A_1,North_fire_1);
   
   assign North_out_A_1=North_out_R_1;
 assign    fire_n= North_fire_1|fire_north_1;
      
//      always@(posedge fire_n or negedge rst)
//            begin
//              if(!rst)
//                  North_in_R_1<= 1'b0;
//              else
//                  North_in_R_1<=~North_in_R_1;
//            end    
(* dont_touch = "true" *)  wire North_in_R_10;
          assign North_in_R_10=~North_in_R_1;
           FDPE_1bit  FDPE_4(North_in_R_10,fire_n,North_in_R_1);           
            
            
      telescope telescope_7(North_in_R_1, North_in_A_1, North_out_R_2, North_out_A_2);        
      click click_north_3(North_out_R_2,North_out_A_2,North_out_R_3, North_out_A_3,North_fire_2);        
      telescope telescope_8(North_out_R_3, North_out_A_3,North_out_R_4, North_out_A_4);            
     
     assign North_out_A_4=North_out_R_4;
////////////////////////////////////////////////////////////////////////////////////////south     
(* dont_touch = "true" *)wire South_in_R_1;
(* dont_touch = "true" *)wire  South_in_A,South_in_A_1;
(* dont_touch = "true" *)wire  South_out_R_1,South_out_A_1,South_out_R_2,South_out_A_2,South_out_R_3,South_out_A_3,South_out_R_4,South_out_A_4;
//(* dont_touch = "true" *)wire  South_fire_1,South_fire_2;
//(* dont_touch = "true" *)wire  fire_s;

wire  South_fire_1,South_fire_2;
wire  fire_s;

click click_south_1(South_in_R,South_in_A,South_out_R_1,South_out_A_1,South_fire_1);
assign South_out_A_1=South_out_R_1;

assign fire_s=South_fire_1|fire_south_1;

//      always@(posedge fire_s or negedge rst)
//           begin
//             if(!rst)
//                 South_in_R_1<= 1'b0;
//             else
//                 South_in_R_1<=~South_in_R_1;
//           end    
 (* dont_touch = "true" *)   wire South_in_R_10;
    assign South_in_R_10=~South_in_R_1;
    FDPE_1bit  FDPE_5(South_in_R_10,fire_s,South_in_R_1);          
                 
   telescope telescope_9(South_in_R_1, South_in_A_1, South_out_R_2,South_out_A_2);        
   click click_south_3(South_out_R_2,South_out_A_2,South_out_R_3,South_out_A_3,South_fire_2);        
   telescope telescope_10(South_out_R_3,South_out_A_3,South_out_R_4,South_out_A_4);    
     
     
     assign South_out_A_4=South_out_R_4;
//////////////////////////////////////////////////////////////////////////////////////////////////    
(* dont_touch = "true" *)wire  [4:0] priority; 
(* dont_touch = "true" *)wire in_R_h,in_R_k;
(* dont_touch = "true" *)wire [4:0] arbiter_out;
(* dont_touch = "true" *)wire click_in_A_1,click_out_R_1,click_out_A_1,click_out_R_2,click_out_A_2,click_out_R_3,click_out_A_3,click_out_R_4,click_out_A_4,click_out_R_5,click_out_A_5;
//(* dont_touch = "true" *)wire fire_arb,fire_h;
wire fire_arb,fire_h;
(* dont_touch = "true" *)wire  out_A_h;
//(* dont_touch = "true" *)wire click_fire_1,click_fire_2,click_fire_3,click_fire_4,click_fire_5;
wire click_fire_1,click_fire_2,click_fire_3,click_fire_4,click_fire_5;
(* dont_touch = "true" *)wire in_R_h_1,in_R_h_2;


    assign    arbiter_in={Local_out_R_4,West_out_R_4,East_out_R_4,North_out_R_4,South_out_R_4};

    arbiter arb(arbiter_in,arbiter_out,fire_arb,priority);

   assign in_R_h_1=Local_out_R_4|West_out_R_4|East_out_R_4|North_out_R_4|South_out_R_4;
   
  (* dont_touch = "true" *) wire in_A_k,out_R_h,in_R_20,in_A_20,fire_20,in_R_k_1,in_R_k_2,in_R_k_3,in_R_k_4;
  
  wire fire_k;
  
  LUT1 #(.INIT(2'b10)) in_R_1 
  ( 
     .O(in_R_h_2),   
     .I0(in_R_h_1)      
  );
    LUT1 #(.INIT(2'b10)) in_R_2
  ( 
     .O(in_R_h),   
     .I0(in_R_h_2)      
  );
   half_click hk(in_R_h,in_R_k, out_A_h,fire_h);
   
       LUT1 #(.INIT(2'b10)) in_R_3
 ( 
    .O(in_R_k_1),   
    .I0(in_R_k)      
 );
        LUT1 #(.INIT(2'b10)) in_R_4
( 
.O(in_R_k_2),   
.I0(in_R_k_1)      
);
       LUT1 #(.INIT(2'b10)) in_R_5
( 
.O(in_R_k_3),   
.I0(in_R_k_2)      
);

       LUT1 #(.INIT(2'b10)) in_R_6
( 
.O(in_R_k_4),   
.I0(in_R_k_3)      
);
   click click_k(in_R_k_3,in_A_k,out_R_h,out_R_h,fire_k);    //�˴�ֻ����һ��fire������ǿ����
  // click click_h(in_R_20,in_A_20,out_R_h,out_R_h,fire_20,rst);    //�˴�ֻ����һ��fire������ǿ����
      
 //     wire fire_hk=fire_h|fire_k;
 (* dont_touch = "true" *)wire [4:0] priority_1;
//always@(posedge fire_h or negedge rst)
//   begin
//       if(!rst)
//           priority_1 <= 5'b0;
   
//       else
//           priority_1<= arbiter_out;
//   end  
 FDPE_5bit  FDPE_6(arbiter_out,fire_h,priority_1);        
      
//    always@(posedge fire_k or negedge rst)
//  begin
//      if(!rst)
//          priority <= 5'b0;
//      else if(priority_1==5'b10000|priority_1==5'b01000|priority_1==5'b00100|priority_1==5'b00010|priority_1==5'b00001)
//          priority<= priority_1;           
//      else if(Local_out_R_4==1'b1)
//          priority<=5'b10000;
//      else if(West_out_R_4==1'b1)
//          priority<=5'b01000;
//      else if(East_out_R_4==1'b1)
//          priority<=5'b00100;
//      else if(North_out_R_4==1'b1)
//          priority<=5'b00010;
//      else if(South_out_R_4==1'b1)
//          priority<=5'b00001;
//  end   
  (* dont_touch = "true" *)wire [4:0] priority_2;
  assign  priority_2=(priority_1==5'b10000|priority_1==5'b01000|priority_1==5'b00100|priority_1==5'b00010|priority_1==5'b00001)?priority_1:((Local_out_R_4==1'b1)?5'b10000:((West_out_R_4==1'b1)?5'b01000:((East_out_R_4==1'b1)?5'b00100:((North_out_R_4==1'b1)?5'b00010:((South_out_R_4==5'b00001)?5'b00001:5'b00000)))))  ;
     
  FDPE_5bit  FDPE_7(priority_2,fire_k,priority);           
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////  
 (* dont_touch = "true" *) wire click_out_R_11,click_out_A_11,fire_11;
 (* dont_touch = "true" *) wire  [50:0]  data_out_1;
 (* dont_touch = "true" *) wire click_out_R_10,click_out_A_10;
   
   delay  delay_c_1(in_R_k       ,click_in_A_1 ,click_out_R_10,click_out_A_10);     
   click click_out_5(click_out_R_10,click_out_A_10 ,click_out_R_1,click_out_A_1,click_fire_1);           //������Ӧ�ٲ�������
   delay  delay_c(click_out_R_1,click_out_A_1,click_out_R_11,click_out_A_11);
   click click_out_6(click_out_R_11,click_out_A_11,click_out_R_2,click_out_A_2,click_fire_2);           // ��ת������out__A     
   click click_out_7(click_out_R_2,click_out_A_2,click_out_R_3,click_out_A_3,click_fire_3);             //�������ȼ�
   click click_out_8(click_out_R_3,click_out_A_3,click_out_R_4,click_out_A_4,click_fire_4);             //��תhk
   //click click_out_9(click_out_R_4,click_out_A_4,click_out_R_5,click_out_A_5,click_fire_5,rst);  
   telescope telescope_11(click_out_R_4,click_out_A_4,        out_R,        out_A);      
           
   
   assign fire_local_1=(priority==5'b10000)?click_fire_1:1'b0;
   assign fire_west_1=(priority==5'b01000)?click_fire_1:1'b0;
   assign fire_east_1 =(priority==5'b00100)?click_fire_1:1'b0;
   assign fire_north_1=(priority==5'b00010)?click_fire_1:1'b0;
   assign fire_south_1=(priority==5'b00001)?click_fire_1:1'b0;
   
   assign fire_local_2=(priority==5'b10000)?click_fire_2:1'b0;
   assign fire_west_2 =(priority==5'b01000)?click_fire_2:1'b0;
   assign fire_east_2 =(priority==5'b00100)?click_fire_2:1'b0;
   assign fire_north_2=(priority==5'b00010)?click_fire_2:1'b0;
   assign fire_south_2=(priority==5'b00001)?click_fire_2:1'b0; 
     
            
   assign fire_arb=click_fire_3;
      
//   always@(posedge click_fire_4 or negedge rst)
//   begin
//       if(!rst)
//           out_A_h <= 1'b0;
   
//       else
//           out_A_h<= in_R_k;
//   end
  
    FDPE_1bit  FDPE_8(in_R_k,click_fire_4,out_A_h);  
      
//     always@(posedge click_fire_2 or negedge rst)
//     begin
//         if(!rst)
//             data_out_1 <= 51'b0;
//         else if(priority==5'b10000)
//             data_out_1<= Local_in;
//         else if(priority==5'b01000)
//             data_out_1<= West_in;
//         else if(priority==5'b00100)
//             data_out_1<= East_in;
//         else if(priority==5'b00010)
//             data_out_1<= North_in;
//         else if(priority==5'b00001)
//             data_out_1<= South_in;
//     end  
    (* dont_touch = "true" *)wire [50:0] data_o;
    assign data_o=(priority==5'b10000)?Local_in:((priority==5'b01000)?West_in:((priority==5'b00100)?East_in:((priority==5'b00010)?North_in:((priority==5'b00001)?South_in:51'b0))));
    
       FDPE_51bit  FDPE_9(data_o,click_fire_2,data_out_1);   
   // reg [50:0] data_out_2;
    
// always@(posedge click_fire_4 or negedge rst)
//    begin
//        if(!rst)
//            data_out_2 <= 51'b0;
//        else 
//            data_out_2<=data_out_1;
//    end   
    

    
//        always@(posedge click_fire_4 or negedge rst)
//     begin
//         if(!rst)
//             data_out <= 51'b0;
//         else 
//             data_out<=data_out_1;
//     end   
    
    
         FDPE_51bit  FDPE_10(data_out_1,click_fire_4,data_out);     
    
      
endmodule
