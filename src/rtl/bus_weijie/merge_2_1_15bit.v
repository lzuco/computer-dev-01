`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/06/14 10:50:54
// Design Name: 
// Module Name: merge_2_1_15bit
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module merge_2_1_15bit(out,a,b);
input [40:0] a;
input [40:0] b;
output[40:0] out;

(* KEEP="TRUE"*) wire a_latch;
(* KEEP="TRUE"*) wire b_latch;
assign a_latch=|a;
assign b_latch=|b;


//assign a_latch=a[0]|a[1]|a[2]|a[3]|a[4]|a[5]|a[6]|a[7]|a[8]|a[9]|a[10]|a[11]|a[12]|a[13]|a[14]|a[15]|a[16]|a[17]|a[18]|a[19]|a[20]|a[21];
//assign b_latch=b[0]|b[1]|b[2]|b[3]|b[4]|b[5]|b[6]|b[7]|b[8]|b[9]|b[10]|b[11]|b[12]|b[13]|b[14]|b[15]|b[16]|b[17]|b[18]|b[19]|b[20]|b[21];

//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [15:0] out;

// always@*
// begin
// if(!rst)begin
        // out<=16'b0;end
// else if(a_latch==1)begin
        // out<=a;end
// else if(b_latch==1)begin
        // out<=b;end
// end

//--------------updated code-------------------------------
(* KEEP="TRUE"*) wire [40:0] out;

assign    out = (a_latch==1) ? a : (b_latch==1) ? b : 22'b0;

endmodule
