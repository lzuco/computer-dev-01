`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2017/03/27 14:33:55
// Design Name: 
// Module Name: west_east
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


 module west_east(East_out,North_south_message,West_merge_out);
input [50:0] West_merge_out;
output [50:0] East_out;
output [50:0] North_south_message;

(* KEEP="TRUE"*) wire [50:0] West_merge_out;
(* KEEP="TRUE"*) wire a;
(* KEEP="TRUE"*) wire [3:0] b;
(* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) subtracter_7bit i0(b,West_merge_out[8:5],4'b0001);
assign a=West_merge_out[8]|West_merge_out[7]|West_merge_out[6]|West_merge_out[5];

//--------------original code-------------------------------
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] North_south_message;
// (* KEEP="TRUE"*) (*OPTIMIZE = "OFF" *) reg [50:0] East_out;

// always@*
// begin
// if(!rst)begin
// North_south_message<=51'b0;
// East_out <= 51'b0;
// end
// else if (a==0)begin
// North_south_message<=West_merge_out;
// East_out<=51'b0;
// end
// else if (a==1)begin
// North_south_message<=51'b0;
// East_out<={West_merge_out[31:15],b,West_merge_out[7:0]};
// end
// else begin
// North_south_message<=51'b0;
// East_out<=51'b0;
// end
// end

//--------------updated code-------------------------------
(* KEEP="TRUE"*) wire [50:0] North_south_message;
(* KEEP="TRUE"*) wire [50:0] East_out;

assign North_south_message = (a==0) ? West_merge_out : 51'b0;
assign East_out = (a==1) ? {West_merge_out[50:9],b,West_merge_out[4:0]} : 51'b0;

endmodule
