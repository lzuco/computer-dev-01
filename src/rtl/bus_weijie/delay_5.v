`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/26 01:19:20
// Design Name: 
// Module Name: delay
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module delay_5(in_R, in_A, out_R, out_A);
input   in_R,out_A;
output  out_R,in_A; 
 (* dont_touch = "true" *)wire in_R,out_A,out_R,in_A;
 (* dont_touch = "true" *)wire out_R_1,out_A_1,out_R_2,out_A_2;
 (* dont_touch = "true" *)wire out_R_3,out_A_3,out_R_4,out_A_4,out_R_5,out_A_5;


telescope delay_1(in_R,in_A,out_R_1,out_A_1);
telescope delay_2(out_R_1,out_A_1,out_R_2,out_A_2);
telescope delay_3(out_R_2,out_A_2,out_R_3,out_A_3);
//telescope delay_4(out_R_3,out_A_3,out_R_4,out_A_4);
//telescope delay_5(out_R_4,out_A_4,out_R_5,out_A_5);
telescope delay_6(out_R_3,out_A_3,out_R,out_A);




endmodule
