//-----------------------------------------------
//    module name: 
//    author: 
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module allTest;

    reg clk;//ʱ��
    reg rst;//reset
    
    wire[31:0] x3 =  cpu_core_top.cpu_core.m_grf.regs[3];
    wire[31:0] x26 = cpu_core_top.cpu_core.m_grf.regs[26];
    wire[31:0] x27 = cpu_core_top.cpu_core.m_grf.regs[27];

    cpu_core_top cpu_core_top({5{1'b0}},rst,clk);
    integer roms;
    always #5 clk=~clk;
    string str;
    initial begin
        roms = $fopen("E:/one_core/cputest/roms.txt","r");
        while(!$feof(roms))begin
			$fgets(str,roms);
			str = str.substr(0, str.len()-2);
            $readmemh(str,cpu_core_top.icache.mem);
			clk=1'b0;
            rst=1'b0;
            #30
            rst=1'b1;
            wait(x26 == 32'b1)   // wait x26 == 1
            #100
            $display("==================================");
            $display("%7dns: %s",$time(),str);
            if (x27 == 32'b1) begin
                $display("PASS!!");
            end else begin
                $display("FAIL!!");
                $display("fail testnum = %2d", x3);
            end
		end
		$finish;
    end


endmodule

