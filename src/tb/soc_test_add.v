//-----------------------------------------------
//    module name: 
//    author: 
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module testbench_for_soc;
    reg               i_clk_1;
    reg               i_rst_1;
    reg               uart_rx;
    wire              uart_tx;
    wire [31:0]       io_pin; 
    soc_top top(
        .rstn_pad(i_rst_1),            
        .clk_pad(i_clk_1),
        .io_pin_pad(io_pin),
        .rx_pin_pad(uart_rx),
        .tx_pin_pad(uart_tx),
        .init_enable_pad(1'b1)
        
    );
    
    parameter ClockPeriod = 20;
    //assign io_pin[31:1] = 32'hFFFFFFFF;
    reg [31:0] mem_init [8191:0];
    
    initial
       begin
      $readmemh("E:/one_core/cputest/rom/rv32ui-p-add.txt", mem_init);
      // Initialize Inputs
		i_clk_1 = 0;
		i_rst_1 = 0;
		// Wait 1000 ns for global reset to finish
		#1000;
          i_rst_1 = 1;        
		// Add stimulus here
		#2000000;
      //  $stop;
      
      end

    always #(ClockPeriod/2) i_clk_1 =~i_clk_1 ;
    
    parameter                        BPS_115200 = 8680;    //每个比特的时间
    parameter                        SIZE = 8'd10;   //文件大小
    parameter                        NUM0 = 8'd0, NUM1 = 8'd1, NUM2 = 8'd2, NUM3 = 8'd3, NUM4 = 8'd4, NUM5 = 8'd5, 
                                                   NUM6 = 8'd6, NUM7 = 8'd7, NUM8 = 8'd8, NUM9 = 8'd9, NUM10 = 8'd10; 
    parameter                        CRC0_1 = 8'hfa, CRC0_2 = 8'h8b, 
                                      CRC1_1 = 8'h1c, CRC1_2 = 8'he6,
                                      CRC2_1 = 8'h91, CRC2_2 = 8'hd7, 
                                      CRC3_1 = 8'h54, CRC3_2 = 8'h3d,
                                      CRC4_1 = 8'hcf, CRC4_2 = 8'h37, 
                                      CRC5_1 = 8'hff, CRC5_2 = 8'hff,
                                      CRC6_1 = 8'hff, CRC6_2 = 8'hff, 
                                      CRC7_1 = 8'hff, CRC7_2 = 8'hff, 
                                      CRC8_1 = 8'hff, CRC8_2 = 8'hff, 
                                      CRC9_1 = 8'hff, CRC9_2 = 8'hff,
                                      CRC10_1 = 8'hff, CRC10_2 = 8'hff;                                              
    
    integer n = 0;  //用于索引包序号和CRC字节
    integer i = 0;  //每个包有32条指令
    integer j = 0;  //每条指令（32bit）分4次发送，每次发送8bit
    integer k = 0;  //对每次发送的8bit进行索引
    
    initial begin
        uart_rx = 1'b1;    //bus idle
        //0号包，表示文件大小
        //NUM0 8'h0 发往Icache
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM0[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
//        for (j=0;j<8;j=j+1)begin
            #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
         
            for (i=0;i<8;i=i+1)
            #BPS_115200 uart_rx = SIZE[i];     //stranmit data bit
          
            #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
            #BPS_115200 uart_rx = 1'b1;     //bus idle
//        end
        for (j=0;j<127;j=j+1)begin
            #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
         
            for (i=0;i<8;i=i+1)
            #BPS_115200 uart_rx = 1'b0;     //stranmit data bit
          
            #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
            #BPS_115200 uart_rx = 1'b1;     //bus idle
        end
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC0_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC0_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
end/*
        //1号包，发送文件数据
        //NUM1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM1[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=0;i<32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC1_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC1_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //2号包，发送文件数据
        //NUM2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM2[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=32;i<64;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC2_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC2_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle

        //3号包，发送文件数据
        //NUM3
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM3[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=64;i<96;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC3_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC3_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //4号包，发送文件数据
        //NUM4
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM4[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=96;i<128;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC4_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC4_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //5号包，发送文件数据
        //NUM5
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM5[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC5_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC5_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //6号包，发送文件数据
        //NUM6
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM6[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC6_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC6_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle

        //7号包，发送文件数据
        //NUM7
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM7[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC7_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC7_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //8号包，发送文件数据
        //NUM8
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM8[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC8_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC8_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //9号包，发送文件数据
        //NUM9
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM9[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC9_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC9_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        //10号包，发送文件数据
        //NUM10
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = NUM10[n];
        
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
        //DATA
        for(i=i+1;i<i+32;i=i+1)begin
            for(j=0;j<4;j=j+1)begin
                #BPS_115200 uart_rx = 1'b0;
                for(k=0;k<8;k=k+1)begin
                    #BPS_115200 uart_rx = mem_init[i][8*j+k]; 
                end
                #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
                #BPS_115200 uart_rx = 1'b1;     //bus idle
            end
        end
        
        //CRC1
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC10_1[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        //CRC2
        #BPS_115200 uart_rx = 1'b0;
        for (n=0;n<8;n=n+1)
        #BPS_115200 uart_rx = CRC10_2[n];     //stranmit data bit
      
        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
        #BPS_115200 uart_rx = 1'b0;
       end   
// parameter                        BPS_115200 = 86800;//每个比特的时间
//    parameter                        SEND_DATA = 8'haf;//      
//    parameter                        CRC1 = 8'h5e;// 
//    parameter                        CRC2 = 8'h25;//  
//    parameter                        CRC3 = 8'h37;// 
//    parameter                        CRC4 = 8'hd1;// 
//    parameter                        NUM1 = 8'hff;
//    parameter                        NUM2 = 8'h1;
//    parameter                        NUM3 = 8'h0;
    
//    integer i = 0;
//    integer j = 0;
    
//      initial begin
//        uart_rx = 1'b1;    //bus idle
//        //第一个包，表示文件大小
//        //NUM1 8'hff 发往Dcache
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = NUM3[i];
        
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
//        //DATA
//        for (j=0;j<8;j=j+1)begin
//            #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
         
//            for (i=0;i<8;i=i+1)
//            #BPS_115200 uart_rx = SEND_DATA[i];     //stranmit data bit
          
//            #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//            #BPS_115200 uart_rx = 1'b1;     //bus idle
//        end
//        for (j=0;j<120;j=j+1)begin
//            #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
         
//            for (i=0;i<8;i=i+1)
//            #BPS_115200 uart_rx = 1'b1;     //stranmit data bit
          
//            #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//            #BPS_115200 uart_rx = 1'b1;     //bus idle
//        end
//        //CRC1
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC1[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
//        //CRC2
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC2[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle

//                //第二个包，发送文件数据
//        //NUM2
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = NUM2[i];
        
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
//        //DATA
//        for (j=0;j<128;j=j+1)begin
//        #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
     
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = SEND_DATA[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
//        end
//        //CRC1
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC3[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
//        //CRC2
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC4[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
        
//        //第二个包，发送文件数据
//        //NUM2
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = NUM3[i];
        
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus enable
        
//        //DATA
//        for (j=0;j<128;j=j+1)begin
//        #BPS_115200 uart_rx = 1'b0;     //stranmit start bit
     
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = SEND_DATA[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
//        end
//        //CRC1
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC3[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
//        //CRC2
//        #BPS_115200 uart_rx = 1'b0;
//        for (i=0;i<8;i=i+1)
//        #BPS_115200 uart_rx = CRC4[i];     //stranmit data bit
      
//        #BPS_115200 uart_rx = 1'b0;     //stranmit stop bit
//        #BPS_115200 uart_rx = 1'b1;     //bus idle
        

//        #BPS_115200 uart_rx = 1'b0;
//       end   
*/
endmodule
