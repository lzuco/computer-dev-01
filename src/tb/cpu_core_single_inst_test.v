//-----------------------------------------------
//    module name: 
//    author: 
//  
//    version: 1st version (2021-10-01)
//    description: 
//        
//
//
//-----------------------------------------------
`timescale 1ns / 1ps
module testbench_for_core;

    reg               i_clk_1;
    reg               i_rst_1;
    
    cpu_core_top cpu_core_top(
        .rstn(i_rst_1),            
        .clk(i_clk_1)   
    );
    
    parameter ClockPeriod = 10 ;
    
    initial
       begin
       $readmemh("E:/one_core/cputest/rom.txt",top.icache.mem);
        i_clk_1 = 0 ;
        i_rst_1 = 1;
        #10
        i_rst_1 = 0;
        #10
        i_rst_1 = 1;
      end
    
    always #(ClockPeriod/2) i_clk_1 =~i_clk_1 ;
    
    
    
endmodule
