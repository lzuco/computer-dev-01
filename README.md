# livp
## livp简介
livp来自于兰州大学本科生第一届“一生一芯”项目的研发成采用两级流水线架构，包含一基于全异步NoC的内部总线
## 开发人员
### 本科生（CPU核、外设等）
梁泽成(2019)、孙泽(2019)、陆钇桦(2019)、魏人(2019)、谭源(2019)、李勋(2019)、张灵壮(2018)、俞炀(2017)
### 研究生（全异步NoC）
赵康利（2020）、付桐（2021）、万宝霞（2021）
### 技术支持
康振邦（2020）、李亦凡（2021）、樊荣（2021）、王艳（2021）
### 指导教师
何安平、陈文波、王兆滨、周睿、高平、李彩虹


